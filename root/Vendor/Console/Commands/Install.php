<?php

namespace STALKER_CMS\Vendor\Console\Commands;

use Illuminate\Console\Command;
use League\Flysystem\Exception;

/**
 * Class Install
 * Команда позволяет устанавливать CMS автоматически
 * Не завершена
 * @package STALKER_CMS\Vendor\Console\Commands
 */
class Install extends Command {

    protected $signature = 'Install';

    protected $description = 'Silent Installation';

    public function __construct() {
        parent::__construct();
    }

    public function handle() {

        if ($this->confirm('Installing the CMS. Continue? [yes|no]')) {
            if (file_exists(base_path('.env'))):
                $this->error('Installation canceled. CMS is already installed.');
                return FALSE;
            endif;
        }
    }
}
