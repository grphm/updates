<?php

namespace STALKER_CMS\Vendor\Console\Commands;

use Illuminate\Console\Command;
use League\Flysystem\Exception;

/**
 * Class CacheKiller
 * Команда позволяет удалить скомпилированные blade-файлы
 * @package STALKER_CMS\Vendor\Console\Commands
 */
class CacheKiller extends Command {

    protected $signature = 'CacheKiller';

    protected $description = 'Clearing application cache';

    /**
     * CacheKiller constructor.
     */
    public function __construct() {
        parent::__construct();
    }

    public function handle() {

        $cachedViewsDirectory = storage_path('framework/views');
        array_map("unlink", glob("$cachedViewsDirectory/*.php"));
    }
}
