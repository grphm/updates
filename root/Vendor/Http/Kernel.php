<?php

namespace STALKER_CMS\Vendor\Http;

use Illuminate\Routing\Router;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Foundation\Http\Kernel as HttpKernel;

/**
 * Регистрация фильтров
 * secure - используется для закрытой части (Панели администрирования)
 * public - используется для открытой части (Гостевого интерфейса)
 * Class Kernel
 * @package STALKER_CMS\Vendor\Http
 */
class Kernel extends HttpKernel {

    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class
    ];

    protected $middlewareGroups = [
        'secure' => [
            \STALKER_CMS\Vendor\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \STALKER_CMS\Vendor\Http\Middleware\VerifyCsrfToken::class,
            \STALKER_CMS\Vendor\Http\Middleware\localesMiddleware::class,
        ],
        'public' => [
            \STALKER_CMS\Vendor\Http\Middleware\EncryptCookies::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \STALKER_CMS\Vendor\Http\Middleware\VerifyCsrfToken::class,
            \STALKER_CMS\Core\Content\Http\Middleware\localesMiddleware::class,
        ],
        'api' => [
            'throttle:60,1',
        ],
    ];

    protected $routeMiddleware = [
        'auth' => \STALKER_CMS\Vendor\Http\Middleware\Authenticate::class,
        'settings' => \STALKER_CMS\Vendor\Http\Middleware\SettingsMiddleware::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'guest' => \STALKER_CMS\Vendor\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
    ];

    public function __construct(Application $app, Router $router) {

        $this->getHomeRouteMiddleware();
        parent::__construct($app, $router);
    }

    /********************************************************************************/
    /**
     * Получает и объеденяет фильтры из пакета
     * расширения функционала
     * @return $this
     */
    private function getHomeRouteMiddleware() {

        $home_middleware = [];
        $directory = base_path('home/Http/Middleware');
        if (file_exists($directory)):
            if ($scanned_directory = glob($directory . '/*.php')):
                foreach ($scanned_directory as $file_name):
                    $namespace = '\APPLICATION_HOME\Http\Middleware\\' . basename($directory . '/' . $file_name, ".php");
                    $middleware = new $namespace();
                    $home_middleware[$middleware->alias] = $namespace;
                endforeach;
            endif;
        endif;
        $this->routeMiddleware = array_merge($this->routeMiddleware, $home_middleware);
        return $this;
    }
}