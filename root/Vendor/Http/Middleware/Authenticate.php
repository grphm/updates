<?php

namespace STALKER_CMS\Vendor\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

/**
 * Фильтр авторизации
 * https://laravel.com/docs/5.2/middleware
 * Class Authenticate
 * @package STALKER_CMS\Vendor\Http\Middleware
 */
class Authenticate {

    /**
     * @param $request
     * @param Closure $next
     * @param null $guard
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function handle($request, Closure $next, $guard = null) {

        if (Auth::guard($guard)->guest()):
            if ($request->ajax()):
                return response('Unauthorized.', 401);
            else:
                return redirect()->guest('login');
            endif;
        elseif (\Auth::check() && Auth::user()->group->dashboard == 'admin'):
            return $next($request);
        else:
            return redirect()->to('/');
        endif;
    }
}