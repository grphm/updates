<?php
namespace STALKER_CMS\Vendor\Http\Controllers;

/**
 * Контроллер обрабатывающий запросы к серверу
 * Class RequestController
 * @package STALKER_CMS\Vendor\Controllers
 *
 * Обрабатывает входящие данные от клиента
 * Уберает пробелы в свойствах объекта Request
 * Применяет XSS фильтр для свойств объекта Request
 */
class RequestController {

    /**
     * Свойство хранящее указатель на обект Request
     * @var \Request
     */
    private $request;

    /**
     * Свойство хранящее список доступных тегов
     * @var
     */
    private $tags;

    /**
     * Инициализируем private свойство request
     */
    public function __construct() {

        $this->request = new \Request;
        $this->tags = '<p><a><strong><ul><li><img><iframe><em><table><td><tr><h1><h2><h3><h4><h5><h6><span><br>';
    }

    /*********************************************************************************/

    /**
     * Метод возвращает private свойство request после обработки
     */
    public function init() {

        $this->trim_spaces();
        $this->xss();
        return $this->request;
    }

    /**
     * Метод возвращает private свойство request без обработки
     */
    public function get() {

        return $this->request;
    }

    /**
     * Проверяет является ли запрос асинхронным
     * @return $this
     * @throws \Exception
     */
    public function isAJAX() {

        $request = $this->request;
        if (!$request::ajax()):
            throw new \Exception('Ожидается асинхронный запрос AJAX');
        endif;
        return $this;
    }
    /*********************************************************************************/

    /**
     * Уберает пробелы в свойствах объекта $this->request (Request)
     */
    public function trim_spaces() {

        $request = $this->request;
        $request::merge(array_map(function ($item) {
            return is_string($item) ? trim($item) : $item;
        }, $request::all()));
        $this->request = $request;
        return $this;
    }

    /**
     * Применяет XSS фильтр для свойств объекта $this->request (Request)
     */
    public function xss() {

        $result = [];
        $request = $this->request;
        foreach ($request::all() as $key => $value):
            $key = strip_tags($key);
            if (is_array($value)):
                $result[$key] = $this->arrayStripTags($value);
            else:
                $result[$key] = trim(strip_tags($value, $this->tags));
            endif;
        endforeach;
        $request::merge($result);
        $this->request = $request;
        return $this;
    }

    /**
     * Рекурсивно проходит по элементам массива и удаляет запрещенный теги
     * @param $array
     * @return mixed
     */
    private function arrayStripTags($array) {

        foreach ($array as $index => $array_item):
            if (is_array($array_item)):
                $this->arrayStripTags($array_item);
            else:
                $array[$index] = trim(strip_tags($array_item, $this->tags));
            endif;
        endforeach;
        return $array;
    }
}