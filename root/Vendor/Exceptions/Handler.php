<?php

namespace STALKER_CMS\Vendor\Exceptions;

use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Debug\ExceptionHandler as SymfonyExceptionHandler;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

/**
 * Class Handler
 * Отслеживание исключений и отображения страниц-ошибок или отладочной информации
 * https://laravel.com/docs/5.2/errors
 * @package STALKER_CMS\Vendor\Exceptions
 */
class Handler extends ExceptionHandler {

    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class
    ];

    /**
     * Метод используется для логирования исключений
     * @param Exception $e
     */
    public function report(Exception $e) {

        return parent::report($e);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param Exception $e
     * Обрабатывает исключения
     * Если установлени и активен пакет Bug Notifier отсылвает уведомления по Email
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response|SymfonyResponse
     */
    public function render($request, Exception $e) {

//              print_r($e->getMessage()); exit;
        $statusCode = $this->getStatusCode($e);
        if ($statusCode != 2005 && $statusCode != 404 && \PermissionsController::isPackageEnabled('solutions_bugnotify')):
            \STALKER_CMS\Solutions\Bugnotify\Http\Controllers\BugnotifyController::Notifier($e);
        endif;
        if ($request->wantsJson()):
            if (config('app.debug') === FALSE):
                $errorText = trans('root_lang::codes.' . $statusCode);
                if(empty($errorText)):
                    $errorText = $e->getMessage();
                endif;
                return \ResponseController::error($statusCode)->set('errorText', $errorText)->json();
            else:
                return parent::render($request, $e);
            endif;
        endif;
        return $this->convertExceptionToResponse($e);
        #return parent::render($request, $e);
    }

    /**
     * @param Exception $e
     * Возвращает страницу с ошибкой или страницу с отладочной информацией
     * в зависимости от включенного режима отладки
     * @return \Illuminate\Http\Response|SymfonyResponse
     */
    protected function convertExceptionToResponse(Exception $e) {

        if (config('app.debug')):
            return parent::convertExceptionToResponse($e);
        else:
            $statusCode = $this->getStatusCode($e);
            if (view()->exists("site_views::errors.$statusCode")):
                return response()->view(
                    "site_views::errors.$statusCode",
                    ['code' => $statusCode, 'message' => trans('root_lang::codes.' . $statusCode)],
                    $statusCode <= 1000 ? $statusCode : 500);
            endif;
            if (view()->exists("root_views::errors.$statusCode")):
                return response()->view(
                    "root_views::errors.$statusCode",
                    ['code' => $statusCode, 'message' => trans('root_lang::codes.' . $statusCode)],
                    $statusCode <= 1000 ? $statusCode : 500);
            else:
                return response()->view(
                    "root_views::errors.2000", ['code' => 2000, 'message' => $e->getMessage()], 500);
            endif;
        endif;
    }

    /**
     * @param Exception $e
     * Возвращает код ошибкив зависимости от типа исключения
     * @return int|mixed|string
     */
    protected function getStatusCode(Exception $e) {

        if (file_exists(base_path('.env')) === FALSE):
            return 2005;
        elseif ($e instanceof ModelNotFoundException):
            return 404;
        elseif ($e instanceof NotFoundHttpException):
            return 404;
        elseif ($e instanceof \InvalidArgumentException):
            return 404;
        elseif ($e instanceof MethodNotAllowedHttpException):
            return 405;
        elseif ($e instanceof HttpException):
            return $e->getStatusCode();
        elseif ($e->getCode() > 0):
            return $e->getCode();
        else:
            return 500;
        endif;
    }
}
