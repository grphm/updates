<?php
namespace STALKER_CMS\Vendor\Providers;

use Illuminate\Support\ServiceProvider as MainServiceProvider;

/**
 * Class ServiceProvider
 * @package STALKER_CMS\Vendor\Providers
 */
abstract class ServiceProvider extends MainServiceProvider {

    protected $RealPath;

    /**
     * Устанавливает путь к каталогу пакета
     * @param $path
     */
    protected function setPath($path) {

        $this->RealPath = realpath($path);
    }

    /********************************************************************************************************************/
    /**
     * Регистрация псевдонима указывающего на каталог представлений пакета
     * @param $namespace
     */
    protected function registerViews($namespace) {

        $this->loadViewsFrom($this->RealPath . '/Resources/Views', $namespace);
    }

    /**
     * Регистрация псевдонима указывающего на каталог локализации пакета
     * @param $namespace
     */
    protected function registerLocalization($namespace) {

        $this->loadTranslationsFrom($this->RealPath . '/Resources/Lang', $namespace);
    }

    /**
     * Регистрация blade директив
     */
    protected function registerBladeDirectives() {

    }

    /**
     * Регистрация псевдонима указывающего на конфигурационный файл пакета
     * @param $namespace
     * @param $config_file
     */
    protected function registerConfig($namespace, $config_file) {

        $this->setConfigurationFile($namespace, $config_file);
    }

    /**
     * Регистрация псевдонима указывающего на файл настроек пакета
     * @param $namespace
     * @param $settings_file
     */
    protected function registerSettings($namespace, $settings_file) {

        $this->setConfigurationFile($namespace, $settings_file);
    }

    /**
     * Регистрация псевдонима указывающего на файл действий пакета
     * @param $namespace
     * @param $actions_file
     */
    protected function registerActions($namespace, $actions_file) {

        $this->setConfigurationFile($namespace, $actions_file);
    }

    /**
     * Регистрация псевдонима указывающего на файл меню пакета
     * @param $namespace
     * @param $menu_file
     */
    protected function registerSystemMenu($namespace, $menu_file) {

        $this->setConfigurationFile($namespace, $menu_file);
    }

    /********************************************************************************************************************/
    /**
     * @param $namespace
     * @param $file
     */
    protected function setConfigurationFile($namespace, $file) {

        $packageConfigFile = $this->RealPath . '/' . $file;
        $config = $this->app['files']->getRequire($packageConfigFile);
        $this->app['config']->set($namespace, $config);
    }
}