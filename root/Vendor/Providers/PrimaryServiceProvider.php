<?php

namespace STALKER_CMS\Vendor\Providers;

use Illuminate\Support\Str;
use STALKER_CMS\Vendor\Http\Controllers\PackagesController;
use STALKER_CMS\Vendor\Http\Controllers\RequestController;
use STALKER_CMS\Vendor\Http\Controllers\ResponseController;
use STALKER_CMS\Vendor\Http\Controllers\ValidatorController;

/**
 * Базовый сервис провайдер CMS
 * Class PrimaryServiceProvider
 * @package STALKER_CMS\Vendor\Providers
 */
class PrimaryServiceProvider extends ServiceProvider {

    /**
     * Метод загрузки
     */
    public function boot() {

        $this->setPath(__DIR__ . '/../');
        $this->registerViews('root_views');
        $this->registerLocalization('root_lang');
        $this->registerBladeDirectives();
    }

    /**
     * Метод регистрации
     */
    public function register() {

        foreach (glob(__DIR__ . '/../Helpers/*.php') as $filename):
            require_once($filename);
        endforeach;
        \App::bind('ResponseController', function () {
            return new ResponseController();
        });
        \App::bind('RequestController', function () {
            return new RequestController();
        });
        \App::bind('ValidatorController', function () {
            return new ValidatorController();
        });
        \App::bind('PackagesController', function () {
            return new PackagesController();
        });
    }

    /********************************************************************************************************************/
    /**
     * Регистрация blade директив
     */
    protected function registerBladeDirectives() {

        \Blade::directive('continue', function () {
            return "<?php continue; ?>";
        });

        \Blade::directive('break', function () {
            return "<?php break; ?>";
        });

        \Blade::directive('numDimensions', function ($expression) {
            if (Str::startsWith($expression, '(')):
                $expression = substr($expression, 1, -1);
            endif;
            return "<?php echo str_pad($expression, 5, '0', STR_PAD_LEFT); ?>";
        });

        \Blade::directive('ProfileAvatar', function ($expression) {
            if (Str::startsWith($expression, '(')):
                $expression = substr($expression, 1, -1);
            endif;
            $Block = '';
            if (!empty($expression)):
                $expressions = [];
                foreach (explode(',', $expression) as $parameter):
                    $expressions[] = trim($parameter);
                endforeach;
                $Block .= '<div data-name="<?php echo ' . @$expressions[0] . '; ?>" class="profile-info-avatar text-center"<?php if(!empty(' . @$expressions[1] . ')): ?> data-src="<?php echo asset(' . @$expressions[1] . '); ?>" <?php endif; ?>>';
                if (isset($expressions[1])):
                    $Block .= '<?php if(!empty(' . $expressions[1] . ')): ?>';
                    $Block .= '<div data-src="<?php echo asset(' . @$expressions[1] . '); ?>">';
                    $Block .= '<div class="lightbox-item p-item">';
                    $Block .= '<img src="<?php echo asset(' . @$expressions[1] . '); ?>" style="max-height: 52px" alt="">';
                    $Block .= '</div>';
                    $Block .= '</div>';
                    $Block .= '<?php endif; ?>';
                endif;
                $Block .= '</div>';
            endif;
            return $Block;
        });

        \Blade::directive('preloader', function () {
            return '<div id="hellopreloader"><div id="hellopreloader_preload"></div></div>';
        });

        \Blade::directive('BtnSave', function () {

            $Block = '<button type="submit" class="btn bgm-green btn-float waves-effect waves-circle waves-float">';
            $Block .= '<i class="fa fa-save"></i>';
            $Block .= '</button>';
            return $Block;
        });

        \Blade::directive('BtnAdd', function ($expression) {

            if (Str::startsWith($expression, '(')):
                $expression = substr($expression, 1, -1);
            endif;
            $Block = '<a href="<?php echo route(' . $expression . ')?>" class="btn bgm-deeppurple m-btn btn-float waves-effect waves-circle waves-float">';
            $Block .= '<i class="zmdi zmdi-plus"></i>';
            $Block .= '</a>';
            return $Block;
        });

        \Blade::directive('translate', function ($expression) {

            if (Str::startsWith($expression, '(')):
                $expression = substr($expression, 2, -2);
            endif;
            return '<?php echo array_first([' . $expression . '], function ($key, $value) { return $key == \App::getLocale(); }); ?>';
        });

        \Blade::directive('dashboard', function () {

            $Block = "<?php if(Auth::check()): ?>";
            $Block .= '<a href="<?php echo route("dashboard"); ?>"><i class="zmdi zmdi-view-dashboard"></i></a>';
            $Block .= '<?php else: ?>';
            if (\Route::getRoutes()->hasNamedRoute('auth.login.index')):
                $Block .= '<a href="<?php echo route(\'auth.login.index\'); ?>"><i class="zmdi zmdi-key"></i></a>';
            else:
                $Block .= '<a href="/login"><i class="zmdi zmdi-key"></i></a>';
            endif;
            $Block .= "<?php endif; ?>";
            return $Block;
        });

        \Blade::directive('cropImage', function ($expression) {

            if (Str::startsWith($expression, '(')):
                $expression = substr($expression, 1, -1);
            endif;
            if (!empty($expression)):
                return "<?php echo \$__env->make('root_views::assets.cropping', ['image' => " . $expression . "])->render(); ?>";
            else:
                return "<?php echo \$__env->make('root_views::assets.cropping')->render(); ?>";
            endif;
        });

        \Blade::directive('set', function ($expression) {

            if (Str::startsWith($expression, '(')):
                $expression = substr($expression, 1, -1);
            endif;
            if (!empty($expression)):
                $expressions = [];
                foreach (explode(',', $expression, 2) as $parameter):
                    $expressions[] = trim($parameter);
                endforeach;
                if (isset($expressions[0]) && isset($expressions[1])):
                    return '<?php ' . $expressions[0] . ' = ' . $expressions[1] . '; ?>';
                elseif (isset($expressions[0])):
                    return '<?php ' . $expressions[0] . ' = NULL; ?>';
                endif;
            endif;
            return NULL;
        });

        \Blade::directive('print', function ($expression) {

            if (Str::startsWith($expression, '(')):
                $expression = substr($expression, 1, -1);
            endif;
            if (!empty($expression)):
                return "<?php echo '<pre>'; ?><?php print_r((array) $expression); ?><?php echo '</pre>'; ?>";
            endif;
            return NULL;
        });

        \Blade::directive('route', function ($expression) {

            if (Str::startsWith($expression, '(')):
                $expression = substr($expression, 2, -2);
            endif;
            if (!empty($expression)):
                if (\Route::getRoutes()->hasNamedRoute($expression)):
                    return "<?php echo route('" . $expression . "'); ?>";
                endif;
            endif;
            return NULL;
        });
    }
}