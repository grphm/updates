<?php

namespace STALKER_CMS\Vendor\Jobs;

use Illuminate\Bus\Queueable;

/**
 * Очереди
 * https://laravel.com/docs/5.2/queues
 * Class Job
 * @package STALKER_CMS\Vendor\Jobs
 */
abstract class Job {
    use Queueable;
}
