<?php
namespace STALKER_CMS\Vendor\Models;

use STALKER_CMS\Vendor\Interfaces\ModelInterface;

/**
 * Модель пакеты
 * Class Packages
 * @package STALKER_CMS\Vendor\Models
 */
class Packages extends BaseModel implements ModelInterface {

    /**
     * @var bool
     */
    public $timestamps = FALSE;

    /**
     * @var string
     */
    protected $table = 'packages';
    /**
     * @var array
     */
    protected $fillable = ['slug', 'title', 'description', 'install_path', 'enabled', 'required', 'relations', 'composer_config', 'order'];
    /**
     * @var array
     */
    protected $hidden = [];
    /**
     * @var array
     */
    protected $guarded = ['id', '_method', '_token'];

    /**
     * @param $request
     * @return mixed
     */
    public function insert($request) {

        $this->slug = $request['slug'];
        $this->title = $request['title'];
        $this->description = $request['description'];
        $this->composer_config = $request['composer_config'];
        $this->install_path = $request['install_path'];
        $this->enabled = $request['enabled'];
        $this->required = $request['required'];
        $this->relations = $request['relations'];
        $this->order = $request['order'];
        $this->save();
        return $this->id;
    }

    /**
     * @param $id
     * @param $request
     */
    public function replace($id, $request) {
        // TODO: Implement replace() method.
    }

    /**
     * @param array|int $id
     */
    public function remove($id) {
        // TODO: Implement remove() method.
    }

    /**
     * @param array $attributes
     */
    public function search(array $attributes) {
        // TODO: Implement search() method.
    }

    /**
     * @param array $attributes
     */
    public function filter(array $attributes) {
        // TODO: Implement filter() method.
    }

    /**
     * @param array $attributes
     * @return mixed
     */
    public function sort(array $attributes) {

        return self::orderBy('required', 'DESC')
            ->orderBy('order')
            ->whereEnabled(TRUE)
            ->where(function ($query) {
                $query->where('order', '>', 0);
                $query->where('order', '<', 9999);
            })
            ->get();
    }

    /**
     * Возвращает название пакета
     * @return string
     */
    public function getTitleAttribute() {

        if (!empty($this->attributes['title'])):
            $json = json_decode($this->attributes['title'], TRUE);
            return isset($json[\App::getLocale()]) ? $json[\App::getLocale()] : '<i class="zmdi zmdi-mood-bad"></i>';
        endif;
    }

    /**
     * Возвращает описание пакета
     * @return string
     */
    public function getDescriptionAttribute() {

        if (!empty($this->attributes['description'])):
            $json = json_decode($this->attributes['description'], TRUE);
            return isset($json[\App::getLocale()]) ? $json[\App::getLocale()] : '<i class="zmdi zmdi-mood-bad"></i>';
        endif;
    }

    /**
     * Последовательность загрузки пакетов
     * @return mixed
     */
    public static function getNestedPackages() {

        return self::orderBy('required', 'DESC')
            ->orderBy('order')
            ->whereEnabled(TRUE)
            ->where(function ($query) {
                $query->where('order', '>', 0);
                $query->where('order', '<', 9000);
            })
            ->get();
    }
}