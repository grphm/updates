<?php
namespace STALKER_CMS\Vendor\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Базовая модель CMS
 * Class BaseModel
 * @package STALKER_CMS\Vendor\Models
 */
class BaseModel extends Model {

}