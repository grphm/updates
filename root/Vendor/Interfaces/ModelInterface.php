<?php
namespace STALKER_CMS\Vendor\Interfaces;

/**
 * Интерфейс модели
 * Interface ModelInterface
 * @package STALKER_CMS\Vendor\Interfaces
 */
interface ModelInterface {

    /**
     * Добавление новой записи
     * @param $request
     */
    public function insert($request);

    /**
     * Обновлении записи
     * @param $id
     * @param $request
     * @return mixed
     */
    public function replace($id, $request);

    /**
     * Удаление записи
     * @param array|int $id
     * @return mixed
     */
    public function remove($id);

    /**
     * Поиск в модели
     * @param array $attributes
     * @return mixed
     */
    public function search(array $attributes);

    /**
     * Фильтр по модели
     * @param array $attributes
     * @return mixed
     */
    public function filter(array $attributes);

    /**
     * Сортировка по модели
     * @param array $attributes
     * @return mixed
     */
    public function sort(array $attributes);
}