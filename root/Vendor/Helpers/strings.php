<?php
namespace STALKER_CMS\Vendor\Helpers;

/**
 * Возвращает цену прописью
 * @param $num
 * @return bool|string
 */
function price2str($num) {

    if (empty($num)):
        return FALSE;
    endif;

    $nul = 'ноль';
    $ten = array(
        array('', 'один', 'два', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'),
        array('', 'одна', 'две', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'),
    );
    $a20 = array('десять', 'одиннадцать', 'двенадцать', 'тринадцать', 'четырнадцать', 'пятнадцать', 'шестнадцать',
        'семнадцать', 'восемнадцать', 'девятнадцать');
    $tens = array(2 => 'двадцать', 'тридцать', 'сорок', 'пятьдесят', 'шестьдесят', 'семьдесят', 'восемьдесят',
        'девяносто');
    $hundred = array('', 'сто', 'двести', 'триста', 'четыреста', 'пятьсот', 'шестьсот', 'семьсот', 'восемьсот',
        'девятьсот');
    $unit = array( // Units
        array('копейка', 'копейки', 'копеек', 1),
        array('рубль', 'рубля', 'рублей', 0),
        array('тысяча', 'тысячи', 'тысяч', 1),
        array('миллион', 'миллиона', 'миллионов', 0),
        array('миллиард', 'милиарда', 'миллиардов', 0),
    );
    list($rub, $kop) = explode('.', sprintf("%015.2f", floatval($num)));
    $out = array();
    if (intval($rub) > 0):
        foreach (str_split($rub, 3) as $uk => $v):
            if (!intval($v))
                continue;
            $uk = sizeof($unit) - $uk - 1; // unit key
            $gender = $unit[$uk][3];
            list($i1, $i2, $i3) = array_map('intval', str_split($v, 1));
            $out[] = $hundred[$i1]; # 1xx-9xx
            if ($i2 > 1)
                $out[] = $tens[$i2] . ' ' . $ten[$gender][$i3]; # 20-99
            else $out[] = $i2 > 0 ? $a20[$i3] : $ten[$gender][$i3]; # 10-19 | 1-9
            if ($uk > 1)
                $out[] = morph($v, $unit[$uk][0], $unit[$uk][1], $unit[$uk][2]);
        endforeach;
    else:
        $out[] = $nul;
    endif;
    $out[] = morph(intval($rub), $unit[1][0], $unit[1][1], $unit[1][2]); // rub
    $out[] = $kop . ' ' . morph($kop, $unit[0][0], $unit[0][1], $unit[0][2]); // kop
    return trim(preg_replace('/ {2,}/', ' ', join(' ', $out)));
}

/**
 * @param $n
 * @param $f1
 * @param $f2
 * @param $f5
 * @return mixed
 */
function morph($n, $f1, $f2, $f5) {

    $n = abs(intval($n)) % 100;
    if ($n > 10 && $n < 20)
        return $f5;
    $n = $n % 10;
    if ($n > 1 && $n < 5)
        return $f2;
    if ($n == 1)
        return $f1;
    return $f5;
}

/**
 * Возвращает инициалы из полного имени
 * @param $fio
 * @return mixed|string
 */
function getInitials($fio) {

    if (empty($fio)):
        return 'Пользователь';
    else:
        $fio_mas = explode(' ', $fio);
        switch (count($fio_mas)):
            case 2 :
                return preg_replace('/(\w+) (\w)\w+/iu', '$1 $2.', $fio);
            case 3 :
                return preg_replace('/(\w+) (\w)\w+ (\w)\w+/iu', '$1 $2. $3.', $fio);
        endswitch;
    endif;
    return $fio;
}

/**
 * Транслитерация строки
 * @param $s
 * @param bool|true $lower
 * @param string $space
 * @return mixed|string
 */
function transliteration($s, $lower = true, $space = '-') {

    $s = (string)$s; // преобразуем в строковое значение
    $s = strip_tags($s); // убираем HTML-теги
    $s = str_replace(array("\n", "\r"), " ", $s); // убираем перевод каретки
    $s = preg_replace('/ +/', ' ', $s); // удаляем повторяющие пробелы
    $s = trim($s); // убираем пробелы в начале и конце строки
    if ($lower)
        $s = function_exists('mb_strtolower') ? mb_strtolower($s) : strtolower($s); // переводим строку в нижний регистр (иногда надо задать локаль)
    $s = strtr($s, array(
        'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'e', 'ж' => 'j', 'з' => 'z',
        'и' => 'i', 'й' => 'y', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r',
        'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ч' => 'ch', 'ш' => 'sh',
        'щ' => 'sch', 'ы' => 'y', 'э' => 'e', 'ю' => 'yu', 'я' => 'ya', 'ъ' => '', 'ь' => '', 'А' => 'A',
        'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D', 'Е' => 'E', 'Ё' => 'E', 'Ж' => 'J', 'З' => 'Z',
        'И' => 'I', 'Й' => 'Y', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O', 'П' => 'P', 'Р' => 'R',
        'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C', 'Ч' => 'CH', 'Ш' => 'SH',
        'Щ' => 'SCH', 'Ы' => 'Y', 'Э' => 'E', 'Ю' => 'YU', 'Я' => 'YA', 'Ъ' => '', 'Ь' => '',
    ));
    $s = preg_replace("/[^0-9A-Za-z-_ ]/i", "", $s); // очищаем строку от недопустимых символов
    $s = str_replace(" ", $space, $s); // заменяем пробелы знаком минус
    return $s; // возвращаем результат
}

/**
 * Мультибайтовая функция, аналогичной функции ucfirst(),
 * которая переводит первый символ строки в верхний регистр
 * @param $str
 * @param string $encoding
 * @return string
 */
function mb_ucfirst($str, $encoding = 'UTF-8') {

    $first = mb_substr($str, 0, 1, $encoding);
    $rest = mb_substr($str, 1, strlen($str), $encoding);
    return mb_strtoupper($first, $encoding) . $rest;
}

/**
 * Определяет кодировку строки
 * @param $string
 * @param int $pattern_size
 * @return string
 */
function detect_encoding($string, $pattern_size = 50) {

    $list = array('cp1251', 'utf-8', 'ascii', '855', 'KOI8R', 'ISO-IR-111', 'CP866', 'KOI8U');
    $c = strlen($string);
    if ($c > $pattern_size):
        $string = substr($string, floor(($c - $pattern_size) / 2), $pattern_size);
        $c = $pattern_size;
    endif;

    $reg1 = '/(\xE0|\xE5|\xE8|\xEE|\xF3|\xFB|\xFD|\xFE|\xFF)/i';
    $reg2 = '/(\xE1|\xE2|\xE3|\xE4|\xE6|\xE7|\xE9|\xEA|\xEB|\xEC|\xED|\xEF|\xF0|\xF1|\xF2|\xF4|\xF5|\xF6|\xF7|\xF8|\xF9|\xFA|\xFC)/i';

    $mk = 10000;
    $enc = 'ascii';
    foreach ($list as $item):
        $sample1 = @iconv($item, 'cp1251', $string);
        $gl = @preg_match_all($reg1, $sample1, $arr);
        $sl = @preg_match_all($reg2, $sample1, $arr);
        if (!$gl || !$sl):
            continue;
        endif;
        $k = abs(3 - ($sl / $gl));
        $k += $c - $gl - $sl;
        if ($k < $mk):
            $enc = $item;
            $mk = $k;
        endif;
    endforeach;
    return $enc;
}

/**
 * Удалить двойные слешы из строки
 * @param $string
 * @return mixed
 */
function double_slash($string) {

    return preg_replace('|([/]+)|s', '/', $string);
}

/**
 * Добавить слеш в начало строки
 * @param $string
 * @return string
 */
function add_first_slash($string) {

    if (mb_substr($string, 0, 1) != '/'):
        return '/' . $string;
    else:
        return $string;
    endif;
}

/**
 * Убрать слеш из начала строки
 * @param $string
 * @return string
 */
function remove_first_slash($string) {

    if (mb_substr($string, 0, 1) == '/'):
        return mb_substr($string, 1);
    else:
        return $string;
    endif;
}

/**
 * Убирает пробелы между частами $string разделенные $delimiter
 * @param $string
 * @param string $delimiter
 * @return string
 */
function trim_delimiter($string, $delimiter = ',') {

    $trim_string = [];
    foreach (explode($delimiter, $string) as $str):
        $trim_string[] = trim($str);
    endforeach;
    return implode($delimiter, $trim_string);
}

/**
 * Убирает многострочность в тексте
 * @param $text
 * @return mixed
 */
function multiSpace($text) {

    return preg_replace("~\s\s+~is", " ", $text);
}