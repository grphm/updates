<?php
namespace STALKER_CMS\Vendor\Helpers;

/**
 * Проверяет подключение к Базе Данных используя mysqli_connect
 * @param $dbuser
 * @param $dbpasswd
 * @param $dbname
 * @param string $dblocation
 * @return bool
 */
function connectMySQL($dbuser, $dbpasswd, $dbname, $dblocation = "localhost") {

    try {
        $dbcnx = \mysqli_connect($dblocation, $dbuser, $dbpasswd);
        if (!$dbcnx):
            return FALSE;
        endif;
        if (!mysqli_select_db($dbcnx, $dbname)):
            return FALSE;
        endif;
    } catch (\Exception $e) {
        return FALSE;
    }
    return TRUE;
}

/**
 * Возращает данные для подключения к Базе Данных из файла конфигурации
 * @return array|bool
 */
function dbConfig() {

    if (file_exists(base_path('.env')) === FALSE):
        return FALSE;
    endif;

    $config_file = file(base_path('.env'));
    $config = [
        'DB_HOST' => 'localhost',
        'DB_DATABASE' => '',
        'DB_USERNAME' => '',
        'DB_PASSWORD' => '',
    ];
    if (is_array($config_file)):
        foreach ($config_file as $key => $value):
            $line = explode('=', $value);
            switch (@$line[0]):
                case 'DB_HOST':
                    $config['DB_HOST'] = trim(@$line[1]);
                    break;
                case 'DB_DATABASE':
                    $config['DB_DATABASE'] = trim(@$line[1]);
                    break;
                case 'DB_USERNAME':
                    $config['DB_USERNAME'] = trim(@$line[1]);
                    break;
                case 'DB_PASSWORD':
                    $config['DB_PASSWORD'] = trim(@$line[1]);
                    break;
            endswitch;
        endforeach;
    endif;
    return $config;
}