<?php
namespace STALKER_CMS\Vendor\Helpers;

/**
 * Возвращает текст по ключу массива исходя из текущей локали
 * @param array $trans
 * @return mixed
 */
function array_translate(array $trans) {

    return array_first($trans, function($key, $value) {

        return $key == \App::getLocale();
    });
}

/**
 * Рекурсивный обход массива
 * Возвращает массив в виде "дерева"
 * @param $rs
 * @param $parent
 * @return array
 */
function recursive(&$rs, $parent) {

    $out = [];
    if(!isset($rs[$parent])):
        return $out;
    endif;
    foreach($rs[$parent] as $row):
        $chidls = recursive($rs, $row['id']);
        if($chidls):
            $row['sub_menu'] = $chidls;
        endif;
        $out[] = $row;
    endforeach;
    return $out;
}

/**
 * Сортировка многомернеого массива
 * http://php.net/manual/ru/function.array-multisort.php#91638
 * @param $array
 * @param $cols
 * @return array
 */
function array_msort($array, $cols) {

    $colarr = array();
    foreach($cols as $col => $order) {
        $colarr[$col] = array();
        foreach($array as $k => $row) {
            $colarr[$col]['_'.$k] = strtolower($row[$col]);
        }
    }
    $eval = 'array_multisort(';
    foreach($cols as $col => $order) {
        $eval .= '$colarr[\''.$col.'\'],'.$order.',';
    }
    $eval = substr($eval, 0, -1).');';
    eval($eval);
    $ret = array();
    foreach($colarr as $col => $arr) {
        foreach($arr as $k => $v) {
            $k = substr($k, 1);
            if(!isset($ret[$k])) {
                $ret[$k] = $array[$k];
            }
            $ret[$k][$col] = $array[$k][$col];
        }
    }
    return $ret;
}