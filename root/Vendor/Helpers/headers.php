<?php
namespace STALKER_CMS\Vendor\Helpers;

/**
 * Возвращает массив заголовков для скачиваемого фала
 * @param $document
 * @return array
 */
function returnDownloadHeaders($document) {

    return array(
        'Pragma: public',
        'Expires: 0',
        'Cache-Control: must-revalidate, post-check=0, pre-check=0',
        'Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime(public_path($document['path']))) . ' GMT',
        'Cache-Control: private',
        'Content-Type: ' . $document['mimetype'],
        'Content-Disposition: attachment; filename="' . basename(public_path($document['path'])) . '"',
        'Content-Transfer-Encoding: binary',
        'Content-Length: ' . filesize(public_path($document['path'])),
        'Connection: close'
    );
}