<?php

return [
    'format_not_supported' => 'El formato no es compatible',
    'valid_size' => 'El archivo subido excede el tamaño permitido. <br> Tamaño máximo'
];