@extends('root_views::layouts.errors')
@section('title', $message)
@section('body-class'){{ 'four-zero-content' }}@stop
@section('content')
    <div class="four-zero">
        <h2>#{{ $code }}</h2>
        <small class="f-20">{{ $message }}</small>
        <footer>
            <a href="{!! url('install') !!}"><i class="zmdi zmdi-puzzle-piece"></i></a>
        </footer>
    </div>
@stop
@section('scripts_before')
@stop
@section('scripts_after')
@stop