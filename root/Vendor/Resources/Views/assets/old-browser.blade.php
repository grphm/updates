<!-- Older IE warning message -->
<!--[if lt IE 9]>
<div class="ie-warning">
    <h1 class="c-white">Предупреждение!</h1>

    <p>
        Вы используете устаревшую версию Internet Explorer.<br>
        Пожалуйста, обновите его на любой из следующих веб-браузеров.
    </p>

    <div class="iew-container">
        <ul class="iew-download">
            <li>
                <a href="http://www.google.com/chrome/">
                    <img src="{{ asset('core/img/browsers/chrome.png') }}" alt="">

                    <div>Chrome</div>
                </a>
            </li>
            <li>
                <a href="https://www.mozilla.org/en-US/firefox/new/">
                    <img src="{{ asset('core/img/browsers/firefox.png') }}" alt="">

                    <div>Firefox</div>
                </a>
            </li>
            <li>
                <a href="http://www.opera.com">
                    <img src="{{ asset('core/img/browsers/opera.png') }}" alt="">

                    <div>Opera</div>
                </a>
            </li>
            <li>
                <a href="https://www.apple.com/safari/">
                    <img src="{{ asset('core/img/browsers/safari.png') }}" alt="">

                    <div>Safari</div>
                </a>
            </li>
            <li>
                <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                    <img src="{{ asset('core/img/browsers/ie.png') }}" alt="">

                    <div>IE (Новый)</div>
                </a>
            </li>
        </ul>
    </div>
    <p>Приносим извинения за неудобства!</p>
</div>
<![endif]-->