<div class="cropping-image fileinput fileinput-new">
    <input type="file" name="file" class="hidden" autocomplete="off">
    <input type="hidden" name="photo" autocomplete="off">
    <input type="hidden" name="thumbnail" autocomplete="off">

    <div class="fileinput-preview thumbnail p-0">
        @if(!empty($image))
            <div class="ci-avatar">
                <img style="width: 100%" alt="" src="{{ asset($image) }}">
            </div>
        @endif
    </div>
    <div class="js-file-error"></div>
    <div>
        <button type="button" class="btn btn-info btn-xs waves-effect js-choice-image">
            @lang('root_lang::cropping.select_image')
        </button>
        <button type="button" class="btn btn-danger btn-xs hidden waves-effect js-close-image">
            @lang('root_lang::cropping.delete_image')
        </button>
    </div>
</div>
<div class="cmsCropOverlay js-cmsCropOverlay">
    <div class="popup">
        <div class="popup__pi">
            <div class="pi__image js-crop-container"></div>
            <div class="pi__btns">
                <div class="no-padding no-margin pull-right">
                    <a href="javascript:void(0);" class="js-crop-save cbtn-save">
                        <span>@lang('root_lang::cropping.select')</span>
                    </a>
                    <a href="javascript:void(0);" class="js-crop-cancel cbtn-cancel">
                        <span>@lang('root_lang::cropping.cancel')</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>