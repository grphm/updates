<?php
namespace STALKER_CMS\Core\Dictionaries\Models;

use Carbon\Carbon;
use League\Flysystem\Exception;
use STALKER_CMS\Vendor\Interfaces\ModelInterface;
use STALKER_CMS\Vendor\Models\BaseModel;
use STALKER_CMS\Vendor\Traits\ModelTrait;

/**
 * Модель Элемент словаря
 * Class DictionaryLists
 * @package STALKER_CMS\Core\Dictionaries\Models
 */
class DictionaryLists extends BaseModel implements ModelInterface {

    use ModelTrait;

    /**
     * @var string
     */
    protected $table = 'dictionary_lists';
    /**
     * @var array
     */
    protected $fillable = [];
    /**
     * @var array
     */
    protected $hidden = [];
    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @param $request
     * @return $this
     */
    public function insert($request) {

        $this->dictionary_id = $request::input('dictionary_id');
        $this->title = $request::input('title');
        $this->order = $this::max('order') + 1;
        $this->save();
        return $this;
    }

    /**
     * @param $id
     * @param $request
     * @return mixed
     */
    public function replace($id, $request) {

        $model = $this::whereFind(['id' => $id]);
        $model->title = $request::input('title');
        $model->save();
        $model->touch();
        return $model;
    }

    /**
     * @param array|int $id
     * @return mixed
     */
    public function remove($id) {

        DictionaryListsFields::where('dictionary_list_id', $id)->delete();
        $instance = static::findOrFail($id);
        return $instance->delete();
    }

    /**
     * @param array $attributes
     */
    public function search(array $attributes) {
        // TODO: Implement search() method.
    }

    /**
     * @param array $attributes
     */
    public function filter(array $attributes) {
        // TODO: Implement filter() method.
    }

    /**
     * @param array $attributes
     */
    public function sort(array $attributes) {
        // TODO: Implement sort() method.
    }

    /**
     * Поля элемента словаря
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function fields() {

        return $this->hasMany('\STALKER_CMS\Core\Dictionaries\Models\DictionaryListsFields', 'dictionary_list_id', 'id');
    }

    /***************************************************************************************************************/
    /**
     * @return array
     */
    public static function getStoreRules() {

        return ['title' => 'required'];
    }

    /**
     * @return array
     */
    public static function getUpdateRules() {

        return ['title' => 'required'];
    }
}