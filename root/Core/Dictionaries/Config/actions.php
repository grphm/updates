<?php

return [
    'dictionaries' => [
        'title' => ['ru' => 'Просмотр', 'en' => 'View', 'es' => 'Ver'],
        'enabled' => FALSE,
        'icon' => 'zmdi zmdi-eye'
    ],
    'constructor' => [
        'title' => ['ru' => 'Конструктор', 'en' => 'Designer', 'es' => 'Diccionario'],
        'enabled' => FALSE,
        'icon' => 'zmdi zmdi-check-circle-u'
    ],
    'create' => [
        'title' => ['ru' => 'Создание', 'en' => 'Add', 'es' => 'Añadir'],
        'enabled' => FALSE,
        'icon' => 'zmdi zmdi-collection-plus'
    ],
    'edit' => [
        'title' => ['ru' => 'Редактирование', 'en' => 'Edit', 'es' => 'Edición'],
        'enabled' => FALSE,
        'icon' => 'zmdi zmdi-edit'
    ],
    'delete' => [
        'title' => ['ru' => 'Удаление', 'en' => 'Delete', 'es' => 'Eliminar'],
        'enabled' => FALSE,
        'icon' => 'zmdi zmdi-delete'
    ],
];