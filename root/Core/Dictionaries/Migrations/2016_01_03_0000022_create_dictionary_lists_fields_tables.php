<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDictionaryListsFieldsTables extends Migration {

    public function up() {

        Schema::create('dictionary_lists_fields', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dictionary_list_id', FALSE, TRUE)->nullable()->index();
            $table->string('field', 50)->nullable()->index();
            $table->text('value')->nullable();
            $table->timestamps();
        });
    }

    public function down() {

        Schema::dropIfExists('dictionary_lists_fields');
    }
}

