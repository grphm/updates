<?php

return [
    'edit' => 'Редактировать',
    'delete' => [
        'question' => 'Удалить элемент словаря',
        'confirmbuttontext' => 'Да, удалить',
        'cancelbuttontext' => 'Нет, я передумал',
        'submit' => 'Удалить',
    ],
    'empty' => 'Список пустой',
    'insert' => [
        'breadcrumb' => 'Добавить',
        'title' => 'Добавление элемента словаря',
        'form' => [
            'title' => 'Название',
            'submit' => 'Сохранить'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Редактировать',
        'title' => 'Редактирование элемента словаря',
        'form' => [
            'title' => 'Название',
            'submit' => 'Сохранить'
        ]
    ]
];