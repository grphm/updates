<?php

return [
    'edit' => 'Editar',
    'delete' => [
        'question' => 'Eliminar elemento de diccionario',
        'confirmbuttontext' => 'Sí, eliminar',
        'cancelbuttontext' => 'He cambiado de idea',
        'submit' => 'Eliminar'
    ],
    'empty' => 'Lista está vacía',
    'insert' => [
        'breadcrumb' => 'Añadir',
        'title' => 'Adición de una entrada de diccionario',
        'form' => [
            'title' => 'Título',
            'submit' => 'Guardar'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Editar',
        'title' => 'Edición de la entrada de diccionario',
        'form' => [
            'title' => 'Título',
            'submit' => 'Guardar'
        ]
    ]
];