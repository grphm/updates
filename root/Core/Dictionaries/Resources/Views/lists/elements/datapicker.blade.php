@if(isset($element['name']))
    <div class="col-sm-2 p-0">
        <div class="form-group fg-float">
            <div class="fg-line">
                {!! Form::text('fields['.$element['name'].']', isset($element['value']) ? $element['value'] : NULL, ['class' => 'input-sm form-control fg-input date-picker date-mask text-center']) !!}
            </div>
            <label class="fg-label">{!! $element['placeholder'] !!}</label>
        </div>
    </div>
    <div class="clearfix"></div>
@else
    <code>{{ $element['placeholder'] }}. Variable name is not set</code>
@endif