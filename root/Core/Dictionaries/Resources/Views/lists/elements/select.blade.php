@if(isset($element['name']))
    @if(\STALKER_CMS\Core\Dictionaries\Models\Dictionary::whereSlug($element['data_item'])->exists())
        <?php
        try {
            $dictionary = \STALKER_CMS\Core\Dictionaries\Models\Dictionary::whereSlug($element['data_item'])->first()->lists()->pluck('title', 'id');
        } catch(\Exception $e) {
            $dictionary = [];
        }
        ?>
        <div class="form-group">
            <p class="f-500 c-black m-b-15">{!! $element['placeholder'] !!}</p>
            @if(isset($element['multiple']) && $element['multiple'])
                {!! Form::select('fields['.$element['name'].'][]', $dictionary, isset($element['value']) ? explode(',', $element['value']) : NULL, ['class' => 'tag-select-multiple', 'multiple' => TRUE]) !!}
            @else
                {!! Form::select('fields['.$element['name'].']', $dictionary, isset($element['value']) ? $element['value'] : NULL, ['class' => 'tag-select']) !!}
            @endif
        </div>
    @elseif($element['data_item'] == 'vendor_countries')
        <?php $countries = \STALKER_CMS\Vendor\Models\Countries::whereLocale(\App::getLocale())->orderBy('title')->pluck('title', 'id'); ?>
        <div class="form-group">
            <p class="f-500 c-black m-b-15">{!! $element['placeholder'] !!}</p>
            @if(isset($element['multiple']) && $element['multiple'])
                {!! Form::select('fields['.$element['name'].'][]', $countries, isset($element['value']) ? explode(',', $element['value']) : NULL, ['class' => 'tag-select-multiple', 'multiple' => TRUE]) !!}
            @else
                {!! Form::select('fields['.$element['name'].']', $countries, isset($element['value']) ? $element['value'] : NULL, ['class' => 'tag-select']) !!}
            @endif
        </div>
    @elseif($element['data_item'] == 'vendor_cities')
        <?php $cities = \STALKER_CMS\Vendor\Models\Cities::whereLocale(\App::getLocale())->orderBy('title')->pluck('title', 'id'); ?>
        <div class="form-group">
            <p class="f-500 c-black m-b-15">{!! $element['placeholder'] !!}</p>
            @if(isset($element['multiple']) && $element['multiple'])
                {!! Form::select('fields['.$element['name'].'][]', $cities, isset($element['value']) ? explode(',', $element['value']) : NULL, ['class' => 'tag-select-multiple', 'multiple' => TRUE]) !!}
            @else
                {!! Form::select('fields['.$element['name'].']', $cities, isset($element['value']) ? $element['value'] : NULL, ['class' => 'tag-select']) !!}
            @endif
        </div>
    @elseif($element['data_item'] == 'core_galleries')
        Еще не реализовано
    @elseif($element['data_item'] == 'core_uploads')
        Еще не реализовано
    @endif
@else
    <code>{{ $element['placeholder'] }}. Variable name is not set</code>
@endif