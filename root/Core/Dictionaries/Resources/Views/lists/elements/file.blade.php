@if(isset($element['name']))
    <div class="form-group">
        <p class="f-500 c-black m-b-15">{!! $element['placeholder'] !!}</p>
        {!! Form::select('fields['.$element['name'].']', $files, isset($element['value']) ? $element['value'] : NULL, ['class' => 'tag-select']) !!}
    </div>
@else
    <code>{{ $element['placeholder'] }}. Variable name is not set</code>
@endif