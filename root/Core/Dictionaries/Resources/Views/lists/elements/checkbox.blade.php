@if(isset($element['name']))
    <div class="form-group">
        {!! Form::text('fields['.$element['name'].']', isset($element['value']) && $element['value'] == 1  ? 1 : 0, ['class' => 'hidden']) !!}
        <div class="checkbox">
            <label>
                {!! Form::checkbox(\Illuminate\Support\Str::random(10), TRUE, isset($element['value']) && $element['value'] == 1  ? TRUE : FALSE, ['autocomplete' => 'off']) !!}
                <i class="input-helper"></i> {{ $element['placeholder'] }}
            </label>
        </div>
    </div>
@else
    <code>{{ $element['placeholder'] }}. Variable name is not set</code>
@endif