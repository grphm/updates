@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('head')
@stop
@section('breadcrumb')
    <ol class="breadcrumb" style="margin-bottom: 5px;">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li>
            <a href="{{ route('core.dictionaries.index') }}">
                <i class="{{ config('core_dictionaries::menu.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_dictionaries::menu.title')) !!}
            </a>
        </li>
        <li class="active">
            {{ $dictionary->title }}
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>{{ $dictionary->title }}</h2>
    </div>
    @BtnAdd('core.dictionaries.lists_create', $dictionary->id)
    <div class="card">
        <div class="card-body card-padding m-h-250">
            @if($lists->count())
                <div class="listview lv-bordered lv-lg">
                    <div class="lv-body">
                        <ul class="nestable-list p-l-0"
                            data-action-url="{!! route('core.dictionaries.lists_sortable', $dictionary->id) !!}">
                            @foreach($lists as $list)
                                <li class="js-item-container lv-item media" data-element="{!! $list->id !!}">
                                    <div class="pull-left m-t-5">
                                        <i class="zmdi zmdi-swap-vertical f-20 p-t-10 cursor-move"></i>
                                    </div>
                                    <div class="media-body">
                                        <div class="lv-title">{{ $list->title }}</div>
                                        <ul class="lv-attrs">
                                            <li>ID: @numDimensions($list->id)</li>
                                        </ul>
                                        <div class="lv-actions actions dropdown">
                                            <a aria-expanded="true" data-toggle="dropdown" href="">
                                                <i class="zmdi zmdi-more-vert"></i>
                                            </a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li>
                                                    <a href="{{ route('core.dictionaries.lists_edit', [$dictionary->id, $list->id]) }}">
                                                        @lang('core_dictionaries_lang::lists.edit')
                                                    </a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    {!! Form::open(['route' => ['core.dictionaries.lists_destroy', $dictionary->id, $list->id], 'method' => 'DELETE']) !!}
                                                    <button type="submit"
                                                            class="form-confirm-warning btn-link pull-right c-red p-r-15"
                                                            autocomplete="off"
                                                            data-question="@lang('core_dictionaries_lang::lists.delete.question') &laquo;{{ $list->title }}&raquo;?"
                                                            data-confirmbuttontext="@lang('core_dictionaries_lang::lists.delete.confirmbuttontext')"
                                                            data-cancelbuttontext="@lang('core_dictionaries_lang::lists.delete.cancelbuttontext')">
                                                        @lang('core_dictionaries_lang::lists.delete.submit')
                                                    </button>
                                                    {!! Form::close() !!}
                                                </li>

                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @else
                <h2 class="f-16 c-gray">@lang('core_dictionaries_lang::dictionaries.empty')</h2>
            @endif
        </div>
    </div>
@stop