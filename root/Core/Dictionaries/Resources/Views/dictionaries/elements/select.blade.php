{!! Form::hidden('type', 'select', ['class' => 'element-input-name']) !!}
<div class="f-500 c-gray m-b-25">
    @lang('core_dictionaries_lang::elements.select.title')
    @if(isset($delete) && $delete === FALSE)
    @else
        <ul class="actions pull-right">
            <li class="dropdown">
                <a aria-expanded="false" data-toggle="dropdown" href="">
                    <i class="zmdi zmdi-more-vert"></i>
                </a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li>
                        <a class="remove-clone-element"
                           href="javascript:void(0)">@lang('core_dictionaries_lang::dictionaries.insert.form.remove-element')
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    @endif
</div>
<div class="form-group fg-float">
    <div class="fg-line">
        {!! Form::text('name', isset($element['name']) ? $element['name'] : NULL, ['class'=>'input-sm form-control fg-input element-input-name']) !!}
    </div>
    <label class="fg-label">@lang('core_dictionaries_lang::elements.select.name')</label>
</div>
<div class="form-group fg-float">
    <div class="fg-line">
        {!! Form::text('placeholder', isset($element['placeholder']) ? $element['placeholder'] : NULL, ['class'=>'input-sm form-control fg-input element-input-name']) !!}
    </div>
    <label class="fg-label">@lang('core_dictionaries_lang::elements.select.placeholder')</label>
</div>
<div class="form-group">
    <p class="c-gray m-b-10">@lang('core_dictionaries_lang::elements.select.data_item')</p>
    {!! Form::select('data_item', $information_items, isset($element['data_item']) ? $element['data_item'] : NULL, ['class' => 'p-5 w-100 element-input-name', 'autocomplete' => 'off']) !!}
</div>
<div class="form-group">
    <div class="checkbox">
        <label>
            {!! Form::checkbox('multiple', TRUE, isset($element['multiple']) ? TRUE : FALSE, ['autocomplete' => 'off', 'class' => 'element-input-name']) !!}
            <i class="input-helper"></i> @lang('core_dictionaries_lang::elements.select.multiple')
        </label>
    </div>
</div>