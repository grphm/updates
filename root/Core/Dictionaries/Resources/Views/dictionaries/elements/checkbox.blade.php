{!! Form::hidden('type', 'checkbox', ['class' => 'element-input-name']) !!}
<div class="f-500 c-gray m-b-25">
    @lang('core_dictionaries_lang::elements.checkbox.title')
    @if(isset($delete) && $delete === FALSE)
    @else
        <ul class="actions pull-right">
            <li class="dropdown">
                <a aria-expanded="false" data-toggle="dropdown" href="">
                    <i class="zmdi zmdi-more-vert"></i>
                </a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li>
                        <a class="remove-clone-element"
                           href="javascript:void(0)">@lang('core_dictionaries_lang::dictionaries.insert.form.remove-element')
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    @endif
</div>
<div class="form-group fg-float">
    <div class="fg-line">
        {!! Form::text('name', isset($element['name']) ? $element['name'] : NULL, ['class'=>'input-sm form-control fg-input element-input-name']) !!}
    </div>
    <label class="fg-label">@lang('core_dictionaries_lang::elements.checkbox.name')</label>
</div>
<div class="form-group fg-float">
    <div class="fg-line">
        {!! Form::text('placeholder', isset($element['placeholder']) ? $element['placeholder'] : NULL, ['class'=>'input-sm form-control fg-input element-input-name']) !!}
    </div>
    <label class="fg-label">@lang('core_dictionaries_lang::elements.checkbox.placeholder')</label>
</div>