@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('head')
@stop
@section('breadcrumb')
    <ol class="breadcrumb" style="margin-bottom: 5px;">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('core_dictionaries::menu.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_dictionaries::menu.title')) !!}
        </li>
        <li class="active">
            <i class="{{ $dictionaries_icon }}"></i> {!! $dictionaries_title !!}
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="{{ $dictionaries_icon }}"></i> {!! $dictionaries_title !!}
        </h2>
    </div>
    @BtnAdd('core.dictionaries.create')
    <div class="card">
        <div class="card-body card-padding m-h-250">
            @if($dictionaries->count())
                <div class="listview lv-bordered lv-lg">
                    <div class="lv-body">
                        @foreach($dictionaries as $dictionary)
                            <div class="js-item-container lv-item media">
                                <div class="media-body">
                                    <div class="lv-title">{{ $dictionary->title }}</div>
                                    <ul class="lv-attrs">
                                        <li>ID: @numDimensions($dictionary->id)</li>
                                        <li>
                                            @lang('core_dictionaries_lang::dictionaries.slug'):
                                            {{ $dictionary->slug }}
                                        </li>
                                        <li>
                                            @lang('core_dictionaries_lang::dictionaries.elements'):
                                            {{ count($dictionary->lists) }}
                                        </li>
                                    </ul>
                                    <div class="lv-actions actions dropdown">
                                        <a aria-expanded="true" data-toggle="dropdown" href="">
                                            <i class="zmdi zmdi-more-vert"></i>
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li>
                                                <a href="{{ route('core.dictionaries.edit', $dictionary->id) }}">
                                                    @lang('core_dictionaries_lang::dictionaries.edit')
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{ route('core.dictionaries.lists_index', $dictionary->id) }}">
                                                    @lang('core_dictionaries_lang::dictionaries.lists')
                                                </a>
                                            </li>
                                            @if(count($dictionary->lists) == 0)
                                                <li class="divider"></li>
                                                <li>
                                                    {!! Form::open(['route' => ['core.dictionaries.destroy', $dictionary->id], 'method' => 'DELETE']) !!}
                                                    <button type="submit"
                                                            class="form-confirm-warning btn-link pull-right c-red p-r-15"
                                                            autocomplete="off"
                                                            data-question="@lang('core_dictionaries_lang::dictionaries.delete.question') &laquo;{{ $dictionary->title }}&raquo;?"
                                                            data-confirmbuttontext="@lang('core_dictionaries_lang::dictionaries.delete.confirmbuttontext')"
                                                            data-cancelbuttontext="@lang('core_dictionaries_lang::dictionaries.delete.cancelbuttontext')">
                                                        @lang('core_dictionaries_lang::dictionaries.delete.submit')
                                                    </button>
                                                    {!! Form::close() !!}
                                                </li>
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @else
                <h2 class="f-16 c-gray">@lang('core_dictionaries_lang::dictionaries.empty')</h2>
            @endif
        </div>
    </div>
@stop