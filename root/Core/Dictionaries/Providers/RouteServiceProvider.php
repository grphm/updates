<?php

namespace STALKER_CMS\Core\Dictionaries\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

/**
 * Class RouteServiceProvider
 * @package STALKER_CMS\Core\Dictionaries\Providers
 */
class RouteServiceProvider extends ServiceProvider {

    /**
     * @var string
     */
    protected $namespace = '\STALKER_CMS\Core\Dictionaries\Http\Controllers';

    /**
     * @param Router $router
     */
    public function boot(Router $router) {

        parent::boot($router);
    }

    /**
     * @param Router $router
     */
    public function map(Router $router) {

        $router->group(['namespace' => $this->namespace], function ($router) {
            require __DIR__ . '/../Http/routes.php';
        });
    }
}
