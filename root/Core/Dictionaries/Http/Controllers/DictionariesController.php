<?php
namespace STALKER_CMS\Core\Dictionaries\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use STALKER_CMS\Core\Dictionaries\Models\Dictionary;
use STALKER_CMS\Vendor\Interfaces\CrudInterface;
use STALKER_CMS\Vendor\Helpers as Helpers;

/**
 * Контроллер словаря
 * Class DictionariesController
 * @package STALKER_CMS\Core\Dictionaries\Http\Controllers
 */
class DictionariesController extends ModuleController implements CrudInterface {

    /**
     * @var Dictionary
     */
    protected $model;

    /**
     * @var mixed
     */
    private $menu;

    /**
     * DictionariesController constructor.
     * @param Dictionary $dictionary
     */
    public function __construct(Dictionary $dictionary) {

        $this->model = $dictionary;
        $this->middleware('auth');
        $menu_child = config('core_dictionaries::menu.menu_child');
        $this->menu = (new $menu_child)->make(\App::getLocale());
        \PermissionsController::allowPermission('core_dictionaries', 'constructor');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {

        return view('core_dictionaries_views::dictionaries.index', [
            'dictionaries' => $this->model->whereLocale(\App::getLocale())->orderBy('updated_at', 'DESC')->with('lists')->get(),
            'dictionaries_icon' => $this->menu['dictionaries']['icon'],
            'dictionaries_title' => Helpers\array_translate($this->menu['dictionaries']['title'])
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {

        $categories = [];
        foreach (config('core_dictionaries::config.form_elements') as $category_type => $category):
            $categories[$category_type] = Helpers\array_translate($category);
        endforeach;
        return view('core_dictionaries_views::dictionaries.create', [
            'categories' => $categories,
            'information_items' => $this->getInformationItems(),
            'dictionaries_icon' => $this->menu['dictionaries']['icon'],
            'dictionaries_title' => Helpers\array_translate($this->menu['dictionaries']['title'])
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function store() {

        $request = \RequestController::isAJAX()->init();
        if (\ValidatorController::passes($request, $this->model->getStoreRules())):
            $dictionary = $this->model->insert($request);
            return \ResponseController::success(201)->redirect(route('core.dictionaries.lists_index', $dictionary->id))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id) {

        $categories = [];
        foreach (config('core_dictionaries::config.form_elements') as $category_type => $category):
            $categories[$category_type] = Helpers\array_translate($category);
        endforeach;
        return view('core_dictionaries_views::dictionaries.edit', [
            'categories' => $categories,
            'information_items' => $this->getInformationItems(),
            'dictionary' => $this->model->findOrFail($id),
            'dictionaries_icon' => $this->menu['dictionaries']['icon'],
            'dictionaries_title' => Helpers\array_translate($this->menu['dictionaries']['title'])
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id) {

        $request = \RequestController::isAJAX()->init();
        if (\ValidatorController::passes($request, $this->model->getUpdateRules())):
            $this->model->replace($id, $request);
            return \ResponseController::success(202)->redirect(route('core.dictionaries.index'))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id) {

        $request = \RequestController::isAJAX()->init();
        $this->model->remove($id);
        return \ResponseController::success(1203)->redirect(route('core.dictionaries.index'))->json();
    }
}