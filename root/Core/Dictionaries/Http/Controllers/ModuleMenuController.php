<?php
namespace STALKER_CMS\Core\Dictionaries\Http\Controllers;

use STALKER_CMS\Core\Dictionaries\Models\Dictionary;
use STALKER_CMS\Vendor\Http\Controllers\Controller;

/**
 * Контроллер меню пакета
 * Class ModuleController
 * @package STALKER_CMS\Core\Dictionaries\Http\Controllers
 */
class ModuleMenuController extends Controller {

    public function make($locale){

        $menu_child['dictionaries'] = [
            'title' => ['ru' => 'Словари', 'en' => 'Dictionaries', 'es' => 'Diccionarios'],
            'route' => 'core.dictionaries.index',
            'icon' => 'zmdi zmdi-check-circle-u'
        ];
        foreach (Dictionary::whereLocale($locale)->get() as $dictionary):
            $menu_child[$dictionary->slug] = [
                'title' => ['ru' => $dictionary->title, 'en' => $dictionary->title, 'es' => $dictionary->title],
                'route' => 'core.dictionaries.lists_index',
                'route_params' => [$dictionary->id],
                'icon' => 'zmdi zmdi-chevron-right',
                'permit' => TRUE
            ];
        endforeach;
        return $menu_child;
    }
}