<?php

\Route::group(['prefix' => 'admin', 'middleware' => 'secure'], function () {

    \Route::resource('dictionaries', 'DictionariesController',
        [
            'except' => ['show'],
            'names' => [
                'index' => 'core.dictionaries.index',
                'create' => 'core.dictionaries.create',
                'store' => 'core.dictionaries.store',
                'edit' => 'core.dictionaries.edit',
                'update' => 'core.dictionaries.update',
                'destroy' => 'core.dictionaries.destroy'
            ]
        ]
    );
    \Route::resource('dictionaries.lists', 'DictionaryListsController',
        [
            'except' => ['show'],
            'names' => [
                'index' => 'core.dictionaries.lists_index',
                'create' => 'core.dictionaries.lists_create',
                'store' => 'core.dictionaries.lists_store',
                'edit' => 'core.dictionaries.lists_edit',
                'update' => 'core.dictionaries.lists_update',
                'destroy' => 'core.dictionaries.lists_destroy'
            ]
        ]
    );
    \Route::post('dictionaries/{dictionary_id}/lists/sortable', ['as' => 'core.dictionaries.lists_sortable', 'uses' => 'DictionaryListsController@sortable']);
});
