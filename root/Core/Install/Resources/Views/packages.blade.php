@extends('root_views::layouts.install')
@section('title', trans('core_install_lang::packages.page_title'))
@section('description', trans('core_install_lang::index.page_description'))
@section('body-class'){{ 'login-content o-auto text-left' }}@stop
@section('content')
    <div class="lc-block toggled p-0 m-0">
        <div class="listview lv-bordered lv-lg">
            <div class="lv-header-alt clearfix">
                <h2 class="lvh-label hidden-xs">@lang('core_install_lang::packages.form.title')</h2>
                <ul class="lv-actions actions">
                    <li class="dropdown">
                        <a aria-expanded="false" data-toggle="dropdown" href="">
                            <i class="zmdi zmdi-more-vert"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li>
                                <a href="javascript:void(0);" class="js-check-all">
                                    <i class="zmdi zmdi-check-all zmdi-hc-fw"></i> @lang('core_install_lang::packages.form.check_all')
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="lv-body">
                {!! Form::open(['route' => 'install.config.packages', 'class' => 'form-validate', 'id' => 'config-packages-form']) !!}
                @foreach($packages as $package)
                    <div class="lv-item media">
                        <div class="checkbox pull-left">
                            <label>
                                {!! Form::checkbox('MODULE[' . $package->slug . ']', TRUE, FALSE, ['autocomplete' => 'off']) !!}
                                <i class="input-helper"></i>
                            </label>
                        </div>
                        <div class="media-body text-left">
                            <div class="lv-title">{!! $package->title !!}</div>
                            @if(!empty($package->description))
                                <small class="lv-small">{{ $package->description }}</small>
                            @endif
                            @if(!empty($package->relations))
                                <small class="lv-small">
                                    Для установки
                                    @choice('требуется модуль:|требуются модули:', count(explode('|', $package->relations)))
                                    <ul class="lv-attrs">
                                        @foreach(explode('|', $package->relations) as $relation_package)
                                            <li>{{ $packages[$relation_package]['title'] or '' }}</li>
                                        @endforeach
                                    </ul>
                                </small>
                            @endif
                        </div>
                    </div>
                @endforeach
                {!! Form::button('<i class="zmdi zmdi-arrow-forward"></i>', ['type' => 'submit', 'class' => 'btn btn-login btn-danger btn-float', 'autocomplete' => 'off']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop
@section('scripts_before')
@stop
@section('scripts_after')
    <script>
        $(".js-check-all").click(function () {
            $("#config-packages-form input[type='checkbox']").prop('checked', true);
            $(this).parents('li.dropdown').removeClass('open');
        });
    </script>
@stop