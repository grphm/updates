@extends('root_views::layouts.install')
@section('title', trans('core_install_lang::index.page_title'))
@section('description', trans('core_install_lang::index.page_description'))
@section('body-class'){{ 'login-content' }}@stop
@section('content')
    {!! Form::open(['route' => 'install.config.db', 'class' => 'form-validate', 'id' => 'config-db-form']) !!}
    <div class="lc-block toggled p-0">
        <div class="listview lv-bordered lv-lg">
            <div class="lv-header-alt clearfix">
                <h2 class="lvh-label hidden-xs">@lang('core_install_lang::index.form.title')</h2>
            </div>
            <div class="clearfix"></div>
            <div class="p-t-10 p-r-30 p-l-30">
                <div class="form-group">
                    <p class="f-13 m-0 m-t-10 c-gray pull-left"><i class="zmdi zmdi-translate"></i> @lang('core_install_lang::index.form.APP_LOCALE')</p>
                    {!! Form::select('APP_LOCALE', config('core_install::config.languages'), \App::getLocale(), ['class' => 'selectpicker', 'autocomplete' => 'off']) !!}
                </div>
                <div class="form-group fg-float">
                    <div class="fg-line">
                        <input type="text" class="form-control" name="DB_DATABASE">
                    </div>
                    <label class="fg-label">
                        <i class="zmdi zmdi-dns"></i> @lang('core_install_lang::index.form.DB_DATABASE')
                    </label>
                </div>
                <div class="form-group fg-float">
                    <div class="fg-line">
                        <input type="text" class="form-control" name="DB_USERNAME">
                    </div>
                    <label class="fg-label">
                        <i class="zmdi zmdi-account"></i> @lang('core_install_lang::index.form.DB_USERNAME')
                    </label>
                </div>
                <div class="form-group fg-float">
                    <div class="fg-line">
                        <input type="password" class="form-control" name="DB_PASSWORD">
                    </div>
                    <label class="fg-label">
                        <i class="zmdi zmdi-key"></i> @lang('core_install_lang::index.form.DB_PASSWORD')
                    </label>
                </div>
                <div class="clearfix"></div>
            </div>
            {!! Form::button('<i class="zmdi zmdi-arrow-forward"></i>', ['type' => 'submit', 'class' => 'btn btn-login btn-danger btn-float', 'autocomplete' => 'off']) !!}
        </div>
    </div>
    {!! Form::close() !!}
@stop
@section('scripts_before')
@stop
@section('scripts_after')
@stop