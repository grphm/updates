<?php

return [
    'page_title' => 'Configuring the connection to the Database',
    'page_description' => '',
    'form' => [
        'title' => 'Step 1. Set up a connection to the Database',
        'APP_LOCALE' => 'Interface language',
        'DB_DATABASE' => 'Database',
        'DB_USERNAME' => 'Username',
        'DB_PASSWORD' => 'Password',
    ]
];
