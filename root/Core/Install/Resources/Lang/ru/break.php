<?php

return [
    'page_title' => 'Установка невозможна',
    'page_description' => '',
    'title' => '<i class="zmdi zmdi-alert-triangle"></i>',
    'message' => 'Обнаружен файл конфигурации<br>Удалите его и начните снова',
];
