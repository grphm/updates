<?php

namespace STALKER_CMS\Core\Install\Providers;

use Illuminate\Support\ServiceProvider;
use STALKER_CMS\Core\Install\Http\Controllers\InstallController;
use STALKER_CMS\Core\Install\Http\Controllers\ModuleController;
use STALKER_CMS\Core\Install\Models\Packages;

/**
 * Class ModuleServiceProvider
 * @package STALKER_CMS\Core\Install\Providers
 */
class ModuleServiceProvider extends ServiceProvider {

    /**
     *
     */
    public function boot() {

        $this->registerViews();
        $this->registerLocalization();
        $this->registerConfig();
    }

    /**
     *
     */
    public function register() {

        \App::bind('InstallController', function () {
            return new InstallController(new Packages());
        });
    }

    /********************************************************************************************************************/
    /**
     * @param $path
     * @return string
     */
    private function getPath($path) {

        return realpath(__DIR__ . '/../' . $path);
    }

    /********************************************************************************************************************/
    /**
     *
     */
    private function registerViews() {

        $this->loadViewsFrom($this->getPath('Resources/Views'), 'core_install_views');
    }

    /**
     *
     */
    private function registerLocalization() {

        $this->loadTranslationsFrom($this->getPath('Resources/Lang'), 'core_install_lang');
    }

    /**
     *
     */
    private function registerConfig() {

        $packageConfigFile = __DIR__ . '/../Config/install.php';
        $config = $this->app['files']->getRequire($packageConfigFile);
        $this->app['config']->set('core_install::config', $config);
    }

}
