<?php
namespace STALKER_CMS\Core\Install\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Facade;

class LanguagesTableSeeder extends Seeder {

    public function run() {

        \DB::table('languages')->insert([
            'slug' => 'ru', 'title' => 'Русский',
            'iso_639_1' => 'ru', 'iso_639_2' => 'rus', 'iso_639_3' => 'rus', 'code' => '570',
            'active' => TRUE, 'default' => TRUE, 'required' => TRUE
        ]);
        \DB::table('languages')->insert([
            'slug' => 'en', 'title' => 'English',
            'iso_639_1' => 'en', 'iso_639_2' => 'eng', 'iso_639_3' => 'eng', 'code' => '045',
            'active' => TRUE, 'default' => FALSE, 'required' => TRUE
        ]);

        \DB::table('languages')->insert([
            'slug' => 'es', 'title' => 'Español',
            'iso_639_1' => 'es', 'iso_639_2' => 'esl/spa', 'iso_639_3' => 'spa', 'code' => '230',
            'active' => TRUE, 'default' => FALSE, 'required' => TRUE
        ]);
    }
}