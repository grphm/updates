<?php

return [
    'throttle' => 'Demasiados intentos de conexión. Vuelve a intentarlo en :seconds segundos',
    'welcome' => 'Bienvenida',
];