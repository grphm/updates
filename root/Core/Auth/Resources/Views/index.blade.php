@extends('root_views::layouts.auth')
@section('title', trans('core_auth_lang::index.page_title'))
@section('description', trans('core_auth_lang::index.page_description'))
@section('body-class'){{ 'login-content' }}@stop
@section('content')
    {!! Form::open(['route' => 'auth.login.authenticate', 'class' => 'form-validate', 'id' => 'auth-form']) !!}
    {!! Form::hidden('redirect', session('auth.redirect')) !!}
    {!! Form::hidden('remember', TRUE) !!}
    <div class="lc-block toggled p-0">
        <div class="listview lv-bordered lv-lg">
            <div class="lv-header-alt clearfix">
                <h2 class="lvh-label hidden-xs">
                    <i class="zmdi zmdi-sign-in"></i> @lang('core_auth_lang::index.form.title')
                </h2>
            </div>
            <div class="clearfix"></div>
            <div class="p-30">
                <div class="form-group fg-float">
                    <div class="fg-line">
                        {!! Form::email('login', NULL, ['class' => 'form-control']) !!}
                    </div>
                    <label class="fg-label">
                        <i class="zmdi zmdi-email"></i> @lang('core_auth_lang::index.form.login_field')
                    </label>
                </div>
                <div class="form-group fg-float">
                    <div class="fg-line">
                        {!! Form::password('password', ['class' => 'form-control']) !!}
                    </div>
                    <label class="fg-label">
                        <i class="zmdi zmdi-key"></i> @lang('core_auth_lang::index.form.password')
                    </label>
                </div>
                <div class="clearfix"></div>
            </div>
            {!! Form::button('<i class="zmdi zmdi-arrow-forward"></i>', ['type' => 'submit', 'class' => 'btn btn-login btn-danger btn-float', 'autocomplete' => 'off']) !!}
        </div>
    </div>
    {!! Form::close() !!}
@stop
@section('scripts_before')
@stop
@section('scripts_after')