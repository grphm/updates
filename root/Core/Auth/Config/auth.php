<?php

return [
    'package_name' => 'core_auth',
    'package_title' => [
        'ru' => 'Авторизация и регистрация',
        'en' => 'Authorization and registration',
        'es' => 'Registro Autorización Andes'
    ],
    'package_icon' => 'zmdi zmdi-sign-in',
    'version' => [
        'ver' => 1.0,
        'date' => '15.04.2016'
    ]
];
