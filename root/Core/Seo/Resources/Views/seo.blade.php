<div id="seo-container">
    <div class="c-gray m-b-20">
        <i class="{{ config('core_seo::config.package_icon') }}"></i>
        @lang('core_seo_lang::seo.seo')
        <div class="pull-right">
            <button type="button" class="btn btn-primary btn-xs btn-icon-text waves-effect auto-paste">
                <i class="fa fa-paste"></i> @lang('core_seo_lang::seo.paste')
            </button>
        </div>
    </div>
    <div class="form-group fg-float">
        <div class="fg-line">
            {!! Form::text('seo_title', isset($seo->seo_title) ? $seo->seo_title : NULL, ['class'=>'input-sm form-control fg-input']) !!}
        </div>
        <label class="fg-label">@lang('core_seo_lang::seo.seo_title')</label>
    </div>
    <div class="form-group fg-float">
        <div class="fg-line">
            {!! Form::textarea('seo_description', isset($seo->seo_description) ? $seo->seo_description : NULL, ['class' => 'form-control auto-size fg-input', 'data-autosize-on' => 'true', 'rows' => 1]) !!}
        </div>
        <label class="fg-label">@lang('core_seo_lang::seo.seo_description')</label>
    </div>
    <div class="form-group fg-float">
        <div class="fg-line">
            {!! Form::text('seo_h1', isset($seo->seo_h1) ? $seo->seo_h1 : NULL, ['class'=>'input-sm form-control fg-input']) !!}
        </div>
        <label class="fg-label">@lang('core_seo_lang::seo.seo_h1')</label>
    </div>
    <div class="form-group fg-float">
        <div class="fg-line">
            {!! Form::text('seo_url', isset($seo->seo_url) ? $seo->seo_url : NULL, ['class'=>'input-sm form-control fg-input']) !!}
        </div>
        <label class="fg-label">@lang('core_seo_lang::seo.seo_url')</label>
        <small class="help-description">@lang('core_seo_lang::seo.seo_url_help_description')</small>
    </div>
</div>