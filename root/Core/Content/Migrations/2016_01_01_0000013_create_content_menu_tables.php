<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContentMenuTables extends Migration {

    public function up() {

        Schema::create('content_menu', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug', 50)->nullable()->index();
            $table->string('locale', 10)->nullable()->index();
            $table->integer('template_id', FALSE, TRUE)->nullable();
            $table->string('title', 100)->nullable();
            $table->integer('user_id', FALSE, TRUE)->nullable()->index();
            $table->timestamps();
        });
    }

    public function down() {

        Schema::dropIfExists('content_menu');
    }
}

