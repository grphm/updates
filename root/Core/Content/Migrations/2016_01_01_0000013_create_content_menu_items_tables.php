<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContentMenuItemsTables extends Migration {

    public function up() {

        Schema::create('content_menu_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('menu_id', FALSE, TRUE)->nullable()->index();
            $table->integer('parent_id', FALSE, TRUE)->nullable()->index();
            $table->string('item_type', 50)->nullable();
            $table->string('title', 100)->nullable();
            $table->integer('page_id', FALSE, TRUE)->nullable()->index();
            $table->integer('file_id', FALSE, TRUE)->nullable();
            $table->string('link', 255)->nullable();
            $table->string('title_link', 100)->nullable();
            $table->string('anchor', 100)->nullable();
            $table->string('css', 100)->nullable();
            $table->string('parameters', 200)->nullable();
            $table->boolean('blank')->nullable();
            $table->boolean('hide')->nullable();
            $table->integer('order', FALSE, TRUE)->default(0)->nullable()->index();
            $table->timestamps();
        });
    }

    public function down() {

        Schema::dropIfExists('content_menu_items');
    }
}

