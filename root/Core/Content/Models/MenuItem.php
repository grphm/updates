<?php
namespace STALKER_CMS\Core\Content\Models;

use Carbon\Carbon;
use League\Flysystem\Exception;
use STALKER_CMS\Vendor\Interfaces\ModelInterface;
use STALKER_CMS\Vendor\Models\BaseModel;
use STALKER_CMS\Vendor\Traits\ModelTrait;

/**
 * Модель элемент меню
 * Class MenuItem
 * @package STALKER_CMS\Core\Content\Models
 */
class MenuItem extends BaseModel implements ModelInterface {

    use ModelTrait;

    /**
     * @var string
     */
    protected $table = 'content_menu_items';
    /**
     * @var array
     */
    protected $fillable = ['menu_id', 'item_type', 'parent_id', 'title', 'order'];
    /**
     * @var array
     */
    protected $hidden = [];
    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @param $request
     * @return $this
     */
    public function insert($request) {

        $this->menu_id = $request::input('menu_id');
        $this->parent_id = $request::input('parent_id');
        $this->item_type = $request::input('item_type');
        $this->title = $request::input('title');
        if ($request::input('item_type') == 'page_link'):
            $this->page_id = $request::input('page_link_page_id');
            $this->file_id = NULL;
            $this->link = NULL;
            $this->anchor = NULL;
        elseif ($request::input('item_type') == 'external_link'):
            $this->page_id = NULL;
            $this->file_id = NULL;
            $this->link = $request::input('external_link');
            $this->anchor = NULL;
        elseif ($request::input('item_type') == 'anchor_link'):
            $this->page_id = $request::input('anchor_link_page_id');
            $this->file_id = NULL;
            $this->link = NULL;
            $this->anchor = $request::input('anchor');
        elseif ($request::input('item_type') == 'file_link'):
            $this->page_id = NULL;
            $this->file_id = $request::input('file_id');
            $this->link = NULL;
            $this->anchor = NULL;
        endif;
        $this->title_link = $request::input('title_link');
        $this->css = $request::input('class_link');
        $this->parameters = $request::input('get_link');
        $this->blank = $request::has('blank') ? TRUE : FALSE;
        $this->hide = $request::has('hide') ? TRUE : FALSE;
        $this->order = $this::whereMenuId($request::input('menu_id'))->max('order') + 1;
        $this->save();
        return $this;
    }

    /**
     * @param $id
     * @param $request
     * @return mixed
     */
    public function replace($id, $request) {

        $model = $this::whereFind(['id' => $id]);
        $model->parent_id = $request::input('parent_id');
        $model->item_type = $request::input('item_type');
        $model->title = $request::input('title');
        if ($request::input('item_type') == 'page_link'):
            $model->page_id = $request::input('page_link_page_id');
            $model->file_id = NULL;
            $model->link = NULL;
            $model->anchor = NULL;
        elseif ($request::input('item_type') == 'external_link'):
            $model->page_id = NULL;
            $model->file_id = NULL;
            $model->link = $request::input('external_link');
            $model->anchor = NULL;
        elseif ($request::input('item_type') == 'anchor_link'):
            $model->page_id = $request::input('anchor_link_page_id');
            $model->file_id = NULL;
            $model->link = NULL;
            $model->anchor = $request::input('anchor');
        elseif ($request::input('item_type') == 'file_link'):
            $model->page_id = NULL;
            $model->file_id = $request::input('file_id');
            $model->link = NULL;
            $model->anchor = NULL;
        endif;
        $model->title_link = $request::input('title_link');
        $model->css = $request::input('class_link');
        $model->parameters = $request::input('get_link');
        $model->blank = $request::has('blank') ? TRUE : FALSE;
        $model->hide = $request::has('hide') ? TRUE : FALSE;
        $model->save();
        return $model;
    }

    /**
     * @param array|int $id
     * @return mixed
     */
    public function remove($id) {

        $instance = static::findOrFail($id);
        return $instance->delete();
    }

    /**
     * @param array $attributes
     */
    public function search(array $attributes) {
        // TODO: Implement search() method.
    }

    /**
     * @param array $attributes
     */
    public function filter(array $attributes) {
        // TODO: Implement filter() method.
    }

    /**
     * @param array $attributes
     */
    public function sort(array $attributes) {
        // TODO: Implement sort() method.
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function page() {

        return $this->hasOne('\STALKER_CMS\Core\Content\Models\Page', 'id', 'page_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function file() {

        return $this->hasOne('\STALKER_CMS\Core\Uploads\Models\Upload', 'id', 'file_id');
    }

    /***************************************************************************************************************/
    /**
     * @return array
     */
    public static function getStoreRules() {

        return ['item_type' => 'required', 'title' => 'required'];
    }

    /**
     * @return array
     */
    public static function getUpdateRules() {

        return ['item_type' => 'required', 'title' => 'required'];
    }
}