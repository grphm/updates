@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('head')
@stop
@section('breadcrumb')
    <ol class="breadcrumb" style="margin-bottom: 5px;">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('core_content::menu.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_content::menu.title')) !!}
        </li>
        <li class="active">
            <i class="{{ config('core_content::menu.menu_child.menu.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_content::menu.menu_child.menu.title')) !!}
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="{{ config('core_content::menu.menu_child.menu.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_content::menu.menu_child.menu.title')) !!}
        </h2>
    </div>
    @BtnAdd('core.content.menu.create')
    <div class="card">
        <div class="card-body card-padding m-h-250">
            @if($menus->count())
                <div class="listview lv-bordered lv-lg">
                    <div class="lv-body">
                        @foreach($menus as $index => $menu)
                            <div class="js-item-container lv-item media">
                                <div class="media-body">
                                    <div class="lv-title">{{ $menu->title }}</div>
                                    @if($menu->template)
                                        <small class="lv-small">{{ \STALKER_CMS\Vendor\Helpers\double_slash('/home/Resources/Views/' . \STALKER_CMS\Vendor\Helpers\settings(['core_content', 'pages', 'templates_dir']). '/' . $menu->template->path) }}</small>
                                    @endif
                                    <ul class="lv-attrs">
                                        <li>ID: @numDimensions($menu->id)</li>
                                        <li>
                                            @lang('core_content_lang::menu.symbolic_code'):
                                            {{ $menu->slug }}
                                        </li>
                                    </ul>
                                    <div class="lv-actions actions dropdown">
                                        <a aria-expanded="true" data-toggle="dropdown" href="">
                                            <i class="zmdi zmdi-more-vert"></i>
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            @if(\PermissionsController::allowPermission('core_content', 'edit', FALSE))
                                                <li>
                                                    <a href="{{ route('core.content.menu.items_index', $menu->id) }}">
                                                        @lang('core_content_lang::menu.items')
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{ route('core.content.menu.edit', $menu->id) }}">
                                                        @lang('core_content_lang::menu.edit')
                                                    </a>
                                                </li>
                                            @endif
                                            <li>
                                                <a href="javascript:void(0);" class="js-copy-link"
                                                   data-clipboard-text="{{ '@'.'Menu(\'' . $menu->slug . '\')' }}">
                                                    @lang('core_content_lang::menu.embed')
                                                </a>
                                            </li>
                                            @if(\PermissionsController::allowPermission('core_content', 'delete', FALSE))
                                                <li class="divider"></li>
                                                <li>
                                                    {!! Form::open(['route' => ['core.content.menu.destroy', $menu->id], 'method' => 'DELETE']) !!}
                                                    <button type="submit"
                                                            class="form-confirm-warning btn-link pull-right c-red p-r-15"
                                                            autocomplete="off"
                                                            data-question="@lang('core_content_lang::menu.delete.question') &laquo;{{ $menu->title }}&raquo;?"
                                                            data-confirmbuttontext="@lang('core_content_lang::menu.delete.confirmbuttontext')"
                                                            data-cancelbuttontext="@lang('core_content_lang::menu.delete.cancelbuttontext')">
                                                        @lang('core_content_lang::menu.delete.submit')
                                                    </button>
                                                    {!! Form::close() !!}
                                                </li>
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @else
                <h2 class="f-16 c-gray">@lang('core_content_lang::menu.empty')</h2>
            @endif
        </div>
    </div>
@stop