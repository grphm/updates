@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('head')
@stop
@section('breadcrumb')
    <ol class="breadcrumb" style="margin-bottom: 5px;">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('core_content::menu.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_content::menu.title')) !!}
        </li>
        <li>
            <a href="{{ route('core.content.pages.index') }}">
                <i class="{{ config('core_content::menu.menu_child.menu.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_content::menu.menu_child.menu.title')) !!}
            </a>
        </li>
        <li>
            <a href="{{ route('core.content.menu.items_index', $menu->id) }}">
                <i class="zmdi zmdi-receipt"></i> @lang('core_content_lang::menu_items.breadcrumb')
            </a>
        </li>
        <li class="active">
            <i class="zmdi zmdi-edit"></i> @lang('core_content_lang::menu.replace.breadcrumb')
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="zmdi zmdi-edit"></i> @lang('core_content_lang::menu.replace.title')
        </h2>
    </div>
    <div class="card">
        <div class="card-body card-padding">
            <div class="row">
                {!! Form::model($item, ['route' => ['core.content.menu.items_update', $menu->id, $item->id], 'class' => 'form-validate', 'id' => 'edit-content-menu-item-form', 'method' => 'PUT']) !!}
                <div class="col-sm-9">
                    @set($items, \PublicMenu::listItems($menu->id, $item->id))
                    <div class="form-group">
                        <p class="c-gray m-b-10">@lang('core_content_lang::menu_items.replace.form.parent')</p>
                        {!! Form::select('parent_id', $items, NULL, ['class' => 'tag-select', 'autocomplete' => 'off']) !!}
                    </div>
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('title', NULL, ['class'=>'input-sm form-control fg-input']) !!}
                        </div>
                        <label class="fg-label">@lang('core_content_lang::menu_items.replace.form.title')</label>
                    </div>
                    @set($pages, \PublicPage::getPagesList()->lists('title', 'id'))
                    <div id="page_link" class="item-type-block">
                        <div class="form-group">
                            <p class="c-gray m-b-10">@lang('core_content_lang::menu_items.replace.form.pages')</p>
                            {!! Form::select('page_link_page_id', $pages, $item->page_id, ['class' => 'tag-select', 'autocomplete' => 'off']) !!}
                        </div>
                    </div>
                    <div id="external_link" class="item-type-block hidden">
                        <div class="form-group fg-float">
                            <div class="fg-line">
                                {!! Form::text('external_link', $item->link, ['class'=>'input-sm form-control fg-input']) !!}
                            </div>
                            <label class="fg-label">@lang('core_content_lang::menu_items.replace.form.external_link')</label>
                        </div>
                    </div>
                    <div id="anchor_link" class="item-type-block hidden">
                        <div class="form-group">
                            <p class="c-gray m-b-10">@lang('core_content_lang::menu_items.replace.form.pages')</p>
                            {!! Form::select('anchor_link_page_id', $pages, $item->page_id, ['class' => 'tag-select', 'autocomplete' => 'off']) !!}
                        </div>
                        <div class="form-group fg-float">
                            <div class="fg-line">
                                {!! Form::text('anchor', NULL, ['class'=>'input-sm form-control fg-input']) !!}
                            </div>
                            <label class="fg-label">@lang('core_content_lang::menu_items.replace.form.anchor')</label>
                        </div>
                    </div>
                    @if(\PermissionsController::isPackageEnabled('core_uploads'))
                        <div id="file_link" class="item-type-block hidden">
                            <div class="form-group">
                                @set($files, \STALKER_CMS\Core\Uploads\Models\Upload::lists('original_name', 'id'))
                                <p class="c-gray m-b-10">@lang('core_content_lang::menu_items.replace.form.files')</p>
                                {!! Form::select('file_id', $files, NULL, ['class' => 'tag-select', 'autocomplete' => 'off']) !!}
                            </div>
                        </div>
                    @endif
                    <p class="m-b-25 m-t-40 c-gray f-500">
                        @lang('core_content_lang::menu_items.insert.form.properties')
                    </p>

                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('title_link', NULL, ['class'=>'input-sm form-control fg-input']) !!}
                        </div>
                        <label class="fg-label">@lang('core_content_lang::menu_items.replace.form.title_link')</label>
                    </div>
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('class_link', $item->css, ['class'=>'input-sm form-control fg-input']) !!}
                        </div>
                        <label class="fg-label">@lang('core_content_lang::menu_items.replace.form.class_link')</label>
                    </div>
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('get_link', $item->parameters, ['class'=>'input-sm form-control fg-input']) !!}
                        </div>
                        <label class="fg-label">@lang('core_content_lang::menu_items.replace.form.get_link')</label>
                    </div>
                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                                {!! Form::checkbox('blank', TRUE, FALSE, ['autocomplete' => 'off']) !!}
                                <i class="input-helper"></i> @lang('core_content_lang::menu_items.replace.form.blank')
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                {!! Form::checkbox('hide', TRUE, FALSE, ['autocomplete' => 'off']) !!}
                                <i class="input-helper"></i> @lang('core_content_lang::menu_items.replace.form.hide')
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <p class="c-gray m-b-20">@lang('core_content_lang::menu_items.insert.form.link_type')</p>

                    <div class="radio m-b-15">
                        <label>
                            {!! Form::radio('item_type', 'page_link', TRUE, ['class' => 'choice-item-type', 'autocomplete' => 'off']) !!}
                            <i class="input-helper"></i> @lang('core_content_lang::menu_items.insert.form.page_link_type')
                        </label>
                    </div>
                    <div class="radio m-b-15">
                        <label>
                            {!! Form::radio('item_type', 'external_link', FALSE, ['class' => 'choice-item-type', 'autocomplete' => 'off']) !!}
                            <i class="input-helper"></i> @lang('core_content_lang::menu_items.insert.form.external_link')
                        </label>
                    </div>
                    <div class="radio m-b-15">
                        <label>
                            {!! Form::radio('item_type', 'anchor_link', FALSE, ['class' => 'choice-item-type', 'autocomplete' => 'off']) !!}
                            <i class="input-helper"></i> @lang('core_content_lang::menu_items.insert.form.anchor_link')
                        </label>
                    </div>
                    @if(\PermissionsController::isPackageEnabled('core_uploads'))
                        <div class="radio m-b-15">
                            <label>
                                {!! Form::radio('item_type', 'file_link', FALSE, ['class' => 'choice-item-type', 'autocomplete' => 'off']) !!}
                                <i class="input-helper"></i> @lang('core_content_lang::menu_items.insert.form.file_link')
                            </label>
                        </div>
                    @endif
                    <button type="submit" autocomplete="off" class="btn btn-primary btn-sm m-t-10 waves-effect">
                        <i class="fa fa-save"></i>
                        <span class="btn-text">@lang('core_content_lang::menu_items.insert.form.submit')</span>
                    </button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop
@section('scripts_after')
    <script>
        $(function () {
            $(".item-type-block").addClass('hidden');
            $("#{!! $item->item_type !!}").removeClass('hidden');

            $(".choice-item-type").change(function () {
                $(".item-type-block").addClass('hidden');
                $("#" + $(this).val()).removeClass('hidden');
            });
        });
    </script>
@stop