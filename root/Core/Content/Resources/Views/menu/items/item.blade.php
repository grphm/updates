<div class="pull-left">
    <i class="zmdi zmdi-swap-vertical f-20 p-t-10 cursor-move"></i>
</div>
<div class="media-body">
    <div class="lv-title">{{ $element['title'] }}</div>
    <small class="lv-small">{{ $types[$element['item_type']] or '' }}</small>
    <div class="lv-actions actions dropdown">
        <a aria-expanded="true" data-toggle="dropdown" href="">
            <i class="zmdi zmdi-more-vert"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            <li>
                <a href="{{ route('core.content.menu.items_edit', [$menu->id, $element['id']]) }}">
                    @lang('core_content_lang::menu_items.edit')
                </a>
            </li>
            <li class="divider"></li>
            <li>
                {!! Form::open(['route' => ['core.content.menu.items_destroy', $menu->id, $element['id']], 'method' => 'DELETE']) !!}
                <button type="submit"
                        class="form-confirm-warning btn-link pull-right c-red p-r-15"
                        autocomplete="off"
                        data-question="@lang('core_content_lang::menu_items.delete.question') &laquo;{{ $element['title'] }}&raquo;?"
                        data-confirmbuttontext="@lang('core_content_lang::menu_items.delete.confirmbuttontext')"
                        data-cancelbuttontext="@lang('core_content_lang::menu_items.delete.cancelbuttontext')">
                    @lang('core_content_lang::menu_items.delete.submit')
                </button>
                {!! Form::close() !!}
            </li>
        </ul>
    </div>
</div>