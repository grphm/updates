@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('head')
@stop
@section('breadcrumb')
    <ol class="breadcrumb" style="margin-bottom: 5px;">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('core_content::menu.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_content::menu.title')) !!}
        </li>
        <li>
            <a href="{{ route('core.content.pages.index') }}">
                <i class="{{ config('core_content::menu.menu_child.pages.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_content::menu.menu_child.pages.title')) !!}
            </a>
        </li>
        <li class="active">
            <i class="zmdi zmdi-edit"></i> @lang('core_content_lang::pages.replace.breadcrumb')
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="zmdi zmdi-edit"></i> @lang('core_content_lang::pages.replace.title')
        </h2>
    </div>
    <div class="card">
        <div class="card-body card-padding">
            <div class="row">
                {!! Form::model($page, ['route' => ['core.content.pages.update', $page->id], 'class' => 'form-validate', 'id' => 'edit-content-page-form', 'method' => 'PUT', 'files' => TRUE]) !!}
                <div class="col-sm-9">
                    @if(\PermissionsController::isPackageEnabled('core_seo') || \PermissionsController::isPackageEnabled('core_open_graph'))
                        @if(\PermissionsController::isPackageEnabled('core_seo') && \PermissionsController::allowPermission('core_content', 'seo', FALSE))
                            @include('core_seo_views::seo', ['seo' => $page])
                        @endif
                        @if(\PermissionsController::isPackageEnabled('core_open_graph') && \PermissionsController::allowPermission('core_content', 'open_graph', FALSE))
                            @include('core_open_graph_views::open_graph', ['open_graph' => $page])
                        @endif
                    @endif
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <p class="c-gray m-b-10">@lang('core_content_lang::pages.replace.form.template')</p>
                        {!! Form::select('template_id', $templates, NULL,['class' => 'selectpicker', 'autocomplete' => 'off']) !!}
                    </div>
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('title', NULL, ['class'=>'input-sm form-control fg-input']) !!}
                        </div>
                        <label class="fg-label">@lang('core_content_lang::pages.replace.form.title')</label>
                        <small class="help-description">@lang('core_content_lang::pages.replace.form.title_help_description')</small>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        {!! Form::checkbox('publication', TRUE, TRUE, ['autocomplete' => 'off']) !!}
                                        <i class="input-helper"></i> @lang('core_content_lang::pages.replace.form.publish')
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            @if($page->start_page || \STALKER_CMS\Core\Content\Models\Page::whereExist(['locale' => \App::getLocale(), 'start_page' => TRUE]) === FALSE)
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            {!! Form::checkbox('start_page', TRUE, TRUE, ['autocomplete' => 'off']) !!}
                                            <i class="input-helper"></i> @lang('core_content_lang::pages.replace.form.main_page')
                                        </label>
                                    </div>
                                </div>
                            @else
                                {!! Form::hidden('start_page', FALSE) !!}
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <button type="submit" autocomplete="off" class="btn btn-primary btn-sm m-t-10 waves-effect">
                                <i class="fa fa-save"></i>
                                <span class="btn-text">@lang('core_content_lang::pages.replace.form.submit')</span>
                            </button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop