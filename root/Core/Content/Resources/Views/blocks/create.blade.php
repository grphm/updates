@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('head')
@stop
@section('breadcrumb')
    <ol class="breadcrumb" style="margin-bottom: 5px;">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('core_content::menu.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_content::menu.title')) !!}
        </li>
        <li>
            <a href="{{ route('core.content.pages.index') }}">
                <i class="{{ config('core_content::menu.menu_child.pages.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_content::menu.menu_child.pages.title')) !!}
            </a>
        </li>
        <li>
            <a href="{{ route('core.content.pages.blocks_index', $page->id) }}">
                <i class="zmdi zmdi-view-subtitles"></i> @lang('core_content_lang::pages.blocks')
            </a>
        </li>
        <li class="active">
            <i class="zmdi zmdi-plus"></i> @lang('core_content_lang::blocks.insert.breadcrumb')
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="zmdi zmdi-plus"></i> @lang('core_content_lang::blocks.insert.title')
        </h2>
    </div>
    <div class="card">
        <div class="lv-header-alt clearfix">
            <h2 class="lvh-label hidden-xs">{{ $page->title }}</h2>
        </div>
        <div class="card-body card-padding">
            <div class="row">
                {!! Form::open(['route' => ['core.content.pages.blocks_store', $page->id], 'class' => 'form-validate', 'id' => 'add-content-block-form']) !!}
                {!! Form::hidden('page_id', $page->id) !!}
                <div class="col-sm-9">
                    <div class="form-group">
                        <p class="c-gray m-b-20">@lang('core_content_lang::blocks.insert.form.content')</p>
                        {!! Form::textarea('content', NULL, ['id' => 'block_content']) !!}
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('title', NULL, ['class'=>'input-sm form-control fg-input']) !!}
                        </div>
                        <label class="fg-label">@lang('core_content_lang::blocks.insert.form.title')</label>
                    </div>
                    @if(FALSE)
                        <div class="form-group">
                            <p class="c-gray m-b-10">@lang('core_content_lang::blocks.insert.form.template')</p>
                            {!! Form::select('template_id', $templates, NULL,['class' => 'selectpicker', 'autocomplete' => 'off']) !!}
                        </div>
                    @endif
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('slug', NULL, ['class'=>'input-sm form-control fg-input']) !!}
                        </div>
                        <label class="fg-label">@lang('core_content_lang::blocks.insert.form.slug')</label>
                        <small class="help-description">@lang('core_content_lang::blocks.insert.form.slug_help_description')</small>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <button type="submit" autocomplete="off" class="btn btn-primary btn-sm m-t-10 waves-effect">
                                <i class="fa fa-save"></i>
                                <span class="btn-text">@lang('core_content_lang::blocks.insert.form.submit')</span>
                            </button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop
@section('scripts_before')
@stop
@section('scripts_after')
    @set($summernote_locale, \App::getLocale() . '-' . strtoupper(\App::getLocale()))
    {!! Html::script('core/js/summernote/summernote-' . $summernote_locale . '.js') !!}
    <script>
        $("#block_content").summernote({
            height: 250,
            tabsize: 2,
            lang: '{{ $summernote_locale }}',
            toolbar: [
                ['font', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link', 'picture']],
                ['view', ['fullscreen', 'codeview']],
            ]
        });
        $('#block_content').on('summernote.change', function (we, contents, $editable) {
            $("#block_content").html(contents);
            $("#block_content").change();
        });
    </script>
@stop