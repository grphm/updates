<?php

return [
    'cache_clear' => 'Limpiar cache',
    'search' => 'Entre en la página código simbólico título o',
    'sort_title' => 'Título',
    'sort_published' => 'Fecha de publicación',
    'sort_updated' => 'Fecha de actualización',
    'edit' => 'Editar',
    'blocks' => 'Bloques de contenido',
    'blank' => 'Abrir en nueva ventana',
    'embed' => 'Código de inserción',
    'delete' => [
        'question' => 'Eliminar página',
        'confirmbuttontext' => 'Sí, eliminar',
        'cancelbuttontext' => 'He cambiado de idea',
        'submit' => 'Eliminar',
    ],
    'not_published' => 'No publicado',
    'published' => 'Publicado',
    'update' => 'Fecha de actualización',
    'slug' => 'Simbólico código',
    'empty' => 'Lista está vacía',
    'blocks_list_empty' => 'Lista de bloques vacíos',
    'blocks_list' => 'Lista de vacíos',
    'insert' => [
        'breadcrumb' => 'Añadir',
        'title' => 'Adición de la página',
        'form' => [
            'template' => 'Plantilla de página',
            'slug' => 'Simbólico código',
            'slug_help_description' => 'Sólo latino personajes, guiones, guiones, al menos 5 caracteres. <br> Ejemplo: index',
            'title' => 'Título',
            'title_help_description' => 'Por ejemplo: La página principal',
            'publish' => 'Publisch',
            'main_page' => 'Principal pag',
            'submit' => 'Guardar'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Editar',
        'title' => 'Editar de la página',
        'form' => [
            'template' => 'Plantilla de página',
            'title' => 'Título',
            'title_help_description' => 'Por ejemplo: La página principal',
            'publish' => 'Publisch',
            'main_page' => 'Principal pag',
            'submit' => 'Guardar'
        ]
    ]
];