<?php

return [
    'embed' => 'Código de inserción',
    'edit' => 'Editar',
    'delete' => [
        'question' => 'Eliminar bloque',
        'confirmbuttontext' => 'Sí, eliminar',
        'cancelbuttontext' => 'He cambiado de idea',
        'submit' => 'Eliminar',
    ],
    'slug' => 'Simbólico código',
    'empty' => 'Lista está vacía',
    'insert' => [
        'breadcrumb' => 'Añadir',
        'title' => 'Adición de la bloque',
        'form' => [
            'title' => 'Título',
            'template' => 'Plantilla de bloque',
            'content' => 'Bloque de contenido',
            'slug' => 'Simbólico código',
            'slug_help_description' => 'Sólo latino personajes, guiones, guiones, al menos 5 caracteres. <br> Ejemplo: index',
            'submit' => 'Guardar'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Editar',
        'title' => 'Editar de la bloque',
        'form' => [
            'title' => 'Título',
            'template' => 'Plantilla de bloque',
            'content' => 'Bloque de contenido',
            'submit' => 'Guardar'
        ]
    ]
];