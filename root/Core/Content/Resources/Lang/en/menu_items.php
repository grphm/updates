<?php

return [
    'breadcrumb' => 'Menu items',
    'top_item' => 'Top level',
    'edit' => 'Edit',
    'delete' => [
        'question' => 'Delete menu item',
        'confirmbuttontext' => 'Yes, delete',
        'cancelbuttontext' => 'No, I change my mind',
        'submit' => 'Delete'
    ],
    'empty' => 'List is empty',
    'insert' => [
        'breadcrumb' => 'Add',
        'title' => 'Adding a menu item',
        'form' => [
            'parent' => 'The parent menu item',
            'title' => 'Text the menu item',
            'pages' => 'Page',
            'files' => 'Uploaded files',
            'blank' => 'Open in new window',
            'anchor' => 'Anchor',
            'properties' => 'Advanced Properties',
            'title_link' => 'The title attribute',
            'class_link' => 'CSS-classes for links',
            'get_link' => 'GET-parameters',
            'hide' => 'Hide',
            'link_type' => 'Type menu item',
            'page_link_type' => 'Link to the page',
            'external_link' => 'External link',
            'anchor_link' => 'Link to "anchor"',
            'file_link' => 'Link to file',
            'submit' => 'Save'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Edit',
        'title' => 'Edit menu item',
        'form' => [
            'parent' => 'The parent menu item',
            'title' => 'Text the menu item',
            'pages' => 'Page',
            'files' => 'Uploaded files',
            'blank' => 'Open in new window',
            'anchor' => 'Anchor',
            'properties' => 'Advanced Properties',
            'title_link' => 'The title attribute',
            'class_link' => 'CSS-classes for links',
            'get_link' => 'GET-parameters',
            'hide' => 'Hide',
            'link_type' => 'Type menu item',
            'page_link_type' => 'Link to the page',
            'external_link' => 'External link',
            'anchor_link' => 'Link to "anchor"',
            'file_link' => 'Link to file',
            'submit' => 'Save'
        ]
    ]
];