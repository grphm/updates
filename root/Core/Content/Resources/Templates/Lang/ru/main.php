<?php

return [
    'index' => [
        'title' => 'Здравствуйте!',
        'description' => 'Вас приветствует ' . config('app.application_name')
    ],
    'contacts' => [
        'description' => 'По всем вопросам и предложениям обращайтесь по:',
        'phone' => 'Телефону: +7(918)891-97-21',
        'email' => 'Электронной почте: vk@grapheme.ru'
    ]
];