<?php

return [
    'pages' => [
        'title' => ['ru' => 'Страницы', 'en' => 'Pages', 'es' => 'Páginas'],
        'options' => [
            ['group_title' => ['ru' => 'Настройка каталогов', 'en' => 'Setting directories', 'es' => 'Configuración de directorios']],
            'templates_dir' => [
                'title' => [
                    'ru' => 'Каталог хранения шаблонов',
                    'en' => 'Catalog store templates',
                    'es' => 'Plantillas de tiendas Catálogo'
                ],
                'note' => [
                    'ru' => 'Корневой каталог: /home/Resources/Views',
                    'en' => 'Root directory: /home/Resources/Views',
                    'es' => 'Directorio raíz: /home/Resources/Views'
                ],
                'type' => 'text',
                'value' => ''
            ]
        ]
    ]
];