<?php

namespace STALKER_CMS\Core\Content\Providers;

use Illuminate\Support\Str;
use STALKER_CMS\Core\Content\Http\Controllers\PublicMenuController;
use STALKER_CMS\Core\Content\Http\Controllers\PublicPagesController;
use STALKER_CMS\Vendor\Models\Languages;
use STALKER_CMS\Vendor\Providers\ServiceProvider;

/**
 * Class ModuleServiceProvider
 * @package STALKER_CMS\Core\Content\Providers
 */
class ModuleServiceProvider extends ServiceProvider {

    /**
     *
     */
    public function boot() {

        $this->setPath(base_path('home'));
        $this->registerViews('site_views');
        $this->registerLocalization('site_lang');
        $this->setPath(__DIR__ . '/../');
        $this->registerViews('core_content_views');
        $this->registerLocalization('core_content_lang');
        $this->registerConfig('core_content::config', 'Config/content.php');
        $this->registerSettings('core_content::settings', 'Config/settings.php');
        $this->registerActions('core_content::actions', 'Config/actions.php');
        $this->registerSystemMenu('core_content::menu', 'Config/menu.php');
        $this->registerBladeDirectives();
        $this->publishesTemplates();
        $this->publishesAssets();
        $this->publishesElixir();
    }

    /**
     *
     */
    public function register() {

        \App::bind('PublicPagesController', function () {
            return new PublicPagesController();
        });
        \App::bind('PublicMenuController', function () {
            return new PublicMenuController();
        });
        $this->app->singleton('Locales', function ($app) {
            $locales = Languages::whereActive(TRUE)->pluck('slug', 'slug')->toArray();
            $locales[\App::getLocale()] = '';
            return $locales;
        });
    }

    /********************************************************************************************************************/
    /**
     * Регистрация blade директив
     */
    public function registerBladeDirectives() {

        \Blade::directive('anchor', function ($expression) {

            if (Str::startsWith($expression, '(')):
                $expression = substr($expression, 1, -1);
            endif;
            if (!empty($expression)):
                $expressions = [];
                foreach (explode(',', $expression) as $parameter):
                    $expressions[] = trim($parameter);
                endforeach;
                switch (count($expressions)):
                    case 1:
                        return '<a href="<?php echo route(\'public.page.\' . ' . $expressions[0] . ');?>"><?php echo ' . $expressions[0] . '; ?></a>';
                    case 2:
                        return '<a href="<?php echo route(\'public.page.\' . ' . $expressions[0] . ');?>"><?php echo ' . $expressions[1] . '; ?></a>';
                    case 3:
                        return '<a class="<?php echo ' . $expressions[2] . '?>" href="<?php echo route(\'public.page.\' . ' . $expressions[0] . ');?>"><?php echo ' . $expressions[1] . '; ?></a>';
                    case 4:
                        return '<a target="_blank" class="<?php echo ' . $expressions[2] . '?>" href="<?php echo route(\'public.page.\' . ' . $expressions[0] . ');?>"><?php echo ' . $expressions[1] . '; ?></a>';
                endswitch;
            endif;
            return NULL;
        });

        \Blade::directive('Menu', function ($expression) {

            if (Str::startsWith($expression, '(')):
                $expression = substr($expression, 1, -1);
            endif;
            if (!empty($expression)):
                $expressions = [];
                foreach (explode(',', $expression, 2) as $parameter):
                    $expressions[] = preg_replace("/[\']/i", '', trim($parameter));
                endforeach;
                switch (count($expressions)):
                    case 1:
                        $template = \PublicMenu::getTemplate($expressions[0]);
                        if (!is_null($template) && view()->exists("site_views::$template")):
                            return "<?php echo \$__env->make('site_views::$template', ['menu_slug' => '$expressions[0]'])->render(); ?>";
                        endif;
                        break;
                    case 2:
                        if (view()->exists("site_views::$expressions[1]")):
                            return "<?php echo \$__env->make('site_views::$expressions[1]', ['menu_slug' => '$expressions[0]'])->render(); ?>";
                        endif;
                        break;
                endswitch;
            endif;
            return NULL;
        });

        \Blade::directive('PageBlock', function ($expression) {

            if (Str::startsWith($expression, '(')):
                $expression = substr($expression, 1, -1);
            endif;
            if (!empty($expression)):
                $Block = '<?php if(isset($blocks[' . $expression . '])):';
                $Block .= '$Filesystem = new \Illuminate\Filesystem\Filesystem();';
                $Block .= '$template = storage_path("framework/views/" . sha1($blocks[' . $expression . ']) . ".php");';
                $Block .= '$compileString = \Blade::compileString($blocks[' . $expression . ']);';
                $Block .= '$Filesystem->put($template, $compileString);';
                $Block .= 'include_once($template);';
                $Block .= 'endif; ?>';
                return $Block;
            endif;
            return NULL;
        });
    }

    /**
     * Публикация шаблонов страниц
     */
    public function publishesTemplates() {

        $this->publishes([
            __DIR__ . '/../Resources/Templates' => base_path('home/Resources'),
        ]);
    }

    /**
     * Публикация ресурсов
     */
    public function publishesAssets() {

        $this->publishes([
            __DIR__ . '/../Resources/Assets' => public_path('packages/content'),
        ]);
    }

    /**
     * Публикация темы сайта
     */
    public function publishesElixir() {

        $this->publishes([
            __DIR__ . '/../Resources/Elixir' => public_path('theme'),
        ]);
    }
}