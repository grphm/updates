<?php
\Route::group(['prefix' => 'admin/content', 'middleware' => 'secure'], function() {

    \Route::get('/clear', ['as' => 'core.content.cache.clear', 'uses' => 'PagesController@clearCash']);
    \Route::post('dashboard/template/edit', ['as' => 'core.content.template.dashboard.edit', 'uses' => 'TemplatesController@editDashboardTemplate']);
    \Route::post('dashboard/template/update', ['as' => 'core.content.template.dashboard.update', 'uses' => 'TemplatesController@updateDashboardTemplate']);
    \Route::resource('pages', 'PagesController',
        [
            'except' => ['show'],
            'names' => [
                'index' => 'core.content.pages.index',
                'create' => 'core.content.pages.create',
                'store' => 'core.content.pages.store',
                'edit' => 'core.content.pages.edit',
                'update' => 'core.content.pages.update',
                'destroy' => 'core.content.pages.destroy'
            ]
        ]
    );
    \Route::resource('pages/{page_id}/blocks', 'PagesBlocksController',
        [
            'except' => ['show'],
            'names' => [
                'index' => 'core.content.pages.blocks_index',
                'create' => 'core.content.pages.blocks_create',
                'store' => 'core.content.pages.blocks_store',
                'edit' => 'core.content.pages.blocks_edit',
                'update' => 'core.content.pages.blocks_update',
                'destroy' => 'core.content.pages.blocks_destroy'
            ]
        ]
    );
    \Route::resource('menu', 'MenuController',
        [
            'except' => ['show'],
            'names' => [
                'index' => 'core.content.menu.index',
                'create' => 'core.content.menu.create',
                'store' => 'core.content.menu.store',
                'edit' => 'core.content.menu.edit',
                'update' => 'core.content.menu.update',
                'destroy' => 'core.content.menu.destroy'
            ]
        ]
    );
    \Route::resource('menu.items', 'MenuItemController',
        [
            'except' => ['show'],
            'names' => [
                'index' => 'core.content.menu.items_index',
                'create' => 'core.content.menu.items_create',
                'store' => 'core.content.menu.items_store',
                'edit' => 'core.content.menu.items_edit',
                'update' => 'core.content.menu.items_update',
                'destroy' => 'core.content.menu.items_destroy'
            ]
        ]
    );
    \Route::post('menu/{menu_id}/sortable', ['as' => 'core.content.menu.sortable', 'uses' => 'MenuItemController@sortable']);
    \Route::resource('templates', 'TemplatesController',
        [
            'except' => ['show'],
            'names' => [
                'index' => 'core.content.templates.index',
                'create' => 'core.content.templates.create',
                'store' => 'core.content.templates.store',
                'edit' => 'core.content.templates.edit',
                'update' => 'core.content.templates.update',
                'destroy' => 'core.content.templates.destroy'
            ]
        ]
    );
    \Route::resource('languages', 'LanguagesController',
        [
            'only' => ['index', 'store'],
            'names' => [
                'index' => 'core.content.languages.index',
                'store' => 'core.content.languages.store',
            ]
        ]
    );
});
\Route::group(['prefix' => \PublicPage::localePrefix(), 'middleware' => 'public'], function() {

    $pages = \PublicPage::getPagesList();
    if($pages->count()):
        foreach($pages as $page):
            if($page->start_page):
                Route::get('', ['as' => 'public.page.index', function() use ($page) {

                    return \PublicPage::showPage($page);
                }]);
                break;
            endif;
        endforeach;
        foreach($pages as $page):
            if($page->start_page == FALSE):
                $page_url = (!empty($page->seo_url) && !is_null($page->seo_url)) ? $page->seo_url : $page->slug;
                Route::get($page_url, ['as' => 'public.page.'.$page->slug, function() use ($page) {

                    return \PublicPage::showPage($page);
                }]);
            endif;
        endforeach;
    endif;
});