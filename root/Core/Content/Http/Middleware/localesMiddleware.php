<?php

namespace STALKER_CMS\Core\Content\Http\Middleware;

use Closure;

/**
 * Установка локали
 * Class localesMiddleware
 * @package STALKER_CMS\Core\Content\Http\Middleware
 */
class localesMiddleware {

    public function handle($request, Closure $next, $package = NULL, $module = NULL, $setting = NULL) {

        if (\PermissionsController::isPackageEnabled('core_content')):
            \App::setLocale(\PublicPage::getLocale());
        endif;
        return $next($request);
    }
}