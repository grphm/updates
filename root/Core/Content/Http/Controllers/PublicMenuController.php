<?php
namespace STALKER_CMS\Core\Content\Http\Controllers;


use Illuminate\Support\Collection;
use STALKER_CMS\Core\Content\Models\Menu;
use STALKER_CMS\Core\Content\Models\MenuItem;
use STALKER_CMS\Core\Content\Models\PageTemplate;

/**
 * Контроллер для получения меню в гостевом интерфейсе
 * Class PublicMenuController
 * @package STALKER_CMS\Core\Content\Http\Controllers
 */
class PublicMenuController extends ModuleController {

    /**
     * Получение меню по системному имени
     * @param $slug
     * @return array
     */
    public function make($slug) {

        $menu_items = $items = [];
        if (Menu::where(['locale' => \App::getLocale(), 'slug' => $slug])->exists()):
            $menu_items_list = Menu::where(['locale' => \App::getLocale(), 'slug' => $slug])->with(['items' => function ($query) {
                $query->orderBy('order', 'asc');
                $query->with('page');
                if (\PermissionsController::isPackageEnabled('core_uploads')):
                    $query->with('file');
                endif;
            }])->first();
            foreach ($menu_items_list->items as $item):
                $items[$item->parent_id][] = $item->toArray();
            endforeach;
            $menu_items = \STALKER_CMS\Vendor\Helpers\recursive($items, 0);
        endif;
        return $menu_items;
    }

    /**
     * Возвращает путь к шаблону меню по системному имени
     * @param $slug
     * @return null
     */
    public function getTemplate($slug) {

        if ($menu = Menu::where(['locale' => \App::getLocale(), 'slug' => $slug])->first()):
            $template = PageTemplate::whereFind(['menu_type' => 'menu', 'locale' => \App::getLocale(), 'id' => $menu->template_id]);
            return \PublicPage::getViewPath($template);
        else:
            return NULL;
        endif;
    }

    /**
     * Возвращает список элементов меню по ID меню
     * за исключении текущего элемента
     * @param $menu_id
     * @param null $exclude_id
     * @return Collection
     */
    public function listItems($menu_id, $exclude_id = NULL) {

        $items = new Collection();
        $items->put(NULL, \Lang::get('core_content_lang::menu_items.top_item'));
        foreach (MenuItem::whereMenuId($menu_id)->lists('title', 'id') as $item_id => $item_title):
            if (!is_null($exclude_id) && $item_id == $exclude_id):
                continue;
            endif;
            $items->put($item_id, $item_title);
        endforeach;
        return $items;
    }
}