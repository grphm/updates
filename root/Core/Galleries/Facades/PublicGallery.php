<?php
namespace STALKER_CMS\Core\Galleries\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Фасад контроллера Галерия для гостевого интерфейса
 * Class PublicGallery
 * @package STALKER_CMS\Core\Galleries\Facades
 */
class PublicGallery extends Facade {

    /**
     * @return string
     */
    protected static function getFacadeAccessor() {

        return 'PublicGalleriesController';
    }
}