<?php
namespace STALKER_CMS\Core\Galleries\Models;

use League\Flysystem\Exception;
use STALKER_CMS\Vendor\Interfaces\ModelInterface;
use STALKER_CMS\Vendor\Models\BaseModel;
use STALKER_CMS\Vendor\Traits\ModelTrait;

class Gallery extends BaseModel implements ModelInterface {

    use ModelTrait;
    protected $table = 'galleries';
    protected $fillable = ['package_id', 'template_id', 'unit_id', 'name', 'title', 'resizable', 'crop_use', 'aspect_ratio', 'crop_wight', 'crop_height', 'crop_wight_thumbnail', 'crop_height_thumbnail'];
    protected $guarded = [];

    public function insert($request) {

        $this->slug = $request::input('slug');
        $this->title = $request::input('title');
        $this->description = $request::input('description');
        $this->package_id = $request::has('package_id') ? $request::input('package_id') : NULL;
        $this->template_id = $request::input('template_id');
        $this->unit_id = $request::has('unit_id') ? $request::input('unit_id') : NULL;
        $this->crop_use = $request::has('crop_use') ? TRUE : FALSE;
        $this->resizable = $request::has('resizable') ? TRUE : FALSE;
        $this->aspect_ratio = $request::input('aspect_ratio');
        $this->crop_wight = $request::input('crop_wight');
        $this->crop_height = $request::input('crop_height');
        $this->crop_wight_thumbnail = $request::input('crop_wight_thumbnail');
        $this->crop_height_thumbnail = $request::input('crop_height_thumbnail');
        $this->save();
        return $this;
    }

    public function replace($id, $request) {

        $model = $this::whereFind(['id' => $id]);
        $model->slug = $request::input('slug');
        $model->title = $request::input('title');
        $model->template_id = $request::input('template_id');
        $model->description = $request::input('description');
        $model->crop_use = $request::has('crop_use') ? TRUE : FALSE;
        $model->resizable = $request::has('resizable') ? TRUE : FALSE;
        $model->aspect_ratio = $request::input('aspect_ratio');
        $model->crop_wight = $request::input('crop_wight');
        $model->crop_height = $request::input('crop_height');
        $model->crop_wight_thumbnail = $request::input('crop_wight_thumbnail');
        $model->crop_height_thumbnail = $request::input('crop_height_thumbnail');
        $model->save();
        $model->touch();
        return $model;
    }

    public function remove($id) {

        $instance = static::findOrFail($id);
        return $instance->delete();
    }

    public function search(array $attributes) {
        // TODO: Implement search() method.
    }

    public function filter(array $attributes) {
        // TODO: Implement filter() method.
    }

    public function sort(array $attributes) {
        // TODO: Implement sort() method.
    }

    public function photos() {

        return $this->hasMany('\STALKER_CMS\Core\Galleries\Models\Photo', 'gallery_id', 'id');
    }

    public function package() {

        return $this->belongsTo('\STALKER_CMS\Vendor\Models\Packages', 'package_id', 'id');
    }

    public function getPhotosCountAttribute() {

        if(!empty($this->photos)):
            return $this->photos->count()
                ? $this->photos->count().' '.\Lang::choice('изображение|изображания|изображений', $this->photos->count())
                : 'Нет загруженных изображений';
        endif;
    }

    public function getPackageIcoAttribute() {

        if(!empty($this->package)):
            return '<i class="fa '.config($this->package->slug.'::config.package_icon').'"> </i>';
        endif;
    }

    public function getPackageTitleAttribute() {

        if(!empty($this->package)):
            return $this->package->title;
        endif;
    }

    public function getRatioAttribute() {

        try {
            $ratio = explode('/', $this->attributes['aspect_ratio']);
            if(count($ratio) == 2):
                return round($ratio[0] / $ratio[1], 3);
            endif;
        } catch(Exception $e) {
            return 1;
        }
    }

    public function getTemplateSlugAttribute() {

        if(!empty($this->template)):
            if($this->template->required):
                return NULL;
            else:
                return ', \''.substr($this->template->path, 0, -10).'\'';
            endif;
        endif;
    }

    /**
     * Шаблон галереи
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function template() {

        return $this->hasOne('\STALKER_CMS\Core\Galleries\Models\GalleryTemplate', 'id', 'template_id');
    }

    /***************************************************************************************************************/
    public static function getStoreRules() {

        return ['slug' => 'required', 'title' => 'required', 'crop_wight_thumbnail' => 'required', 'crop_height_thumbnail' => 'required'];
    }

    public static function getUpdateRules() {

        return ['slug' => 'required', 'title' => 'required', 'crop_wight_thumbnail' => 'required', 'crop_height_thumbnail' => 'required'];
    }
}