<?php

return [
    'package_name' => 'core_galleries',
    'package_title' => [
        'ru' => 'Модуль галерей',
        'en' => 'Module galleries',
        'es' => 'Galerías del módulo'
    ],
    'package_icon' => 'zmdi zmdi-collection-image',
    'version' => [
        'ver' => 1.2,
        'date' => '19.09.2016'
    ]
];