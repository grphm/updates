@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('head')
@stop
@section('breadcrumb')
    <ol class="breadcrumb" style="margin-bottom: 5px;">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('core_galleries::menu.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_galleries::menu.title')) !!}
        </li>
        <li class="active">
            <i class="{{ config('core_galleries::menu.menu_child.templates.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_galleries::menu.menu_child.templates.title')) !!}
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="{{ config('core_galleries::menu.menu_child.templates.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_galleries::menu.menu_child.templates.title')) !!}
        </h2>
    </div>
    @BtnAdd('core.galleries.templates.create')
    <div class="card">
        <div class="card-body card-padding m-h-250">
            @if($templates->count())
                <div class="row">
                    <div class="col-sm-12 m-b-25">
                        <p class="f-500 c-black m-b-0">@lang('core_galleries_lang::templates.templates')</p>
                        <small>@lang('core_galleries_lang::templates.root_directory')</small>
                        <div class="listview lv-bordered lv-lg">
                            @forelse($templates as $template)
                                <div class="js-item-container lv-item media">
                                    <div class="media-body">
                                        <div class="lv-title">
                                            {{ $template->title }}
                                        </div>
                                        <small class="lv-small">
                                            <strong>{{ $template->path }}</strong>
                                        </small>
                                        <div class="lv-actions actions dropdown">
                                            <a aria-expanded="true" data-toggle="dropdown" href="">
                                                <i class="zmdi zmdi-more-vert"></i>
                                            </a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li>
                                                    <a href="{{ route('core.galleries.templates.edit', $template->id) }}">
                                                        @lang('core_galleries_lang::templates.edit')
                                                    </a>
                                                </li>
                                                @if($template->required)
                                                @else
                                                    <li class="divider"></li>
                                                    <li>
                                                        {!! Form::open(['route' => ['core.galleries.templates.destroy', $template->id], 'method' => 'DELETE']) !!}
                                                        <button type="submit"
                                                                class="form-confirm-warning btn-link pull-right c-red p-r-15"
                                                                autocomplete="off"
                                                                data-question="@lang('core_galleries_lang::templates.delete.question') &laquo;{{ $template->title }}&raquo;?"
                                                                data-confirmbuttontext="@lang('core_galleries_lang::templates.delete.confirmbuttontext')"
                                                                data-cancelbuttontext="@lang('core_galleries_lang::templates.delete.cancelbuttontext')">
                                                            @lang('core_galleries_lang::templates.delete.submit')
                                                        </button>
                                                    {!! Form::close() !!}
                                                @endif
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            @empty
                                <h2 class="f-16 c-gray">@lang('core_galleries_lang::templates.empty')</h2>
                            @endforelse
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            @else
                <h2 class="f-16 c-gray">@lang('core_galleries_lang::templates.empty')</h2>
            @endif
        </div>
    </div>
@stop