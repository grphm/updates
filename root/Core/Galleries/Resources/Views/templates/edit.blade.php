@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('head')
@stop
@section('breadcrumb')
    <ol class="breadcrumb" style="margin-bottom: 5px;">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('core_galleries::menu.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_galleries::menu.title')) !!}
        </li>
        <li>
            <a href="{{ route('core.galleries.templates.index') }}">
                <i class="{{ config('core_galleries::menu.menu_child.templates.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_galleries::menu.menu_child.templates.title')) !!}
            </a>
        </li>
        <li class="active">
            <i class="zmdi zmdi-edit"></i> @lang('core_galleries_lang::templates.replace.breadcrumb')
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="zmdi zmdi-edit"></i> @lang('core_galleries_lang::templates.replace.title')
        </h2>
    </div>
    <div class="card">
        <div class="card-body card-padding">
            <div class="row">
                {!! Form::model($template, ['route' => ['core.galleries.templates.update', $template->id], 'class' => 'form-validate', 'id' => 'edit-gallery-template-form', 'method' => 'PUT']) !!}
                <div class="col-sm-9">
                    <div class="form-group">
                        <p class="c-gray m-b-20">@lang('core_galleries_lang::templates.replace.form.content')</p>

                        <pre id="template_content">{{ $template_content }}</pre>
                        {!! Form::textarea('content', NULL, ['class' => 'hidden', 'data-autosize-on' => 'true']) !!}
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('title', NULL, ['class'=>'input-sm form-control fg-input']) !!}
                        </div>
                        <label class="fg-label">@lang('core_galleries_lang::templates.replace.form.title')</label>
                        <small class="help-description">@lang('core_galleries_lang::templates.replace.form.title_help_description')</small>
                    </div>
                    <button type="submit" autocomplete="off" class="btn btn-primary btn-sm m-t-10 waves-effect">
                        <i class="fa fa-save"></i>
                        <span class="btn-text">@lang('core_galleries_lang::templates.replace.form.submit')</span>
                    </button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop
@section('modal')
@stop
@section('scripts_before')
    {!! Html::script('packages/galleries/js/ace.js') !!}
    <script>
        var editor = ace.edit("template_content");
        editor.setTheme("ace/theme/monokai");
        editor.getSession().setMode("ace/mode/javascript");
        editor.getSession().setUseSoftTabs(true);
        document.getElementById('template_content').style.fontSize = '14px';
        editor.getSession().setUseWrapMode(true);
        editor.setShowPrintMargin(false);
        editor.setOptions({
            maxLines: Infinity
        });
        $("#edit-gallery-template-form button[type='submit']").click(function () {
            $("#edit-gallery-template-form textarea[name='content']").val(editor.getValue());
        });
    </script>
@stop
@section('scripts_after')
@stop