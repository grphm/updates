@set($images, \PublicGallery::images($gallery_slug))
@if($images->count())
    <div class="slider">
        @foreach($images as $image)
            @if($image->ExistOnDisk)
                <div class="slider-img">
                    <img src="{{ $image->AssetPath }}" alt="{{ $image->alt }}" title="{{ $image->title }}"/>
                </div>
            @endif
        @endforeach
    </div>
@endif