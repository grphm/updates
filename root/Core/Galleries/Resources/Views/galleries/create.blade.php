@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb" style="margin-bottom: 5px;">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('core_galleries::menu.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_galleries::menu.title')) !!}
        </li>
        <li class="active">
            <i class="zmdi zmdi-plus"></i> @lang('core_galleries_lang::galleries.insert.breadcrumb')
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="zmdi zmdi-plus"></i> @lang('core_galleries_lang::galleries.insert.title')
        </h2>
    </div>
    <div class="card">
        <div class="card-body card-padding">
            {!! Form::open(['route' => 'core.galleries.store', 'class' => 'form-validate', 'id' => 'add-gallery-form']) !!}
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('title', NULL, ['class'=>'input-sm form-control fg-input']) !!}
                        </div>
                        <label class="fg-label">@lang('core_galleries_lang::galleries.insert.form.title')</label>
                        <small class="help-description">@lang('core_galleries_lang::galleries.insert.form.title_help_description')</small>
                    </div>
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('slug', NULL, ['class'=>'input-sm form-control fg-input']) !!}
                        </div>
                        <label class="fg-label">@lang('core_galleries_lang::galleries.insert.form.slug')</label>
                        <small class="help-description">@lang('core_galleries_lang::galleries.insert.form.slug_help_description')</small>
                    </div>
                    <div class="form-group">
                        <p class="c-gray m-b-10">@lang('core_galleries_lang::galleries.insert.form.template')</p>
                        {!! Form::select('template_id', $templates, NULL,['class' => 'selectpicker', 'autocomplete' => 'off']) !!}
                    </div>
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::textarea('description', NULL, ['class' => 'form-control auto-size fg-input', 'data-autosize-on' => 'true', 'rows' => 1]) !!}
                        </div>
                        <label class="fg-label">@lang('core_galleries_lang::galleries.insert.form.description')</label>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="checkbox m-b-25">
                        <label>
                            {!! Form::checkbox('crop_use', TRUE, TRUE, ['autocomplete' => 'off']) !!}
                            <i class="input-helper"></i> @lang('core_galleries_lang::galleries.insert.form.crop_use')
                        </label>
                    </div>
                    <div class="js-crop-properties">
                        <div class="form-group fg-float">
                            <div class="fg-line">
                                {!! Form::text('aspect_ratio', NULL, ['class'=>'input-sm form-control fg-input']) !!}
                            </div>
                            <label class="fg-label">@lang('core_galleries_lang::galleries.insert.form.aspect_ratio')</label>
                            <small class="help-description">@lang('core_galleries_lang::galleries.insert.form.aspect_ratio_help_description')</small>
                        </div>
                        <div class="form-group fg-float">
                            <div class="fg-line">
                                {!! Form::text('crop_wight', NULL, ['class'=>'input-sm form-control fg-input input-numeric']) !!}
                            </div>
                            <label class="fg-label">@lang('core_galleries_lang::galleries.insert.form.crop_wight')</label>
                            <small class="help-description">@lang('core_galleries_lang::galleries.insert.form.crop_wight_help_description')</small>
                        </div>
                        <div class="form-group fg-float m-b-10">
                            <div class="fg-line">
                                {!! Form::text('crop_height', NULL, ['class'=>'input-sm form-control fg-input input-numeric']) !!}
                            </div>
                            <label class="fg-label">@lang('core_galleries_lang::galleries.insert.form.crop_height')</label>
                            <small class="help-description">@lang('core_galleries_lang::galleries.insert.form.crop_height_help_description')</small>
                        </div>
                        <div role="alert" class="alert alert-info f-10 p-10">
                            @lang('core_galleries_lang::galleries.insert.form.info')
                        </div>
                        <div class="checkbox m-b-25">
                            <label>
                                {!! Form::checkbox('resizable', TRUE, TRUE, ['autocomplete' => 'off']) !!}
                                <i class="input-helper"></i> @lang('core_galleries_lang::galleries.insert.form.resizable')
                            </label>
                            <small class="help-description">
                                @lang('core_galleries_lang::galleries.insert.form.resizable_help_description')
                            </small>
                        </div>
                        <div class="form-group fg-float">
                            <div class="fg-line">
                                {!! Form::text('crop_wight_thumbnail', 200, ['class'=>'input-sm form-control fg-input input-numeric']) !!}
                            </div>
                            <label class="fg-label">@lang('core_galleries_lang::galleries.insert.form.crop_wight_thumbnail')</label>
                        </div>
                        <div class="form-group fg-float">
                            <div class="fg-line">
                                {!! Form::text('crop_height_thumbnail', 200, ['class'=>'input-sm form-control fg-input input-numeric']) !!}
                            </div>
                            <label class="fg-label">@lang('core_galleries_lang::galleries.insert.form.crop_height_thumbnail')</label>
                        </div>
                    </div>
                </div>
            </div>
            <button class="btn btn-primary btn-sm m-t-10 waves-effect" autocomplete="off" type="submit">
                <i class="fa fa-save"></i>
                <span class="btn-text">@lang('core_galleries_lang::galleries.insert.form.submit')</span>
            </button>
            {!! Form::close() !!}
        </div>
    </div>
@stop
@section('scripts_before')
@stop
@section('scripts_after')
    <script>
        $(function () {
            $.mask.definitions['9'] = '[0-9 ]';
            $("input[name='aspect_ratio']").mask("99/99", {placeholder: " "});
            $("input[name='aspect_ratio']").focusout(function (event) {
                var value = $.trim(event.target.value);
                if (value == '/') {
                    $(this).attr('value', '').val('');
                }
            });
            $("#add-gallery-form input[name='crop_use']").click(function () {
                if ($(this).prop('checked')) {
                    $(".js-crop-properties").removeClass('hidden');
                } else {
                    $(".js-crop-properties").addClass('hidden');
                }
            });
        })
    </script>
@stop