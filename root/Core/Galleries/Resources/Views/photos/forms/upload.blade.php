<div class="cropping-image">
    {!! Form::open(['route' => ['core.galleries.photos_store', $gallery->id], 'class' => 'js-upload-form']) !!}
    {!! Form::file('file', ['id' => 'js-upload-file', 'class' => 'hidden']) !!}
    {!! Form::hidden('photo', NULL, ['autocomplete' => 'off']) !!}
    {!! Form::hidden('thumbnail', NULL, ['autocomplete' => 'off']) !!}
    {!! Form::hidden('alt', NULL, ['autocomplete' => 'off']) !!}
    {!! Form::hidden('title', NULL, ['autocomplete' => 'off']) !!}
    {!! Form::hidden('original_name', NULL, ['autocomplete' => 'off']) !!}
    {!! Form::hidden('mime_type', NULL, ['autocomplete' => 'off']) !!}
    <div class="listview lv-bordered lv-lg">
        <div class="lv-body">
            <div class="lv-item media p-10">
                <div class="pull-left js-file-preview"></div>
                <div class="media-body">
                    <div class="lv-title">
                        <span id="js-file-name"></span>
                    </div>
                    <small class="lv-small c-red js-file-error hidden"></small>
                    <ul class="lv-attrs">
                        <li>@lang('core_galleries_lang::photos.form.size'): <span id="js-file-size"></span></li>
                        <li>@lang('core_galleries_lang::photos.form.type'): <span id="js-file-type"></span></li>
                    </ul>
                    <div class="m-t-20">
                        {!! Form::button(\Lang::get('core_galleries_lang::photos.form.upload'), ['type' => 'submit', 'class' => 'btn btn-warning hidden']) !!}
                        <div class="js-upload-progress hidden">
                            <p>
                                @lang('core_galleries_lang::photos.form.uploading')
                                <span class="js-percent-complete">0</span> @lang('core_galleries_lang::photos.form.done')
                                <button type="button" class="close xhr-close font-xs c-red pull-right">
                                    <i class="zmdi zmdi-close"></i>
                                </button>
                            </p>
                            <div class="progress progress-striped progress-sm no-padding">
                                <div style="width: 0%;" role="progressbar"
                                     class="progress-bar bg-color-blueLight"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>