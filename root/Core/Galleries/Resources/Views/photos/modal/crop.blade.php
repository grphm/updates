<a class="btn btn-default waves-effect hidden" href="#showUploadModalClick" data-toggle="modal"></a>
<div class="modal fade" id="showUploadModalClick" data-backdrop="static" data-keyboard="false" tabindex="-1"
     role="dialog"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    <button type="button" class="btn btn-link btn-close pull-right p-0 p-r-5" data-dismiss="modal">
                        <i class="zmdi zmdi-close"></i>
                    </button>
                </h4>
            </div>
            <div class="modal-body">
                @include('core_galleries_views::photos.forms.upload')
            </div>
        </div>
    </div>
</div>