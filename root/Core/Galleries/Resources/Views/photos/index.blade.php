@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('head')
    {!! Html::meta(['name' => 'crop_ratio', 'content' => $gallery->ratio]) !!}
    {!! Html::meta(['name' => 'crop_resizable', 'content' => $gallery->resizable ? TRUE : FALSE]) !!}
    {!! Html::meta(['name' => 'crop_wight', 'content' => $gallery->crop_wight ? $gallery->crop_wight : NULL]) !!}
    {!! Html::meta(['name' => 'crop_height', 'content' => $gallery->crop_height ? $gallery->crop_height : NULL]) !!}
    {!! Html::meta(['name' => 'crop_wight_thumbnail', 'content' => $gallery->crop_wight_thumbnail ? $gallery->crop_wight_thumbnail : NULL]) !!}
    {!! Html::meta(['name' => 'crop_height_thumbnail', 'content' => $gallery->crop_height_thumbnail ? $gallery->crop_height_thumbnail : NULL]) !!}
@stop
@section('breadcrumb')
    <ol class="breadcrumb" style="margin-bottom: 5px;">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li>
            <a href="{{ route('core.galleries.index') }}">
                <i class="{{ config('core_galleries::menu.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_galleries::menu.title')) !!}
            </a>
        </li>
        <li class="active">
            <i class="zmdi zmdi-wallpaper"></i> @lang('core_galleries_lang::photos.breadcrumb')
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="zmdi zmdi-wallpaper"></i> @lang('core_galleries_lang::photos.title')
        </h2>
    </div>
    @if(\PermissionsController::allowPermission('core_galleries', 'images', FALSE))
        <a class="btn bgm-deeppurple m-btn btn-float waves-effect waves-circle waves-float waves-effect waves-circle waves-float js-choice-upload-file">
            <i class="zmdi zmdi-upload"></i>
        </a>
    @endif
    <div class="card">
        @if($photos->count())
            <div class="lv-header-alt clearfix">
                <ul class="lv-actions actions">
                    <li class="dropdown">
                        <a href="" data-toggle="dropdown" aria-expanded="true">
                            <i class="zmdi zmdi-more-vert"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li>
                                <a href="javascript:void(0);" class="js-check-all">
                                    <i class="zmdi zmdi-check-all zmdi-hc-fw"></i> @lang('core_galleries_lang::photos.select_all')
                                </a>
                            </li>
                            @if(\PermissionsController::allowPermission('core_galleries', 'images', FALSE))
                                <li class="divider"></li>
                                <li>
                                    {!! Form::open(['route' => ['core.galleries.photos_destroy', 'selected'], 'class' => 'selected-delete-form', 'method' => 'DELETE']) !!}
                                    {!! Form::hidden('gallery', $gallery->id) !!}
                                    <button type="submit"
                                            class="form-confirm-warning btn-link pull-right c-red m-5"
                                            autocomplete="off"
                                            data-question="@lang('core_galleries_lang::photos.delete_selected.question')"
                                            data-confirmbuttontext="@lang('core_galleries_lang::photos.delete_selected.confirmbuttontext')"
                                            data-cancelbuttontext="@lang('core_galleries_lang::photos.delete_selected.cancelbuttontext')">
                                        @lang('core_galleries_lang::photos.delete_selected.submit')
                                    </button>
                                    {!! Form::close() !!}
                                </li>
                                <li>
                                    {!! Form::open(['route' => ['core.galleries.photos_destroy', 'all'],'class' => 'all-delete-form', 'method' => 'DELETE']) !!}
                                    {!! Form::hidden('gallery', $gallery->id) !!}
                                    <button type="submit"
                                            class="form-confirm-warning btn-link pull-right c-red m-5"
                                            autocomplete="off"
                                            data-question="@lang('core_galleries_lang::photos.delete_all.question')"
                                            data-confirmbuttontext="@lang('core_galleries_lang::photos.delete_all.confirmbuttontext')"
                                            data-cancelbuttontext="@lang('core_galleries_lang::photos.delete_all.cancelbuttontext')">
                                        @lang('core_galleries_lang::photos.delete_all.submit')
                                    </button>
                                    {!! Form::close() !!}
                                </li>
                            @endif
                        </ul>
                    </li>
                </ul>
            </div>
        @endif
        <div class="card-body card-padding m-h-250">
            @if($photos->count())
                <ul class="lightbox photos photo-nestable-list gallery-photos-list">
                    @foreach($photos as $photo)
                        <li data-src="{{ $photo->asset_path }}"
                            class="col-md-2 col-sm-4 col-xs-6 js-item-container nested-element"
                            data-element="{!! $photo->id !!}" data-title="{{ $photo->title }}" data-alt="{{ $photo->alt }}">
                            <div style="position: absolute; top: 8px; left: 15px; z-index: 99;">
                                <i class="zmdi zmdi-swap f-20 p-t-10 cursor-move c-white"></i>
                            </div>
                            <div class="lightbox-item p-item">
                                <ul class="actions photo-actions"
                                    style="position: absolute; top: 10px; right: 10px;">
                                    <li class="dropdown">
                                        <a aria-expanded="false" data-toggle="dropdown" href="">
                                            <i class="zmdi zmdi-more-vert c-white"></i>
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li>
                                                <a href="#editPhotoLabels" class="js-edit-item">
                                                    @lang('core_galleries_lang::photos.edit')
                                                </a>
                                            </li>
                                            <li>
                                                {!! Form::checkbox('files[]', $photo->id, NULL, ['class' => 'hidden', 'autocomplete' => 'off']) !!}
                                                <a href="javascript:void(0);" class="js-check-item">
                                                    @lang('core_galleries_lang::photos.select')
                                                </a>
                                                <a href="javascript:void(0);" class="js-check-item hidden">
                                                    @lang('core_galleries_lang::photos.cancel_selection')
                                                </a>
                                            </li>
                                            @if(\PermissionsController::allowPermission('core_galleries', 'images', FALSE))
                                                <li class="divider"></li>
                                                <li>
                                                    {!! Form::open(['route' => ['core.galleries.photos_destroy', $photo->id], 'method' => 'DELETE']) !!}
                                                    {!! Form::hidden('gallery', $gallery->id) !!}
                                                    <button type="submit"
                                                            class="form-confirm-warning btn-link pull-right c-red p-r-15"
                                                            autocomplete="off"
                                                            data-question="@lang('core_galleries_lang::photos.delete.question') &laquo;{{ $photo->original_name }}&raquo;?"
                                                            data-confirmbuttontext="@lang('core_galleries_lang::photos.delete.confirmbuttontext')"
                                                            data-cancelbuttontext="@lang('core_galleries_lang::photos.delete.cancelbuttontext')">
                                                        @lang('core_galleries_lang::photos.delete.submit')
                                                    </button>
                                                    {!! Form::close() !!}
                                                </li>
                                            @endif
                                        </ul>
                                    </li>
                                </ul>
                                <img src="{!! $photo->asset_thumbnail_path !!}" title="{{ $photo->title }}"
                                     alt="{{ $photo->alt }}"/>
                            </div>
                        </li>
                    @endforeach
                </ul>
                <div class="clearfix"></div>
            @else
                <h2 class="f-16 c-gray">@lang('core_galleries_lang::photos.empty')</h2>
            @endif
        </div>
    </div>
    @if($gallery->crop_use)
        @galleryCropImage()
    @else
        @galleryPreviewImage()
    @endif
@stop
@section('modal')
    @if($gallery->crop_use)
        @include('core_galleries_views::photos.modal.crop')
    @else
        @include('core_galleries_views::photos.modal.preview')
    @endif
    <div class="modal" id="editPhotoLabels">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"></h4>
                </div>
                {!! Form::open(['route' => 'core.galleries.photos_update', 'class' => 'form-validate', 'id' => 'edit-gallery-photo-form']) !!}
                {!! Form::hidden('photo_id') !!}
                <div class="modal-body">
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('title', NULL, ['class'=>'input-sm form-control fg-input']) !!}
                        </div>
                        <label class="fg-label">@lang('core_galleries_lang::photos.crop.title')</label>
                    </div>
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('alt', NULL, ['class'=>'input-sm form-control fg-input']) !!}
                        </div>
                        <label class="fg-label">@lang('core_galleries_lang::photos.crop.alt_text')</label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success waves-effect">
                        <i class="fa fa-save"></i>
                        <span>@lang('core_galleries_lang::photos.modal_edit.submit')</span>
                    </button>
                    <a href="javascript:void(0);" class="js-edit-cancel btn btn-danger waves-effect">
                        <span>@lang('core_galleries_lang::photos.modal_edit.cancel')</span>
                    </a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop
@section('scripts_before')
    <script>
        var $_form_upload = $('.js-upload-form');
        $(".js-check-item").click(function (event) {
            var input = $(this).siblings('input:checkbox');
            $('.dropdown').removeClass('open');
            event.preventDefault();
            event.stopPropagation();
            if (input.is(':checked')) {
                input.prop("checked", false);
                input.parents(".p-item").removeClass('item-selected');
            } else {
                input.prop("checked", true);
                input.parents(".p-item").addClass('item-selected');
            }
            $(this).addClass('hidden');
            $(this).siblings('.js-check-item').removeClass('hidden');
        });
        $(".photo-actions").click(function (event) {
            event.stopPropagation();
            event.preventDefault();
            $(this).find('.form-confirm-warning').off('click');
            if ($(this).find('.dropdown').hasClass('open')) {
                $(this).find('.dropdown').removeClass('open');
            } else {
                $(this).find('.dropdown').addClass('open');
                $(this).find('.form-confirm-warning').on('click', function (event) {
                    event.preventDefault();
                    $(this).parents('.dropdown').removeClass('open');
                    BASIC.ajax = {
                        url: $(this).parents('form').attr('action'),
                        type: $(this).parents('form').find('input[name="_method"]').val(),
                        data: $(this).parents('form').formSerialize()
                    }
                    if (typeof BASIC.ajax.type == 'undefined') {
                        BASIC.ajax.type = $(this).parents('form').attr('method')
                    }
                    BASIC.ShowConfirmDialog(this);
                    $(this).find('.dropdown').removeClass('open');
                });
            }
        });
        $(".selected-delete-form button[type='submit']").click(function () {
            $(".selected-delete-form .js-dynamic-append").remove();
            $(".photos input[type='checkbox']:checked").each(function (index, element) {
                $(".selected-delete-form").append('<input type="hidden" class="js-dynamic-append" name="files[]" value="' + $(element).val() + '">');
            })
        });
    </script>
@stop
@section('scripts_after')
    <script>
        $(function () {
            @foreach($allowed_types as $type)
                BASIC.setAllowedFileType({!! json_encode($type) !!});
            @endforeach
            $(".js-choice-upload-file").click(function (event) {
                $("#js-upload-file").click();
            });
            $(".js-check-all").click(function () {
                $(".photos input[type='checkbox']").prop('checked', true);
                $(".photos .p-item").addClass('item-selected');
                $(".photos .js-check-item").addClass('hidden');
                $(".photos .dropdown-menu").each(function (index, element) {
                    $(element).find('.js-check-item').eq(1).removeClass('hidden');
                });
            });
            BASIC.cropping.ratio = parseInt($("meta[name='crop_ratio']").attr('content')) || NaN;
            BASIC.cropping.boxResizable = parseInt($("meta[name='crop_resizable']").attr('content')) == 1 ? true : false;
            BASIC.cropping.width = parseInt($("meta[name='crop_wight']").attr('content')) || 800;
            BASIC.cropping.height = parseInt($("meta[name='crop_height']").attr('content')) || 600;
            BASIC.cropping.thumbnail.width = parseInt($("meta[name='crop_wight_thumbnail']").attr('content')) || 200;
            BASIC.cropping.thumbnail.height = parseInt($("meta[name='crop_height_thumbnail']").attr('content')) || 200;
            BASIC.cropping.setData = {
                x: 0,
                y: 0,
                width: BASIC.cropping.width,
                height: BASIC.cropping.height,
                rotate: false,
                scaleX: 1,
                scaleY: 1
            };
            function setImgSize(imgSrc) {
                var newImg = new Image();
                newImg.src = imgSrc;
                var height = newImg.height;
                var width = newImg.width;
                $(newImg).load(function () {
                    $(".js-preview-image-wight").html(newImg.width);
                    $(".js-preview-image-height").html(newImg.height);
                    $(".cmsPreviewOverlay").show();
                });
            }

            $(document).on("change", "#js-upload-file", function (event) {
                var form = event.target.form;
                var file = event.target.files[0];
                $(form).find(".js-upload-progress").addClass('hidden');
                $(form).find('#js-file-name').html(file.name);
                $(form).find('#js-file-type').html(BASIC.getFileType(file));
                @if(!$gallery->crop_use)
                if (file.size > BASIC.MBite) {
                    var translate = {"ru": "МБайт", "en": "Mb"};
                    $(form).find('#js-file-size').html((file.size / BASIC.MBite).toFixed(2) + ' ' + translate[BASIC.locale]);
                } else if (file.size < BASIC.MBite) {
                    var translate = {"ru": "кБайт", "en": "kb"};
                    $(form).find('#js-file-size').html((file.size / 1024).toFixed(2) + ' ' + translate[BASIC.locale]);
                } else {
                    var translate = {"ru": "Байт", "en": "b"};
                    $(form).find('#js-file-size').html((file.size / 1024) + ' ' + translate[BASIC.locale]);
                }
                $(form).find('.js-file-error').html('').addClass('hidden');
                $(form).find('button[type="submit"]').addClass('hidden');
                $(form).find(".js-file-preview img").remove();
                var valid = BASIC.validateUploadFile(file);
                if (valid.error === false) {
                    if (file.size > 10 * BASIC.MBite) {
                        var translate = {
                            "ru": "Внимание. Большой размер файла",
                            "en": "Attention. Large file size"
                        }
                        $(form).find('.js-file-error').html('<i class="fa fa-warning"></i>' + translate[BASIC.locale]).removeClass('hidden');
                        $(form).find('button[type="submit"]').removeClass('hidden');
                    } else {
                        $("#showUploadModalClick .btn-close").addClass('hidden');
                        $(form).find(".js-percent-complete").html('0%');
                        $(form).find(".progress-bar").css('width', '0%');
                        $(form).find(".js-upload-progress").removeClass('hidden');
                        $(form).find("input[name='original_name']").val(file.name);
                        $(form).find("input[name='mime_type']").val(file.type);
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $(".js-preview-container img").attr('src', e.target.result).css('max-width', '100%');
                            setImgSize($(".js-preview-container img").attr('src'));
                        }
                        reader.readAsDataURL(file);
                    }
                } else {
                    $(form).find('.js-file-error').html(valid.message[BASIC.locale]).removeClass('hidden');
                }
                @endif
            });
            $(".js-preview-cancel").click(function () {
                $(".cmsPreviewOverlay").hide();
            });
            $(".js-preview-save").click(function () {
                $(".cmsPreviewOverlay").hide();
                var form = $(".js-upload-form");
                $(form).find("input[name='alt']").val($("#js-preview-image-alt").val());
                $(form).find("input[name='title']").val($("#js-preview-image-title").val());
                $(form).find(".js-file-preview").append('<img alt="" src="' + $(".js-preview-container img").attr('src') + '" class="lv-img brd-0 h-70">');
                $("a[href='#showUploadModalClick']").click();
                setTimeout(function () {
                    $(form).find('button[type="submit"]').click();
                }, 1000);
            });
        })
    </script>
    <script>
        $(function () {
            $(".js-edit-item").click(function (event) {
                var item_id = $(this).parents('.js-item-container').data('element');
                var item_title = $(this).parents('.js-item-container').data('title');
                var item_alt = $(this).parents('.js-item-container').data('alt');
                $("#editPhotoLabels").find('form input[name="photo_id"]').val(item_id);
                $("#editPhotoLabels").find('form input[name="alt"]').val(item_alt).focus(1);
                $("#editPhotoLabels").find('form input[name="title"]').val(item_title).focus(1);
                $("#editPhotoLabels").show();
            });
            $(".js-edit-cancel").click(function () {
                $("#editPhotoLabels").hide();
            });
        });
    </script>
    <script>
        var nestableList = [];
        var list = $('.photo-nestable-list').sortable({
            group: 'photo-nestable-list',
            delay: 500,
            handle: 'i.zmdi-swap',
            onDrop: function ($item, container, _super) {
                nestableList = [];
                list.sortable("serialize").get();
                $.ajax({
                    url: '{!! route('core.galleries.photos_sortable', $gallery->id) !!}',
                    type: 'POST',
                    dataType: 'json',
                    data: {elements: nestableList.join(',')},
                    beforeSend: function () {
                    },
                    success: function (response, textStatus, xhr) {
                        if (response.status == true) {
                            BASIC.notify(null, response.responseText, 'bottom', 'center', 'zmdi zmdi-notifications-none zmdi-hc-fw', 'success', 'animated flipInX', 'animated flipOutX');
                        } else {
                            BASIC.notify(null, response.errorText, 'bottom', 'center', null, 'danger', 'animated flipInX', 'animated flipOutX');
                        }
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        BASIC.notify(null, xhr.responseText, 'bottom', 'center', null, 'danger', 'animated flipInX', 'animated flipOutX');
                    }
                });
                _super($item, container);
            },
            serialize: function (parent, children, isContainer) {
                if ($(parent).hasClass('nested-element')) {
                    nestableList.push($(parent).data('element'));
                }
            }
        });
    </script>
@stop