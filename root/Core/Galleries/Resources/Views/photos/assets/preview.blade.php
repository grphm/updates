<div class="cmsPreviewOverlay js-cmsPreviewOverlay" style="display: none;">
    <div class="popup">
        <div class="js-preview-container" style="text-align: center; background-color: #ececec">
            <img src=""/>
        </div>
        <div class="p-10 no-margin pull-left">
            <span class="js-preview-image-wight">0</span>x<span class="js-preview-image-height">0</span>
            <input class="p-5 l-h-n wp-40" placeholder="@lang('core_galleries_lang::photos.crop.title')" id="js-preview-image-title">
            <input class="m-l-5 p-5 l-h-n wp-40" placeholder="@lang('core_galleries_lang::photos.crop.alt_text')" id="js-preview-image-alt">
        </div>
        <div class="p-10 no-margin pull-right">
            <a class="js-preview-save btn btn-success" href="javascript:void(0);"><span>@lang('core_galleries_lang::photos.crop.select')</span></a>
            <a class="js-preview-cancel btn btn-danger" href="javascript:void(0);"><span>@lang('core_galleries_lang::photos.crop.cancel')</span></a>
        </div>
    </div>
</div>