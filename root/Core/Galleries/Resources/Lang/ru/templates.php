<?php

return [
	'templates' => 'Шаблоны галерей',
    'root_directory' => 'Корневой каталог: /home/Resources/Views',
    'sort_date_created' => 'Дата получения',
    'empty' => 'Список пустой',
    'edit' => 'Редактировать',
    'delete' => [
        'question' => 'Удалить шаблон',
        'confirmbuttontext' => 'Да, удалить',
        'cancelbuttontext' => 'Нет, я передумал',
        'submit' => 'Удалить',
    ],
    'empty' => 'Список пустой',
    'insert' => [
        'breadcrumb' => 'Добавить',
        'title' => 'Добавление шаблона',
        'form' => [
            'content' => 'Контент шаблона',
            'title' => 'Название',
            'title_help_description' => 'Например: Галерея на главной',
            'path' => 'Имя файла шаблона',
            'path_help_description' => 'Расширение .blade.php указывать не нужно!<br>Например: feedback',
            'submit' => 'Сохранить'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Редактировать',
        'title' => 'Редактирование шаблона',
        'form' => [
            'content' => 'Контент шаблона',
            'title' => 'Название',
            'title_help_description' => 'Например: Галерея на главной',
            'submit' => 'Сохранить'
        ]
    ]
];