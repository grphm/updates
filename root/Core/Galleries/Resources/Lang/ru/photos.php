<?php

return [
    'breadcrumb' => 'Изображения',
    'title' => 'Изображения',
    'select_all' => 'Выбрать все',
    'delete_selected' => [
        'question' => 'Удалить выбранные изображения?',
        'confirmbuttontext' => 'Да, удалить',
        'cancelbuttontext' => 'Нет, я передумал',
        'submit' => 'Удалить выбранные',
    ],
    'delete_all' => [
        'question' => 'Удалить все изображения?',
        'confirmbuttontext' => 'Да, удалить',
        'cancelbuttontext' => 'Нет, я передумал',
        'submit' => 'Удалить все',
    ],
    'edit' => 'Редактировать',
    'select' => 'Выбрать',
    'cancel_selection' => 'Отменить выбор',
    'delete' => [
        'question' => 'Удалить файл',
        'confirmbuttontext' => 'Да, удалить',
        'cancelbuttontext' => 'Нет, я передумал',
        'submit' => 'Удалить',
    ],
    'empty' => 'Список пустой',
    'crop' => [
        'title' => 'Название',
        'alt_text' => 'Alt текст',
        'select' => 'Выбрать',
        'cancel' => 'Отмена'
    ],
    'form' => [
        'size' => 'Размер',
        'type' => 'Тип',
        'upload' => 'Загрузить',
        'uploading' => 'Загрузка файла',
        'done' => 'выполнено',
    ],
    'modal_edit' => [
        'submit' => 'Сохранить',
        'cancel' => 'Отмена'
    ],
    'sizes' => ['mb' => 'Мбайт', 'kb' => 'kБайт', 'b' => 'байт', 'undefined' => 'Размер не определен'],
    'mime_undefined' => 'Mime-тип не определен'
];