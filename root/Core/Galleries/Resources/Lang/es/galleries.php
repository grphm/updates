<?php

return [
    'pictures' => 'Fotos',
    'edit' => 'Editar',
    'embed' => 'Código de inserción',
    'delete' => [
        'question' => 'Eliminar galería',
        'confirmbuttontext' => 'Sí, eliminar',
        'cancelbuttontext' => 'He cambiado de idea',
        'submit' => 'Eliminar',
    ],
    'missing' => 'Faltan imágenes',
    'empty' => 'Lista está vacía',
    'insert' => [
        'breadcrumb' => 'Añadir',
        'title' => 'Adición de la galería',
        'form' => [
            'title' => 'Título',
            'title_help_description' => 'Por ejemplo: Galería Principal',
            'template' => 'Galería de plantillas',
            'description' => 'Descripción',
            'slug' => 'Simbólico código',
            'slug_help_description' => 'Sólo latino personajes, guiones, guiones, al menos 5 caracteres. <br> Ejemplo: galería_principal',
            'crop_use' => 'El uso de poda',
            'aspect_ratio' => 'Relación de aspecto',
            'aspect_ratio_help_description' => 'Dejar en blanco si la imagen debe tener una relación de aspecto arbitraria',
            'crop_wight' => 'Ancho fijo en píxeles',
            'crop_wight_help_description' => 'Dejar en blanco si el ancho del área a recortar, debe tener un valor arbitrario',
            'crop_height' => 'Altura fija en píxeles',
            'crop_height_help_description' => 'Dejar en blanco si la altura de la zona a recortar, debe tener un valor arbitrario',
            'info' => 'Al especificar los valores & raquo; Relación de Aspecto & raquo; y ldquo; Ancho fijo (altura) & raquo; hacer los cálculos pertinentes de altura y la anchura',
            'resizable' => 'Cambiar el tamaño del área de recorte',
            'resizable_help_description' => 'Se permite cambiar el tamaño de la selección para recortar la imagen',
            'crop_wight_thumbnail' => 'Miniatura ancho fijo en píxeles',
            'crop_height_thumbnail' => 'Altura fija de la miniatura en píxeles',
            'submit' => 'Guardar'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Editar',
        'title' => 'Editar galería',
        'form' => [
            'title' => 'Título',
            'title_help_description' => 'Por ejemplo: Galería Principal',
            'template' => 'Galería de plantillas',
            'description' => 'Descripción',
            'slug' => 'Simbólico código',
            'slug_help_description' => 'Sólo latino personajes, guiones, guiones, al menos 5 caracteres. <br> Ejemplo: galería_principal',
            'crop_use' => 'El uso de poda',
            'aspect_ratio' => 'Relación de aspecto',
            'aspect_ratio_help_description' => 'Dejar en blanco si la imagen debe tener una relación de aspecto arbitraria',
            'crop_wight' => 'Ancho fijo en píxeles',
            'crop_wight_help_description' => 'Dejar en blanco si el ancho del área a recortar, debe tener un valor arbitrario',
            'crop_height' => 'Altura fija en píxeles',
            'crop_height_help_description' => 'Dejar en blanco si la altura de la zona a recortar, debe tener un valor arbitrario',
            'info' => 'Al especificar los valores & raquo; Relación de Aspecto & raquo; y ldquo; Ancho fijo (altura) & raquo; hacer los cálculos pertinentes de altura y la anchura',
            'resizable' => 'Cambiar el tamaño del área de recorte',
            'resizable_help_description' => 'Se permite cambiar el tamaño de la selección para recortar la imagen',
            'crop_wight_thumbnail' => 'Miniatura ancho fijo en píxeles',
            'crop_height_thumbnail' => 'Altura fija de la miniatura en píxeles',
            'submit' => 'Guardar'
        ]
    ]
];