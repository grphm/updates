<?php

return [
	'templates' => 'Templates of galleries',
    'root_directory' => 'Root directory: /home/Resources/Views',
    'sort_date_created' => 'Date received',
    'empty' => 'List is empty',
    'edit' => 'Edit',
    'delete' => [
        'question' => 'Delete template',
        'confirmbuttontext' => 'Yes, delete',
        'cancelbuttontext' => 'No, I change my mind',
        'submit' => 'Delete',
    ],
    'empty' => 'List is empty',
    'insert' => [
        'breadcrumb' => 'Add',
        'title' => 'Adding template',
        'form' => [
            'content' => 'Content template',
            'title' => 'Title',
            'title_help_description' => 'For example: Gallery on the main page',
            'path' => 'Filename template',
            'path_help_description' => 'Expanding .blade.php is not needed!<br> Example: feedback',
            'submit' => 'Save'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Edit',
        'title' => 'Edit template',
        'form' => [
            'content' => 'Content template',
            'title' => 'Title',
            'title_help_description' => 'For example: Gallery on the main page',
            'submit' => 'Save'
        ]
    ]
];