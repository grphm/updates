<?php
namespace STALKER_CMS\Core\Galleries\Http\Controllers;

use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Контроллер Валидация загрузки изображений в гелерию
 * Class GalleryValidatorController
 * @package STALKER_CMS\Core\Galleries\Http\Controllers
 */
class GalleryValidatorController extends ModuleController {

    /**
     * @var
     */
    private $error;
    /**
     * @var
     */
    private $message;
    /**
     * @var
     */
    private $uploaded_file;
    /**
     * @var
     */
    private $file_types;
    /**
     * @var
     */
    private $uploaded_type;

    /**
     * @param UploadedFile $uploaded_file
     * @return $this
     */
    public function init(UploadedFile $uploaded_file) {

        $this->error = FALSE;
        $this->message = '';
        $this->uploaded_type = NULL;
        $this->uploaded_file = $uploaded_file;
        $this->file_types = parent::getAllowedFileTypes();
        return $this;
    }

    /**
     * @return bool
     */
    public function passes() {

        $this->valid();
        return !$this->error ? TRUE : FALSE;
    }

    /**
     * @return $this
     */
    public function valid() {

        if ($this->uploaded_file->isValid()):
            $this->validMimeType();
            $this->validExtensions();
            $this->validSize();
        else:
            $this->error = TRUE;
            $this->message = $this->uploaded_file->getErrorMessage();
        endif;
        return $this;
    }

    /**
     * @param $mime_type
     * @return $this
     */
    public function setUploadedType($mime_type) {

        foreach ($this->file_types as $type):
            if ($mime_type == $this->uploaded_file->getClientMimeType()):
                $this->uploaded_type = $type;
            endif;
        endforeach;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMessage() {

        return $this->message;
    }

    /**
     * Валидация mime-типа
     * @return bool
     */
    private function validMimeType() {

        foreach ($this->file_types as $type):
            if ($type['mime'] == strtolower($this->uploaded_file->getClientMimeType())):
                $this->uploaded_type = $type;
                return TRUE;
            endif;
        endforeach;
        $this->error = TRUE;
        $this->message = \Lang::get('root_lang::validation.format_not_supported');
        return FALSE;
    }

    /**
     * Валидация разширения
     * @return bool
     */
    private function validExtensions() {

        if ($this->error || is_null($this->uploaded_type)):
            return FALSE;
        endif;
        foreach (explode('|', $this->uploaded_type['extensions']) as $extension):
            if ($extension == strtolower($this->uploaded_file->getClientOriginalExtension())):
                return TRUE;
            endif;
        endforeach;
        $this->error = TRUE;
        $this->message = \Lang::get('root_lang::validation.format_not_supported');
        return FALSE;
    }

    /**
     * Валидация размера
     * @return bool
     */
    private function validSize() {

        if ($this->error || is_null($this->uploaded_type)):
            return FALSE;
        endif;
        if ($this->uploaded_file->getClientSize() <= ($this->uploaded_type['max_size'] * 1048576)):
            return TRUE;
        endif;
        $this->error = TRUE;
        $this->message = \Lang::get('root_lang::validation.valid_size') . ': ' . $this->uploaded_type['max_size'] .
            \STALKER_CMS\Vendor\Helpers\array_translate(['ru' => 'Мбайт', 'ru' => 'Mb', 'es' => 'Mb']);
        return FALSE;
    }
}