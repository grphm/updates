<?php
\Route::group(['prefix' => 'admin', 'middleware' => 'secure'], function () {
    \Route::resource('galleries', 'GalleriesController',
        [
            'except' => ['show'],
            'names' => [
                'index' => 'core.galleries.index',
                'create' => 'core.galleries.create',
                'store' => 'core.galleries.store',
                'edit' => 'core.galleries.edit',
                'update' => 'core.galleries.update',
                'destroy' => 'core.galleries.destroy'
            ]
        ]
    );
    \Route::resource('galleries.photos', 'PhotosController',
        [
            'only' => ['index', 'store', 'destroy'],
            'names' => [
                'index' => 'core.galleries.photos_index',
                'store' => 'core.galleries.photos_store',
                'destroy' => 'core.galleries.photos_destroy'
            ]
        ]
    );
    \Route::resource('templates', 'TemplatesController',
        [
            'except' => ['show'],
            'names' => [
                'index' => 'core.galleries.templates.index',
                'create' => 'core.galleries.templates.create',
                'store' => 'core.galleries.templates.store',
                'edit' => 'core.galleries.templates.edit',
                'update' => 'core.galleries.templates.update',
                'destroy' => 'core.galleries.templates.destroy'
            ]
        ]
    );
    \Route::post('galleries/photos/labels/update', ['as' => 'core.galleries.photos_update', 'uses' => 'PhotosController@updateLabels']);
    \Route::delete('galleries/photos/{photo_id}/destroy', ['as' => 'core.galleries.photos_destroy', 'uses' => 'PhotosController@destroy']);
    \Route::post('galleries/photos/{photo_id}/sortable', ['as' => 'core.galleries.photos_sortable', 'uses' => 'PhotosController@sortable']);
});