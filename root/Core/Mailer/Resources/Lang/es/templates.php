<?php

return [
    'edit' => 'Editar',
    'delete' => [
        'question' => 'Eliminar plantilla',
        'confirmbuttontext' => 'Sí, eliminar',
        'cancelbuttontext' => 'He cambiado de idea',
        'submit' => 'Eliminar',
    ],
    'empty' => 'Lista está vacía',
    'insert' => [
        'breadcrumb' => 'Añadir',
        'title' => 'Adición de la plantilla',
        'form' => [
            'content' => 'Plantilla de contenido',
            'title' => 'Título',
            'title_help_description' => 'Por ejemplo: Realimentación',
            'path' => 'Nombre del archivo de plantilla',
            'path_help_description' => '.blade.php en expansión no es necesaria <br> Ejemplo: feedback',
            'submit' => 'Guardar'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Editar',
        'title' => 'Editar plantilla',
        'form' => [
            'content' => 'Plantilla de contenido',
            'title' => 'Título',
            'title_help_description' => 'Por ejemplo: Realimentación',
            'submit' => 'Guardar'
        ]
    ]
];