<?php

return [
    'templates' => 'Шаблоны писем',
    'root_directory' => 'Корневой каталог: /home/Resources/Mails',
    'sort_date_created' => 'Дата получения',
    'empty' => 'Список пустой',
];