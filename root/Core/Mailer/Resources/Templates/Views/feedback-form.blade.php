{!! Form::model(\Auth::user(), ['route' => 'core.mailer.feedback', 'class' => 'form-validate', 'id' => 'feedback-form']) !!}
<div class="form-group fg-float">
    <div class="fg-line">
        {!! Form::text('name', NULL, ['class'=>'input-sm form-control fg-input']) !!}
    </div>
    <label class="fg-label">@lang('site_lang::feedback.name')</label>
</div>
<div class="form-group fg-float">
    <div class="fg-line">
        {!! Form::email('email', NULL, ['class'=>'input-sm form-control fg-input']) !!}
    </div>
    <label class="fg-label">@lang('site_lang::feedback.email')</label>
</div>
<div class="form-group fg-float">
    <div class="fg-line">
        {!! Form::text('phone', NULL, ['class'=>'input-sm form-control fg-input']) !!}
    </div>
    <label class="fg-label">@lang('site_lang::feedback.phone')</label>
</div>
<div class="form-group fg-float">
    <div class="fg-line">
        {!! Form::textarea('message', NULL, ['class' => 'form-control auto-size fg-input', 'data-autosize-on' => 'true', 'rows' => 1]) !!}
    </div>
    <label class="fg-label">@lang('site_lang::feedback.message')</label>
</div>
<button type="submit" autocomplete="off" class="btn btn-primary btn-sm m-t-10 waves-effect">
    <i class="fa fa-save"></i>
    <span class="btn-text">@lang('site_lang::feedback.submit')</span>
</button>
{!! Form::close() !!}