<?php

namespace  STALKER_CMS\Core\Mailer\Providers;

use Illuminate\Support\Str;
use STALKER_CMS\Core\Mailer\Http\Controllers\PublicMailerController;
use STALKER_CMS\Vendor\Providers\ServiceProvider as BaseServiceProvider;

/**
 * Class ModuleServiceProvider
 * @package STALKER_CMS\Core\Mailer\Providers
 */
class ModuleServiceProvider extends BaseServiceProvider {

    /**
     * Метод загрузки
     */
    public function boot() {

        $this->setPath(base_path('home'));
        $this->registerMailsViews('mails_views');
        $this->registerMailsLocalization('mails_lang');
        $this->setPath(__DIR__ . '/../');
        $this->registerViews('core_mailer_views');
        $this->registerLocalization('core_mailer_lang');
        $this->registerConfig('core_mailer::config', 'Config/mailer.php');
        $this->registerSettings('core_mailer::settings', 'Config/settings.php');
        $this->registerActions('core_mailer::actions', 'Config/actions.php');
        $this->registerSystemMenu('core_mailer::menu', 'Config/menu.php');
        $this->registerBladeDirectives();
        $this->publishesTemplates();
        $this->publishesAssets();
    }

    /**
     * Метод регистрации
     */
    public function register() {

        \App::bind('PublicMailerController', function () {
            return new PublicMailerController();
        });
    }

    /********************************************************************************************************************/
    /**
     * Метод регистрации алиаса пути к шаблонам писем
     * @param $namespace
     */
    protected function registerMailsViews($namespace) {

        $this->loadViewsFrom($this->RealPath . '/Resources/Mails', $namespace);
    }

    /**
     * Метод регистрации алиаса пути к локализации писем
     * @param $namespace
     */
    protected function registerMailsLocalization($namespace) {

        $this->loadTranslationsFrom($this->RealPath . '/Resources/Lang', $namespace);
    }

    /**
     * Метод регистрации blade директивы
     */
    protected function registerBladeDirectives() {

        \Blade::directive('feedback', function ($expression) {
            if (Str::startsWith($expression, '(')):
                $expression = substr($expression, 1, -1);
            endif;
            if(\PermissionsController::isPackageEnabled('core_mailer')):
                if (!empty($expression)):
                    if (view()->exists("site_views::$expression")):
                        return "<?php echo \$__env->make('site_views::$expression')->render(); ?>";
                    endif;
                else:
                    if (view()->exists("site_views::feedback-form")):
                        return "<?php echo \$__env->make('site_views::feedback-form')->render(); ?>";
                    endif;
                endif;
            endif;
            return NULL;
        });
    }

    /**
     * Метод публикации шаблонов
     */
    public function publishesTemplates() {

        $this->publishes([
            __DIR__ . '/../Resources/Mails' => base_path('home/Resources/Mails'),
            __DIR__ . '/../Resources/Templates' => base_path('home/Resources')
        ]);
    }

    /**
     * Метод публикации ресурсов
     */
    public function publishesAssets() {

        $this->publishes([
            __DIR__ . '/../Resources/Assets' => public_path('packages/mailer'),
        ]);
    }
}
