<?php
namespace STALKER_CMS\Core\Mailer\Models;

use Carbon\Carbon;
use League\Flysystem\Exception;
use STALKER_CMS\Vendor\Interfaces\ModelInterface;
use STALKER_CMS\Vendor\Models\BaseModel;
use STALKER_CMS\Vendor\Traits\ModelTrait;

/**
 * Модель Входящее сообщение
 * Class MailInbox
 * @package STALKER_CMS\Core\Mailer\Models
 */
class MailInbox extends BaseModel implements ModelInterface {

    use ModelTrait;

    /**
     * @var string
     */
    protected $table = 'mailer_inbox';
    /**
     * @var array
     */
    protected $fillable = ['content', 'views', 'user_id'];
    /**
     * @var array
     */
    protected $hidden = [];
    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @param $request
     * @return $this
     */
    public function insert($request) {

        $this->content = $request::input('content');
        $this->views = 0;
        $this->user_id = $request::input('author');
        $this->created_at = Carbon::now();
        $this->updated_at = Carbon::now();
        $this->save();
        return $this;
    }

    /**
     * @param $id
     * @param $request
     * @return mixed
     */
    public function replace($id, $request) {

        $model = $this::whereFind(['id' => $id]);
        $model->views = TRUE;
        $model->updated_at = Carbon::now();
        $model->save();
        return $model;
    }

    /**
     * @param array|int $id
     * @return mixed
     */
    public function remove($id) {

        $instance = static::findOrFail($id);
        return $instance->delete();
    }

    /**
     * @param array $attributes
     */
    public function search(array $attributes) {
        // TODO: Implement search() method.
    }

    /**
     * @param array $attributes
     */
    public function filter(array $attributes) {
        // TODO: Implement filter() method.
    }

    /**
     * @param array $attributes
     */
    public function sort(array $attributes) {
        // TODO: Implement sort() method.
    }

    /**
     * Автор
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function author() {

        return $this->hasOne('\STALKER_CMS\Core\System\Models\User', 'id', 'user_id');
    }

    /**
     * Вернуть имя отправителя
     * @return null
     */
    public function getNameAttribute() {

        if (!empty($this->attributes['content'])):
            $content = json_decode($this->attributes['content'], TRUE);
            return isset($content['name']) ? $content['name'] : NULL;
        endif;
        return NULL;
    }

    /**
     * Вернуть email отправителя
     * @return null
     */
    public function getEmailAttribute() {

        if (!empty($this->attributes['content'])):
            $content = json_decode($this->attributes['content'], TRUE);
            return isset($content['email']) ? $content['email'] : NULL;
        endif;
        return NULL;
    }

    /**
     * Вернуть телефон отправителя
     * @return null
     */
    public function getPhoneAttribute() {

        if (!empty($this->attributes['content'])):
            $content = json_decode($this->attributes['content'], TRUE);
            return isset($content['phone']) ? $content['phone'] : NULL;
        endif;
        return NULL;
    }

    /**
     * Вернуть сообщение отправителя
     * @return null
     */
    public function getMessageAttribute() {

        if (!empty($this->attributes['content'])):
            $content = json_decode($this->attributes['content'], TRUE);
            return isset($content['message']) ? $content['message'] : NULL;
        endif;
        return NULL;
    }

    /**
     * Вернуть ссылку пользователя
     * @return null
     */
    public function getLinkAttribute() {

        if (!empty($this->attributes['content'])):
            $content = json_decode($this->attributes['content'], TRUE);
            return isset($content['link']) ? $content['link'] : NULL;
        endif;
        return NULL;
    }

    /**
     * Вернуть аватар отправителя
     * @return null
     */
    public function getAvatarAttribute() {

        if (!empty($this->author) && !empty($this->author->thumbnail)):
            return \STALKER_CMS\Vendor\Helpers\double_slash('uploads/' . $this->author->thumbnail);
        endif;
        return NULL;
    }

    /***************************************************************************************************************/
    /**
     * @return array
     */
    public static function getStoreRules() {

        return ['name' => 'required', 'email' => 'required|email', 'message' => 'required', 'timer' => 'integer|min:5'];
    }

    /**
     * @return array
     */
    public static function getUpdateRules() {

        return [];
    }
}