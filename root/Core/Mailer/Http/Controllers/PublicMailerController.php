<?php

namespace STALKER_CMS\Core\Mailer\Http\Controllers;

use Carbon\Carbon;
use \STALKER_CMS\Vendor\Helpers;
use Illuminate\Database\Eloquent\Collection;
use STALKER_CMS\Core\Mailer\Models\MailInbox;

/**
 * Контроллер для отправки сообщений в гостевом интерфейсе
 * Class PublicMailerController
 * @package STALKER_CMS\Core\Mailer\Http\Controllers
 */
class PublicMailerController extends ModuleController {

    /**
     * @var MailInbox
     */
    protected $model;

    /**
     * PublicMailerController constructor.
     */
    public function __construct() {

        $this->model = new MailInbox();
    }

    /**
     * Обработка формы обратной связи
     * @return \Illuminate\Http\JsonResponse
     */
    public function feedback() {

        $request = \RequestController::isAJAX()->init();
        if (\ValidatorController::passes($request, $this->model->getStoreRules())):
            $insert = [
                'content' => json_encode([
                    'name' => $request::input('name'),
                    'email' => $request::input('email'),
                    'phone' => $request::input('phone'),
                    'message' => $request::input('message'),
                ]),
                'author' => \Auth::check() ? \Auth::id() : NULL
            ];
            $request::merge($insert);
            $this->model->insert($request);
            $subject = Helpers\settings(['core_mailer', 'mailer', 'feedback_subject']);
            $emails = explode(',', Helpers\settings(['core_mailer', 'mailer', 'feedback_emails']));
            $this->send($request, $emails, $subject);
            return \ResponseController::success(1700)->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /**
     * Регистрация пользователя
     * @param \Request $request
     */
    public function register(\Request $request) {

        $subject = Helpers\array_translate(['ru' => 'Регистрация', 'en' => 'Registration', 'es' => 'Registro']);
        $this->send($request, $request::input('email'), $subject, 'registration');
    }

    /**
     * Возвращает новые сообщения или их количество
     * @param bool|FALSE $getCount
     * @return mixed
     */
    public function getNewMessages($getCount = FALSE) {

        return $getCount ?
            $this->model->whereViews(FALSE)->count() :
            $this->model->whereViews(FALSE)->orderBy('created_at', 'DESC')->with('author')->get();
    }

    /**
     * Возвращает просмотренные сообщения или их количество
     * @param bool|FALSE $getCount
     * @return mixed
     */
    public function getViewMessages($getCount = FALSE) {

        return $getCount ?
            $this->model->whereViews(TRUE)->count() :
            $this->model->whereViews(TRUE)->orderBy('created_at', 'DESC')->with('author')->get();
    }

    /**
     * Возвращает отправленные сообщения или их количество
     * @param bool|FALSE $getCount
     * @return mixed
     */
    public function getSendMessages($getCount = FALSE) {

        return $getCount ?
            $this->model->count() :
            $this->model->orderBy('created_at', 'DESC')->with('author')->get();
    }

    /**
     * Возвращает последние сообщения
     * @param int $getCount
     * @return mixed
     */
    public function getLastMessages($getCount = 5) {

        return $this->model->orderBy('created_at', 'DESC')->with('author')->take($getCount)->get();
    }

    /***************************************************************************/
    /**
     * Отправка сообщения
     * @param \Request $request
     * @param $emails
     * @param string $subject
     * @param string $template
     */
    public function send(\Request $request, $emails, $subject = 'NoName', $template = 'feedback') {

        \Mail::send('mails_views::' . $template, ['data' => $request::all()], function ($message) use ($subject, $emails) {
            $message->from(Helpers\settings(['core_mailer', 'mailer', 'from_email']), Helpers\settings(['core_mailer', 'mailer', 'from_name']));
            $message->to($emails)->subject($subject);
        });
    }
}