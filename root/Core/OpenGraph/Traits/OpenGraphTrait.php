<?php

namespace STALKER_CMS\Core\OpenGraph\Traits;

use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Class OpenGraphTrait
 * @package STALKER_CMS\Core\OpenGraph\Traits
 */
trait OpenGraphTrait {

    /**
     * Получить данные OpenGraph
     * @return string
     */
    public function getPageOpenGraphAttribute() {

        if (!empty($this->attributes['open_graph'])):
            return $this->attributes['open_graph'];
        else:
            return '';
        endif;
    }

    /**
     * Получить og:title
     * @return null
     */
    public function getOgTitleAttribute() {

        try {
            if (!empty($this->attributes['open_graph'])):
                return json_decode($this->attributes['open_graph'], TRUE)['og:title'];
            else:
                return NULL;
            endif;
        } catch (Exception $e) {
            return NULL;
        }
    }

    /**
     * Получить og:description
     * @return null
     */
    public function getOgDescriptionAttribute() {

        try {
            if (!empty($this->attributes['open_graph'])):
                return json_decode($this->attributes['open_graph'], TRUE)['og:description'];
            else:
                return NULL;
            endif;
        } catch (Exception $e) {
            return NULL;
        }
    }

    /**
     * Получить og:image
     * @return null
     */
    public function getOgImagesAttribute() {

        try {
            if (!empty($this->attributes['open_graph'])):
                return json_decode($this->attributes['open_graph'], TRUE)['og:image'];
            else:
                return NULL;
            endif;
        } catch (Exception $e) {
            return NULL;
        }
    }

    /**
     * Получить первое изображение из og:image
     * @return null|string
     */
    public function getOgImageFirstAttribute() {

        try {
            if (!empty($this->attributes['open_graph'])):
                if ($images = json_decode($this->attributes['open_graph'], TRUE)['og:image']):
                    return isset($images[0]) ? asset('uploads/pages/' . $images[0]) : NULL;
                endif;
            endif;
        } catch (Exception $e) {
            return NULL;
        }
        return NULL;
    }

    /**
     * Получить второе изображение из og:image
     * @return null|string
     */
    public function getOgImageSecondAttribute() {

        try {
            if (!empty($this->attributes['open_graph'])):
                if ($images = json_decode($this->attributes['open_graph'], TRUE)['og:image']):
                    return isset($images[1]) ? asset('uploads/pages/' . $images[1]) : NULL;
                endif;
            endif;
        } catch (Exception $e) {
            return NULL;
        }
        return NULL;
    }

    /**
     * Получить третье изображение из og:image
     * @return null|string
     */
    public function getOgImageThirdAttribute() {

        try {
            if (!empty($this->attributes['open_graph'])):
                if ($images = json_decode($this->attributes['open_graph'], TRUE)['og:image']):
                    return isset($images[2]) ? asset('uploads/pages/' . $images[2]) : NULL;
                endif;
            endif;
        } catch (Exception $e) {
            return NULL;
        }
        return NULL;
    }

}