<?php

return [
    'open_graph' => 'Meta tags Open Graph',
    'paste' => 'Fill',
    'open_graph_title' => 'Title',
    'open_graph_description' => 'Description',
    'open_graph_images' => 'Images',
    'open_graph_images_help_description' => 'Images only. Supported formats: png, jpg, gif',
    'open_graph_select' => 'Select',
    'open_graph_change' => 'Change',
    'open_graph_delete' => 'Delete'
];