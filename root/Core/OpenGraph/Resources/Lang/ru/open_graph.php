<?php

return [
    'open_graph' => 'Мета-теги Open Graph',
    'paste' => 'Заполнить',
    'open_graph_title' => 'Заголовок',
    'open_graph_description' => 'Описание',
    'open_graph_images' => 'Изображения',
    'open_graph_images_help_description' => 'Только изображения. Поддерживаемые форматы: png, jpg, gif',
    'open_graph_select' => 'Выбрать',
    'open_graph_change' => 'Изменить',
    'open_graph_delete' => 'Удалить'
];