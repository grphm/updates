<?php
namespace STALKER_CMS\Core\Uploads\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use PhpSpec\Exception\Exception;

class ModuleTableSeeder extends Seeder {

    public function run() {

        try {
            $file_types = [
                [
                    'title' => json_encode([
                        'ru' => 'Документ в формате PDF',
                        'en' => 'Document in PDF format',
                        'es' => 'Documento en formato PDF',
                        'fr' => 'Document en format PDF',
                        'uk' => 'Документ у форматі PDF'
                    ]),
                    'icon' => 'fa fa-file-pdf-o',
                    'extensions' => 'pdf',
                    'mime_type' => 'application/pdf|application/x-pdf|application/x-download|application/download|binary/octet-stream',
                    'max_size' => FALSE,
                    'enabled' => TRUE
                ],
                [
                    'title' => json_encode([
                        'ru' => 'Изображение в формате PNG',
                        'en' => 'PNG images',
                        'es' => 'Imágenes PNG',
                        'fr' => 'Les images PNG',
                        'uk' => 'Зображення у форматі PNG'
                    ]),
                    'icon' => 'fa fa-file-image-o',
                    'extensions' => 'png',
                    'mime_type' => 'image/png|image/x-png',
                    'max_size' => FALSE,
                    'enabled' => TRUE
                ],
                [
                    'title' => json_encode([
                        'ru' => 'Изображение в формате JPG',
                        'en' => 'JPG images',
                        'es' => 'Imágenes JPG',
                        'fr' => 'Les images JPG',
                        'uk' => 'Зображення у форматі JPG'
                    ]),
                    'icon' => 'fa fa-file-image-o',
                    'extensions' => 'jpg|jpeg|jpe',
                    'mime_type' => 'image/jpeg|image/pjpeg',
                    'max_size' => FALSE,
                    'enabled' => TRUE
                ],
                [
                    'title' => json_encode([
                        'ru' => 'Изображение в формате GIF',
                        'en' => 'GIF images',
                        'es' => 'Imágenes GIF',
                        'fr' => 'Les images GIF',
                        'uk' => 'Зображення у форматі GIF'
                    ]),
                    'icon' => 'fa fa-file-image-o',
                    'extensions' => 'gif',
                    'mime_type' => 'image/gif',
                    'max_size' => FALSE,
                    'enabled' => TRUE
                ],
                [
                    'title' => json_encode([
                        'ru' => 'Изображение в формате SVG',
                        'en' => 'SVG images',
                        'es' => 'Imágenes SVG',
                        'fr' => 'Les images SVG',
                        'uk' => 'Зображення у форматі SVG'
                    ]),
                    'icon' => 'fa fa-file-image-o',
                    'extensions' => 'svg',
                    'mime_type' => 'image/svg+xml',
                    'max_size' => FALSE,
                    'enabled' => TRUE
                ],
                [
                    'title' => json_encode([
                        'ru' => 'Документ в формате PSD',
                        'en' => 'Document in PSD format',
                        'es' => 'Documento en formato PSD',
                        'fr' => 'Document en format PSD',
                        'uk' => 'Документ у форматі PSD'
                    ]),
                    'icon' => 'fa fa-file-image-o',
                    'extensions' => 'psd',
                    'mime_type' => 'application/x-photoshop',
                    'max_size' => FALSE,
                    'enabled' => TRUE
                ],
                [
                    'title' => json_encode([
                        'ru' => 'Документ в формате MS Excel 2003',
                        'en' => 'Document in MS Excel format 2003',
                        'es' => 'Documento en formato MS Excel 2003',
                        'fr' => 'Document en format MS Excel 2003',
                        'uk' => 'Документ у форматі MS Excel 2003'
                    ]),
                    'icon' => 'fa fa-file-excel-o',
                    'extensions' => 'xls',
                    'mime_type' => 'application/excel|application/vnd.ms-excel|application/msexcel',
                    'max_size' => FALSE,
                    'enabled' => TRUE
                ],
                [
                    'title' => json_encode([
                        'ru' => 'Документ в формате MS Excel 2007',
                        'en' => 'Document in MS Excel format 2007',
                        'es' => 'Documento en formato MS Excel 2007',
                        'fr' => 'Document en format MS Excel 2007',
                        'uk' => 'Документ у форматі MS Excel 2007'
                    ]),
                    'icon' => 'fa fa-file-excel-o',
                    'extensions' => 'xlsx',
                    'mime_type' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                    'max_size' => FALSE,
                    'enabled' => TRUE
                ],
                [
                    'title' => json_encode([
                        'ru' => 'Документ в формате MS Word 2003',
                        'en' => 'Document in MS Word format 2003',
                        'es' => 'Documento en formato MS Word 2003',
                        'fr' => 'Document en format MS Word 2003',
                        'uk' => 'Документ у форматі MS Word 2003'
                    ]),
                    'icon' => 'fa fa-file-word-o',
                    'extensions' => 'doc',
                    'mime_type' => 'application/msword',
                    'max_size' => FALSE,
                    'enabled' => TRUE
                ],
                [
                    'title' => json_encode([
                        'ru' => 'Документ в формате MS Word 2007',
                        'en' => 'Document in MS Word format 2007',
                        'es' => 'Documento en formato MS Word 2007',
                        'fr' => 'Document en format MS Word 2007',
                        'uk' => 'Документ у форматі MS Word 2007'
                    ]),
                    'icon' => 'fa fa-file-word-o',
                    'extensions' => 'docx',
                    'mime_type' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                    'max_size' => FALSE,
                    'enabled' => TRUE
                ],
                [
                    'title' => json_encode([
                        'ru' => 'Документ в формате ZIP',
                        'en' => 'Document in ZIP format',
                        'es' => 'Documento en formato ZIP',
                        'fr' => 'Document en format ZIP',
                        'uk' => 'Документ у форматі ZIP'
                    ]),
                    'icon' => 'fa fa-file-zip-o',
                    'extensions' => 'zip',
                    'mime_type' => 'application/x-zip|application/zip|application/x-zip-compressed',
                    'max_size' => FALSE,
                    'enabled' => TRUE
                ],
                [
                    'title' => json_encode([
                        'ru' => 'Документ в формате MP3-Audio',
                        'en' => 'Document in MP3-Audio format',
                        'es' => 'Documento en formato MP3-Audio',
                        'fr' => 'Document en format MP3-Audio',
                        'uk' => 'Документ у форматі MP3-Audio'
                    ]),
                    'icon' => 'fa fa-file-audio-o',
                    'extensions' => 'mp3',
                    'mime_type' => 'audio/mpeg|audio/mpg|audio/mpeg3|audio/mp3',
                    'max_size' => FALSE,
                    'enabled' => TRUE
                ],
                [
                    'title' => json_encode([
                        'ru' => 'Документ в формате TXT',
                        'en' => 'Document in TXT format',
                        'es' => 'Documento en formato TXT',
                        'fr' => 'Document en format TXT',
                        'uk' => 'Документ у форматі TXT'
                    ]),
                    'icon' => 'fa fa-file-text-o',
                    'extensions' => 'txt',
                    'mime_type' => 'text/plain',
                    'max_size' => FALSE,
                    'enabled' => TRUE
                ],
                [
                    'title' => json_encode([
                        'ru' => 'Документ в формате CSV',
                        'en' => 'Document in CSV format',
                        'es' => 'Documento en formato CSV',
                        'fr' => 'Document en format CSV',
                        'uk' => 'Документ у форматі CSV'
                    ]),
                    'icon' => 'fa fa-file-code-o',
                    'extensions' => 'csv',
                    'mime_type' => 'text/csv|application/csv|text/x-csv',
                    'max_size' => FALSE,
                    'enabled' => TRUE
                ]
            ];

            foreach ($file_types as $type):
                \DB::table('file_types')->insert($type);
            endforeach;

        } catch (Exception $e) {

        }

    }
}