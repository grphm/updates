<?php
namespace STALKER_CMS\Core\Uploads\Http\Controllers;

use STALKER_CMS\Core\Uploads\Models\FileTypes;
use STALKER_CMS\Vendor\Interfaces\CrudInterface;

/**
 * Контроллер Типы файлов
 * Class UploadsTypesController
 * @package STALKER_CMS\Core\Uploads\Http\Controllers
 */
class UploadsTypesController extends ModuleController implements CrudInterface {

    /**
     * @var FileTypes
     */
    protected $model;

    /**
     * UploadsTypesController constructor.
     * @param FileTypes $fileTypes
     */
    public function __construct(FileTypes $fileTypes) {

        $this->model = $fileTypes;
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {

        \PermissionsController::allowPermission('core_uploads', 'types');
        return view('core_uploads_views::types.index', ['types' => $this->model->get()]);
    }

    /**
     *
     */
    public function create() {
        // TODO: Implement create() method.
    }

    /**
     *
     */
    public function store() {
        // TODO: Implement store() method.
    }

    /**
     * @param $id
     */
    public function edit($id) {
        // TODO: Implement edit() method.
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id) {

        \PermissionsController::allowPermission('core_uploads', 'types');
        $request = \RequestController::isAJAX()->init();
        $this->model->replace($id, $request);
        return \ResponseController::success(1204)->redirect(route('core.uploads.types.index'))->json();
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id) {

        \PermissionsController::allowPermission('core_uploads', 'types');
        $request = \RequestController::isAJAX()->init();
        $this->model->replace($id, $request);
        return \ResponseController::success(1205)->redirect(route('core.uploads.types.index'))->json();
    }
}