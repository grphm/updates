<?php
namespace STALKER_CMS\Core\Uploads\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use STALKER_CMS\Core\Uploads\Models\FileTypes;
use STALKER_CMS\Core\Uploads\Models\Upload;

use \STALKER_CMS\Vendor\Helpers as Helpers;
use STALKER_CMS\Vendor\Interfaces\CrudInterface;

/**
 * Загрузка файлов
 * Class UploadsController
 * @package STALKER_CMS\Core\Uploads\Http\Controllers
 */
class UploadsController extends ModuleController implements CrudInterface {

    /**
     * Модель
     * @var Upload
     */
    protected $model;

    /**
     * UploadsController constructor.
     * @param Upload $uploads
     */
    public function __construct(Upload $uploads) {

        $this->model = $uploads;
        $this->middleware('auth', ['except' => 'dynamicDownloadFile']);
    }

    /**
     * Список загружанных файлов
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {

        \PermissionsController::allowPermission('core_uploads', 'uploads');
        $request = \RequestController::init();
        $files = $this->model->with('type_file', 'package');
        if ($request::has('type')):
            $files = $files->where('file_type_id', $request::input('type'));
        endif;
        if ($request::has('sort_field') && $request::has('sort_direction')):
            foreach (explode(', ', $request::get('sort_field')) as $index):
                $files = $files->orderBy($index, $request::get('sort_direction'));
            endforeach;
        endif;
        if ($request::has('search')):
            $search = $request::get('search');
            $files = $files->where(function ($query) use ($search) {
                $query->where('original_name', 'like', '%' . $search . '%');
                $query->orWhere('mime_type', 'like', '%' . $search . '%');
            });
        endif;
        return view('core_uploads_views::files.index',
            [
                'file_types' => FileTypes::whereEnabled(TRUE)->get(),
                'files' => $files->orderBy('created_at', 'DESC')->paginate(25),
                'allowed_types' => self::getAllowedFileTypes()
            ]
        );
    }

    /**
     *
     */
    public function create() {
        // TODO: Implement create() method.
    }

    /**
     * Загрузка файла на сервер
     * @return \Illuminate\Http\JsonResponse
     */
    public function store() {

        \PermissionsController::allowPermission('core_uploads', 'uploads');
        $request = \RequestController::isAJAX()->init();
        $valid = \UploadValidatorController::init($request::file('file'));
        if ($valid->passes()):
            $upload_directory = Helpers\setDirectory(Helpers\settings(['core_uploads', 'uploads', 'uploads_dir']));
            $filePath = Helpers\double_slash($upload_directory . '/' . time() . "_" . rand(1000, 1999) . '.' . $request::file('file')->getClientOriginalExtension());
            \Storage::put($filePath, \File::get($request::file('file')));
            $request::merge([
                'file_type_id' => $this->getFileTypeID($request::file('file')->getClientMimeType()),
                'path' => Helpers\add_first_slash($filePath)
            ]);
            $this->model->insert($request);
            return \ResponseController::success(200)->redirect(route('core.uploads.index'))->json();
        else:
            return \ResponseController::error(0)->set('errorText', $valid->getMessage())->json();
        endif;
    }

    /**
     * @param $id
     */
    public function edit($id) {
        // TODO: Implement edit() method.
    }

    /**
     * @param $id
     */
    public function update($id) {
        // TODO: Implement update() method.
    }

    /**
     * Удаление файла
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id) {

        \PermissionsController::allowPermission('core_uploads', 'delete');
        $request = \RequestController::isAJAX()->init();
        if (is_numeric($id)):
            $this->deleteFile($this->model->whereId($id)->first());
            return \ResponseController::success(1203)->redirect(route('core.uploads.index'))->json();
        elseif (is_string($id) && $id == 'selected'):
            $this->deleteFiles($this->model->whereIn('id', $request::input('files'))->get());
            return \ResponseController::success(1203)->redirect(route('core.uploads.index'))->json();
        elseif (is_string($id) && $id == 'all'):
            $this->deleteFiles($this->model->all());
            return \ResponseController::success(1203)->redirect(route('core.uploads.index'))->json();
        endif;
        return \ResponseController::error(2503)->json();
    }

    /**
     * Загрузка файла по димамической ссылке
     * @param $crypt_id
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function dynamicDownloadFile($crypt_id) {

        try {
            if ($file_id = \Crypt::decrypt($crypt_id)):
                if ($file = Upload::whereId($file_id)->first()):
                    if ($file->ExistOnDisk):
                        return \Response::download(public_path('uploads' . $file->path), $file->original_name, ['content-type' => $file->mime_type]);
                    endif;
                endif;
            endif;
        } catch (Exception $e) {
            throw new Exception(\Lang::get('root_lang::codes.404'), 404);
        }
    }

    /**
     * Поиск ID типа файла по mime-типу
     * @param $mime_type
     * @return int
     */
    private function getFileTypeID($mime_type) {

        foreach (self::getAllowedFileTypes() as $type):
            if ($type['mime'] == strtolower($mime_type)):
                return $type['id'];
            endif;
        endforeach;
        return 0;
    }

    /************************************************************************************/
    /**
     * @param Upload $file
     * @throws \Exception
     */
    private function deleteFile(Upload $file) {

        if ($file->ExistOnDisk):
            \Storage::delete($file->path);
        endif;
        $file->delete();
    }

    /**
     * @param Collection $files
     */
    private function deleteFiles(Collection $files) {

        foreach ($files as $file):
            $this->deleteFile($file);
        endforeach;
    }
}