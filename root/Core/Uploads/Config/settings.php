<?php

return [
    'uploads' => [
        'title' => ['ru' => 'Загрузка', 'en' => 'Upload', 'es' => 'Populares subir'],
        'options' => [
            ['group_title' => ['ru' => 'Основные', 'en' => 'Main', 'es' => 'Los principales']],
            'upload_max_file_size' => [
                'title' => [
                    'ru' => 'Максимальный размер загружаемого файла',
                    'en' => 'Maximum upload size',
                    'es' => 'El tamaño máximo de subida'
                ],
                'note' => [
                    'ru' => 'Указать в МБайтах',
                    'en' => 'Specify in Mb',
                    'es' => 'Especifique en Mb'
                ],
                'type' => 'text',
                'value' => 5
            ],
            'uploads_dir' => [
                'title' => [
                    'ru' => 'Каталог для загрузки',
                    'en' => 'Catalog to uploads',
                    'es' => 'Catálogo de los archivos subidos'
                ],
                'note' => [
                    'ru' => 'Корневой каталог: /public/uploads',
                    'en' => 'Root directory: /public/uploads',
                    'es' => 'Directorio raíz: /public/uploads'
                ],
                'type' => 'text',
                'value' => 'files'
            ]
        ]
    ]
];