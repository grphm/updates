<?php

return [
    'package_name' => 'core_uploads',
    'package_title' => [
        'ru' => 'Модуль загрузки',
        'en' => 'Module uploads',
        'es' => 'Los archivos subidos módulo'
    ],
    'package_icon' => 'zmdi zmdi-upload',
    'version' => [
        'ver' => 1.0,
        'date' => '15.04.2016'
    ]
];
