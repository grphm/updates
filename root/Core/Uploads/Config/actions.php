<?php

return [
    'uploads' => [
        'title' => ['ru' => 'Просмотр', 'en' => 'View', 'es' => 'Ver'],
        'enabled' => FALSE,
        'icon' => 'zmdi zmdi-eye'
    ],
    'upload' => [
        'title' => ['ru' => 'Загрузка', 'en' => 'Upload', 'es' => 'Populares subir'],
        'enabled' => FALSE,
        'icon' => 'zmdi zmdi-upload'
    ],
    'delete' => [
        'title' => ['ru' => 'Удаление', 'en' => 'Delete', 'es' => 'Eliminar'],
        'enabled' => FALSE,
        'icon' => 'zmdi zmdi-delete'
    ],
    'types' => [
        'title' => ['ru' => 'Типы файлов', 'en' => 'File types', 'es' => 'Tipos de archivo'],
        'enabled' => FALSE,
        'icon' => 'zmdi zmdi-collection-item'
    ]
];