<?php

return [
    'mime_type' => 'Mime-tipo',
    'search' => 'Introduzca el nombre de archivo o mime-tipo',
    'sort_title' => 'Título',
    'sort_size' => 'El tamaño',
    'sort_date_uploaded' => 'Fecha subido',
    'all_types' => 'Todos los tipos',
    'select_all' => 'Seleccionar todo',
    'delete_selected' => [
        'question' => 'Eliminar archivos seleccionados?',
        'confirmbuttontext' => 'Sí, eliminar',
        'cancelbuttontext' => 'He cambiado de idea',
        'submit' => 'Eliminar seleccionado',
    ],
    'delete_all' => [
        'question' => 'Elimine todos los archivos?',
        'confirmbuttontext' => 'Sí, eliminar',
        'cancelbuttontext' => 'He cambiado de idea',
        'submit' => 'Eliminar todos',
    ],
    'uploaded' => 'Subido',
    'size' => 'El tamaño',
    'missing' => 'Archivo desaparecidos',
    'direct_relative_link' => 'Vínculo familiar directo',
    'direct_absolute_link' => 'Vínculo absoluto directo',
    'dynamic_relative_link' => 'Vínculo relativo dinámico',
    'dynamic_absolute_link' => 'Vínculo absoluto dinámica',
    'download' => 'Descargar archivo',
    'delete' => [
        'question' => 'Eliminar archivo',
        'confirmbuttontext' => 'Sí, eliminar',
        'cancelbuttontext' => 'He cambiado de idea',
        'submit' => 'Eliminar',
    ],
    'empty' => 'Lista está vacía',
    'form' => [
        'size' => 'El tamaño',
        'type' => 'Tipo',
        'upload' => 'Subir',
        'uploading' => 'Cargar archivos',
        'done' => 'hecho'
    ],
    'sizes' => ['mb' => 'Mb', 'kb' => 'kb', 'b' => 'b', 'undefined' => 'El tamaño no se define'],
    'mime_undefined' => 'Mime-tipo no está definido'
];