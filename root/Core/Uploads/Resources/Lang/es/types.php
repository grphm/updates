<?php

return [
    'mime_type' => 'Mime-tipo',
    'disabled' => [
        'question' => 'Upload desactivar',
        'confirmbuttontext' => 'Sí, desactivar',
        'cancelbuttontext' => 'He cambiado de idea',
        'submit' => 'Discapacitados'
    ],
    'enabled' => [
        'question' => 'Habilitar upload',
        'confirmbuttontext' => 'Sí, permitir',
        'cancelbuttontext' => 'He cambiado de idea',
        'submit' => 'Activar'
    ]
];