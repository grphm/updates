<?php

return [
    'mime_type' => 'Mime-тип',
    'search' => 'Введите название файла или Mime-тип',
    'sort_title' => 'Название',
    'sort_size' => 'Размер',
    'sort_date_uploaded' => 'Дата загрузки',
    'all_types' => 'Все типы',
    'select_all' => 'Выбрать все',
    'delete_selected' => [
        'question' => 'Удалить выбранные файлы?',
        'confirmbuttontext' => 'Да, удалить',
        'cancelbuttontext' => 'Нет, я передумал',
        'submit' => 'Удалить выбранные',
    ],
    'delete_all' => [
        'question' => 'Удалить все файлы?',
        'confirmbuttontext' => 'Да, удалить',
        'cancelbuttontext' => 'Нет, я передумал',
        'submit' => 'Удалить все',
    ],
    'uploaded' => 'Загружен',
    'size' => 'Размер',
    'missing' => 'Файл отсутствует',
    'direct_relative_link' => 'Прямая относительная ссылка',
    'direct_absolute_link' => 'Прямая абсолютная ссылка',
    'dynamic_relative_link' => 'Динамическая относительная ссылка',
    'dynamic_absolute_link' => 'Динамическая абсолютная ссылка',
    'download' => 'Скачать файл',
    'delete' => [
        'question' => 'Удалить файл',
        'confirmbuttontext' => 'Да, удалить',
        'cancelbuttontext' => 'Нет, я передумал',
        'submit' => 'Удалить',
    ],
    'empty' => 'Список пустой',
    'form' => [
        'size' => 'Размер',
        'type' => 'Тип',
        'upload' => 'Загрузить',
        'uploading' => 'Загрузка файла',
        'done' => 'выполнено',
    ],
    'sizes' => ['mb' => 'Мбайт', 'kb' => 'kБайт', 'b' => 'байт', 'undefined' => 'Размер не определен'],
    'mime_undefined' => 'Mime-тип не определен'
];