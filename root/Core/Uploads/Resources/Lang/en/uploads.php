<?php

return [
    'mime_type' => 'Mime-type',
    'search' => 'Enter the filename or mime-type',
    'sort_title' => 'Title',
    'sort_size' => 'Size',
    'sort_date_uploaded' => 'Date uploaded',
    'all_types' => 'All types',
    'select_all' => 'Select all',
    'delete_selected' => [
        'question' => 'Delete selected files?',
        'confirmbuttontext' => 'Yes, delete',
        'cancelbuttontext' => 'No, I change my mind',
        'submit' => 'Delete selected',
    ],
    'delete_all' => [
        'question' => 'Delete all files?',
        'confirmbuttontext' => 'Yes, delete',
        'cancelbuttontext' => 'No, I change my mind',
        'submit' => 'Delete all',
    ],
    'uploaded' => 'Uploaded',
    'size' => 'Size',
    'missing' => 'File missing',
    'direct_relative_link' => 'Direct relative link',
    'direct_absolute_link' => 'Direct absolute link',
    'dynamic_relative_link' => 'Dynamic relative link',
    'dynamic_absolute_link' => 'Dynamic absolute link',
    'download' => 'Download file',
    'delete' => [
        'question' => 'Delete file',
        'confirmbuttontext' => 'Yes, delete',
        'cancelbuttontext' => 'No, I change my mind',
        'submit' => 'Delete',
    ],
    'empty' => 'List is empty',
    'form' => [
        'size' => 'Size',
        'type' => 'Type',
        'upload' => 'Upload',
        'uploading' => 'Uploading file',
        'done' => 'done'
    ],
    'sizes' => ['mb' => 'Mb', 'kb' => 'kb', 'b' => 'b', 'undefined' => 'Size is not defined'],
    'mime_undefined' => 'Mime-type is not defined'
];