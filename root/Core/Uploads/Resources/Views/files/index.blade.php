@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb" style="margin-bottom: 5px;">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('core_uploads::menu.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_uploads::menu.title')) !!}
        </li>
        <li class="active">
            <i class="{{ config('core_uploads::menu.menu_child.uploads.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_uploads::menu.menu_child.uploads.title')) !!}
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="{{ config('core_uploads::menu.menu_child.uploads.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_uploads::menu.menu_child.uploads.title')) !!}
        </h2>
    </div>
    @if(\PermissionsController::allowPermission('core_uploads', 'upload', FALSE))
        <a class="btn bgm-deeppurple m-btn btn-float waves-effect waves-circle waves-float waves-effect waves-circle waves-float js-choice-upload-file">
            <i class="zmdi zmdi-upload"></i>
        </a>
    @endif
    <div class="card">
        <div class="lv-header-alt clearfix">
            <div class="lvh-search">
                {!! Form::open(['route' => 'core.uploads.index', 'method' => 'get']) !!}
                <input type="text" name="search" placeholder="@lang('core_uploads_lang::uploads.search')"
                       class="lvhs-input">
                <i class="lvh-search-close">&times;</i>
                {!! Form::close() !!}
            </div>
            <ul class="lv-actions actions">
                <li>
                    <a href="" class="lvh-search-trigger">
                        <i class="zmdi zmdi-search"></i>
                    </a>
                </li>
                @if($files->count())
                    <li class="dropdown">
                        <a href="" data-toggle="dropdown" aria-expanded="false" aria-haspopup="true">
                            <i class="zmdi zmdi-sort-amount-asc"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li>
                                <a href="{{ route('core.uploads.index', array_merge(Request::all(), ['sort_field' => 'original_name', 'sort_direction' => 'asc'])) }}">
                                    @lang('core_uploads_lang::uploads.sort_title')
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('core.uploads.index', array_merge(Request::all(), ['sort_field' => 'file_size', 'sort_direction' => 'asc'])) }}">
                                    @lang('core_uploads_lang::uploads.sort_size')
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('core.uploads.index', array_merge(Request::all(), ['sort_field' => 'created_at', 'sort_direction' => 'asc'])) }}">
                                    @lang('core_uploads_lang::uploads.sort_date_uploaded')
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="" data-toggle="dropdown" aria-expanded="false" aria-haspopup="true">
                            <i class="zmdi zmdi-sort-amount-desc"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li>
                                <a href="{{ route('core.uploads.index', array_merge(Request::all(), ['sort_field' => 'original_name', 'sort_direction' => 'desc'])) }}">
                                    @lang('core_uploads_lang::uploads.sort_title')
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('core.uploads.index', array_merge(Request::all(), ['sort_field' => 'file_size', 'sort_direction' => 'desc'])) }}">
                                    @lang('core_uploads_lang::uploads.sort_size')
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('core.uploads.index', array_merge(Request::all(), ['sort_field' => 'created_at', 'sort_direction' => 'desc'])) }}">
                                    @lang('core_uploads_lang::uploads.sort_date_uploaded')
                                </a>
                            </li>
                        </ul>
                    </li>
                @endif
                <li class="dropdown">
                    <a aria-haspopup="true" aria-expanded="true" data-toggle="dropdown" href="">
                        <i class="zmdi zmdi-filter-list"></i>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li>
                            <a href="{{ route('core.uploads.index') }}">@lang('core_uploads_lang::uploads.all_types')</a>
                        </li>
                        <li class="divider"></li>
                        @foreach($file_types as $type)
                            <li>
                                <a href="{{ route('core.uploads.index') . '?type=' . $type->id }}">{{ $type->title }}</a>
                            </li>
                        @endforeach
                    </ul>
                </li>
                @if($files->count())
                    <li class="dropdown">
                        <a aria-expanded="true" data-toggle="dropdown" href="">
                            <i class="zmdi zmdi-more-vert"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li>
                                <a href="javascript:void(0);" class="js-check-all">
                                    <i class="zmdi zmdi-check-all zmdi-hc-fw"></i> @lang('core_uploads_lang::uploads.select_all')
                                </a>
                            </li>
                            @if(\PermissionsController::allowPermission('core_uploads', 'delete', FALSE))
                                <li class="divider"></li>
                                <li>
                                    {!! Form::open(['route' => ['core.uploads.destroy', 'selected'], 'class' => 'selected-delete-form', 'method' => 'DELETE']) !!}
                                    <button type="submit"
                                            class="form-confirm-warning btn-link pull-right c-red m-5"
                                            autocomplete="off"
                                            data-question="@lang('core_uploads_lang::uploads.delete_selected.question')"
                                            data-confirmbuttontext="@lang('core_uploads_lang::uploads.delete_selected.confirmbuttontext')"
                                            data-cancelbuttontext="@lang('core_uploads_lang::uploads.delete_selected.cancelbuttontext')">
                                        @lang('core_uploads_lang::uploads.delete_selected.submit')
                                    </button>
                                    {!! Form::close() !!}
                                </li>
                                <li>
                                    {!! Form::open(['route' => ['core.uploads.destroy', 'all'], 'class' => 'all-delete-form', 'method' => 'DELETE']) !!}
                                    <button type="submit"
                                            class="form-confirm-warning btn-link pull-right c-red m-5"
                                            autocomplete="off"
                                            data-question="@lang('core_uploads_lang::uploads.delete_all.question')"
                                            data-confirmbuttontext="@lang('core_uploads_lang::uploads.delete_all.confirmbuttontext')"
                                            data-cancelbuttontext="@lang('core_uploads_lang::uploads.delete_all.cancelbuttontext')">
                                        @lang('core_uploads_lang::uploads.delete_all.submit')
                                    </button>
                                    {!! Form::close() !!}
                                </li>
                            @endif
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
        <div class="card-body card-padding m-h-250">
            @if($files->count())
                <div class="listview lv-bordered lv-lg">
                    <div class="lv-body">
                        @foreach($files as $file)
                            <div class="js-item-container lv-item media">
                                <div class="checkbox pull-left">
                                    <label>
                                        {!! Form::checkbox('files[]', $file->id) !!}
                                        <i class="input-helper"></i>
                                    </label>
                                </div>
                                @if($file->ExistOnDisk && $file->isImage)
                                    <div class="pull-left lightbox clearfix">
                                        <div data-src="{{ $file->asset }}">
                                            <div class="lightbox-item p-item">
                                                <img alt="{{ $file->original_name }}" src="{{ $file->asset }}"
                                                     class="lv-img h-50 brd-0">
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div class="media-body">
                                    <div class="lv-title">{{ $file->original_name }}</div>
                                    <small class="lv-small">{!! $file->PackageIco !!} {{ $file->PackageTitle }}</small>
                                    <ul class="lv-attrs">
                                        @if($file->ExistOnDisk)
                                            <li>
                                                @lang('core_uploads_lang::uploads.uploaded'):
                                                {{ $file->CreatedDate }}</li>
                                            <li>
                                                @lang('core_uploads_lang::uploads.size'):
                                                {{ $file->UploadSize }}</li>
                                            <li>
                                                <i class="{{ $file->type_file->icon }}"></i> {{ $file->type_file->title }}
                                            </li>
                                        @else
                                            <li class="bgm-red c-white">
                                                <i class="zmdi zmdi-alert-triangle"></i>
                                                @lang('core_uploads_lang::uploads.missing')
                                            </li>
                                        @endif
                                    </ul>
                                    <div class="lv-actions actions dropdown">
                                        <a aria-expanded="true" data-toggle="dropdown" href="">
                                            <i class="zmdi zmdi-more-vert"></i>
                                        </a>
                                        @if($file->ExistOnDisk)
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li>
                                                    <a href="javascript:void(0);" class="js-copy-link"
                                                       data-clipboard-text="{{ $file->full_path }}">
                                                        @lang('core_uploads_lang::uploads.direct_relative_link')
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0);" class="js-copy-link"
                                                       data-clipboard-text="{{ $file->asset }}">
                                                        @lang('core_uploads_lang::uploads.direct_absolute_link')
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0);" class="js-copy-link"
                                                       data-clipboard-text="{{ route('core.uploads.file.download', Crypt::encrypt($file->id), FALSE) }}">
                                                        @lang('core_uploads_lang::uploads.dynamic_relative_link')
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0);" class="js-copy-link"
                                                       data-clipboard-text="{{ route('core.uploads.file.download', Crypt::encrypt($file->id)) }}">
                                                        @lang('core_uploads_lang::uploads.dynamic_absolute_link')
                                                    </a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="{{ route('core.uploads.file.download', Crypt::encrypt($file->id)) }}">
                                                        @lang('core_uploads_lang::uploads.download')
                                                    </a>
                                                </li>
                                                @if(\PermissionsController::allowPermission('core_uploads', 'delete', FALSE))
                                                    <li class="divider"></li>
                                                    <li>
                                                        {!! Form::open(['route' => ['core.uploads.destroy', $file->id], 'method' => 'DELETE']) !!}
                                                        <button type="submit"
                                                                class="form-confirm-warning btn-link pull-right c-red p-r-15"
                                                                autocomplete="off"
                                                                data-question="@lang('core_uploads_lang::uploads.delete.question') &laquo;{{ $file->original_name }}&raquo;?"
                                                                data-confirmbuttontext="@lang('core_uploads_lang::uploads.delete.confirmbuttontext')"
                                                                data-cancelbuttontext="@lang('core_uploads_lang::uploads.delete.cancelbuttontext')">
                                                            @lang('core_uploads_lang::uploads.delete.submit')
                                                        </button>
                                                        {!! Form::close() !!}
                                                    </li>
                                                @endif
                                            </ul>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    {!! $files->appends(['type' => \Request::input('type'), 'search' => \Request::input('search')])->render() !!}
                </div>
            @else
                <h2 class="f-16 c-gray">@lang('core_uploads_lang::uploads.empty')</h2>
            @endif
        </div>
    </div>
@stop
@section('modal')
    <a class="btn btn-default waves-effect hidden" href="#showUploadModalClick" data-toggle="modal"></a>
    <div class="modal fade" id="showUploadModalClick" data-backdrop="static" data-keyboard="false" tabindex="-1"
         role="dialog"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">
                        <button type="button" class="btn btn-link btn-close pull-right p-0 p-r-5" data-dismiss="modal">
                            <i class="zmdi zmdi-close"></i>
                        </button>
                    </h4>
                </div>
                <div class="modal-body">
                    @include('core_uploads_views::files.forms.upload')
                </div>
            </div>
        </div>
    </div>
@stop
@section('scripts_before')
    <script>
        var $_form_upload = $('.js-upload-form');
        $(".selected-delete-form button[type='submit']").click(function () {
            $(".selected-delete-form .js-dynamic-append").remove();
            $(".listview input[type='checkbox']:checked").each(function (index, element) {
                $(".selected-delete-form").append('<input type="hidden" class="js-dynamic-append" name="files[]" value="' + $(element).val() + '">');
            })
        });
    </script>
@stop
@section('scripts_after')
    <script>
        $(function () {
            @foreach($allowed_types as $type)
                BASIC.setAllowedFileType({!! json_encode($type) !!});
            @endforeach
            $(".js-choice-upload-file").click(function () {
                $("#js-upload-file").click();
            });
            $(".js-check-all").click(function () {
                $(".listview input[type='checkbox']").prop('checked', true);
                $(this).parents('li.dropdown').removeClass('open');
            });
            $(document).on("change", "#js-upload-file", function (event) {
                var form = event.target.form;
                var file = event.target.files[0];
                $(form).find(".js-upload-progress").addClass('hidden');
                $(form).find('#js-file-name').html(file.name);
                $(form).find('#js-file-type').html(BASIC.getFileType(file));
                if (file.size > BASIC.MBite) {
                    var translate = {"ru": "МБайт", "en": "Mb", "es": "Mb", "fr": "Mb", "uk": "МБайт"};
                    $(form).find('#js-file-size').html((file.size / BASIC.MBite).toFixed(2) + ' ' + translate[BASIC.locale]);
                } else if (file.size < BASIC.MBite) {
                    var translate = {"ru": "кБайт", "en": "kb", "es": "kb", "kb": "kb", "uk": "кБайт"};
                    $(form).find('#js-file-size').html((file.size / 1024).toFixed(2) + ' ' + translate[BASIC.locale]);
                } else {
                    var translate = {"ru": "Байт", "en": "b", "es": "b", "fr": "b", "uk": "Байт"};
                    $(form).find('#js-file-size').html((file.size / 1024) + ' ' + translate[BASIC.locale]);
                }
                $(form).find('.js-file-error').html('').addClass('hidden');
                $(form).find('button[type="submit"]').addClass('hidden');
                $(form).find(".js-file-preview img").remove();
                $("a[href='#showUploadModalClick']").click();
                if (BASIC.isImage(file)) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $(form).find(".js-file-preview").append('<img alt="" src="' + reader.result + '" class="lv-img brd-0 h-70">');
                    }
                    reader.readAsDataURL(file);
                }
                var valid = BASIC.validateUploadFile(file);
                if (valid.error === false) {
                    $("#showUploadModalClick .btn-close").addClass('hidden');
                    $(form).find(".js-percent-complete").html('0%');
                    $(form).find(".progress-bar").css('width', '0%');
                    $(form).find(".js-upload-progress").removeClass('hidden');
                    setTimeout(function () {
                        $(form).find('button[type="submit"]').click();
                    }, 1000);
                } else {
                    $(form).find('.js-file-error').html(valid.message[BASIC.locale]).removeClass('hidden');
                }
            });
        })
    </script>
@stop