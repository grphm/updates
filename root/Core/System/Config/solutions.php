<?php

return [
    'system' => [
        'title' => ['ru' => 'Система', 'en' => 'System', 'es' => 'Sistema'],
        'solutions' => [
            'solutions_bugnotify' => [
                'package_name' => 'solutions_bugnotify',
                'package_title' => ['ru' => 'Bug Notifier', 'en' => 'Bug Notifier', 'es' => 'Bug Notifier'],
                'package_icon' => 'zmdi zmdi-cake',
                'relations' => [],
                'composer' => '"grphm/stalker_bugnotify":"@dev"',
                'directory' => 'Bugnotify',
                'repository_group' => 'grphm',
                'repository_name' => 'stalker_bugnotify',
                'package_description' => [
                    'ru' => 'Bug Notifier for STALKER  CMS',
                    'en' => 'Bug Notifier for STALKER  CMS',
                    'es' => 'Bug Notifier for STALKER  CMS'
                ],
                'version' => [
                    'ver' => 0.1,
                    'date' => '14.03.2016'
                ]
            ]
        ]
    ],
    'content' => [
        'title' => ['ru' => 'Контент', 'en' => 'Content', 'es' => 'Contenido'],
        'solutions' => [
            'solutions_events' => [
                'package_name' => 'solutions_events',
                'package_title' => ['ru' => 'События', 'en' => 'Events', 'es' => 'Eventos'],
                'package_icon' => 'zmdi zmdi-drink',
                'relations' => ['core_content', 'core_galleries'],
                'composer' => '"grphm/stalker_events":"@dev"',
                'directory' => 'Events',
                'repository_group' => 'grphm',
                'repository_name' => 'stalker_events',
                'package_description' => [
                    'ru' => 'Позволяет управлять событиями. Используется модуль "Контент" и "Галереи"',
                    'en' => 'It allows you to manage events. Using the module "Content" and "Galleries"',
                    'es' => 'Se le permite gestionar eventos. Utilice el módulo de "Contenido" y "Galería"'
                ],
                'version' => [
                    'ver' => 1.0,
                    'date' => '10.05.2016'
                ]
            ],
            'solutions_news' => [
                'package_name' => 'solutions_news',
                'package_title' => ['ru' => 'Новости', 'en' => 'News', 'es' => 'Noticias'],
                'package_icon' => 'zmdi zmdi-radio',
                'relations' => ['core_content', 'core_galleries'],
                'composer' => '"grphm/stalker_news":"@dev"',
                'directory' => 'News',
                'repository_group' => 'grphm',
                'repository_name' => 'stalker_news',
                'package_description' => [
                    'ru' => 'Позволяет управлять новостями. Используется модуль "Контент" и "Галереи"',
                    'en' => 'It allows you to manage news. Using the module "Content" and "Galleries"',
                    'es' => 'Se le permite administrar las noticias. Utilice el módulo de "Contenido" y "Galería"'
                ],
                'version' => [
                    'ver' => 0.5,
                    'date' => '10.02.2016'
                ]
            ],
            'solutions_sliders' => [
                'package_name' => 'solutions_sliders',
                'package_title' => ['ru' => 'Слайды', 'en' => 'Sliders', 'es' => 'Diapositivas'],
                'package_icon' => 'zmdi zmdi-collection-case-play',
                'relations' => ['core_content'],
                'composer' => '"grphm/stalker_sliders":"@dev"',
                'directory' => 'Sliders',
                'repository_group' => 'grphm',
                'repository_name' => 'stalker_sliders',
                'package_description' => [
                    'ru' => 'Позволяет управлять слайдами. Используется модуль "Контент"',
                    'en' => 'It allows you to control sliders. Using the module "Content"',
                    'es' => 'Se le permite controlar de diapositivas. Utilice el módulo de "Contenido"'
                ],
                'version' => [
                    'ver' => 0.2,
                    'date' => '16.03.2016'
                ]
            ],
            'solutions_articles' => [
                'package_name' => 'solutions_articles',
                'package_title' => ['ru' => 'Статьи', 'en' => 'Articles', 'es' => 'Artículos'],
                'package_icon' => 'zmdi zmdi-receipt',
                'relations' => ['core_content'],
                'composer' => '"grphm/stalker_articles":"@dev"',
                'directory' => 'Articles',
                'repository_group' => 'grphm',
                'repository_name' => 'stalker_articles',
                'package_description' => [
                    'ru' => 'Позволяет управлять статьями. Используется модуль "Контент"',
                    'en' => 'It allows you to manage articles. Using the module "Content"',
                    'es' => 'Se le permite gestionar artículos. Utilice el módulo de "contenido" y "Galería"'
                ],
                'version' => [
                    'ver' => 1.0,
                    'date' => '17.06.2016'
                ]
            ]
        ]
    ],
    'social_networks' => [
        'title' => ['ru' => 'Социальные сети', 'en' => 'Social networks', 'es' => 'Redes sociales'],
        'solutions' => [
            'solutions_social_image_grabber' => [
                'package_name' => 'solutions_social_image_grabber',
                'package_title' => ['ru' => 'Сборщик изображений из соц.сетей', 'en' => 'Social Image Grabber', 'es' => 'Redes sociales colector'],
                'package_icon' => 'zmdi zmdi-vk',
                'relations' => [],
                'composer' => '"grphm/stalker_sig":"@dev"',
                'directory' => 'SocialImageGrabber',
                'repository_group' => 'grphm',
                'repository_name' => 'stalker_sig',
                'package_description' => [
                    'ru' => 'Позволяет собирать изображения из социальных сетей. Используются социальные сети: Вконтакте, Twitter',
                    'en' => 'It allows you to collect images of the social networks. Use social networks: Vkontakte, Twitter',
                    'es' => 'Se le permite recoger imágenes de las redes sociales. Utilizar las redes sociales: Vkontakte, Twitter'
                ],
                'version' => [
                    'ver' => 1.1,
                    'date' => '26.08.2016'
                ]
            ]
        ]
    ],
    'other' => [
        'title' => ['ru' => 'Другое', 'en' => 'Other', 'es' => 'Otro'],
        'solutions' => [
            'solutions_questions' => [
                'package_name' => 'solutions_questions',
                'package_title' => ['ru' => 'Вопросы', 'en' => 'Questions', 'es' => 'Preguntas'],
                'package_icon' => 'zmdi zmdi-comments',
                'relations' => [],
                'composer' => '"grphm/stalker_questions":"@dev"',
                'directory' => 'Questions',
                'repository_group' => 'grphm',
                'repository_name' => 'stalker_questions',
                'package_description' => [
                    'ru' => 'Позволяет создавать перечень вопросов',
                    'en' => 'It allows you to create a list of questions',
                    'es' => 'Se le permite crear una lista de preguntas'
                ],
                'version' => [
                    'ver' => 1.0,
                    'date' => '03.03.2016'
                ]
            ]
        ]
    ]
];