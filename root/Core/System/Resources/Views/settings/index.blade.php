@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb" style="margin-bottom: 5px;">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('core_system::menu.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_system::menu.title')) !!}
        </li>
        <li class="active">
            <i class="{{ config('core_system::menu.menu_child.settings.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_system::menu.menu_child.settings.title')) !!}
        </li>
    </ol>
@stop
@section('body-class')
@section('content')
    <div class="block-header">
        <h2>
            <i class="{{ config('core_system::menu.menu_child.settings.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_system::menu.menu_child.settings.title')) !!}
        </h2>
    </div>
    <div class="card">
        {!! Form::open(['url' => URL::route('core.system.settings.update'), 'class' => 'form-validate', 'id' => 'settings-form']) !!}
        <div role="tabpanel" class="tab">
            <div class="card-header ch-alt m-b-20">
                <ul class="tab-nav" role="tablist" data-tab-color="teal">
                    <?php $index = 0; ?>
                    @foreach($settings_values as $package_slug => $modules)
                        @foreach($modules as $module_name => $module)
                            @if(PermissionsController::allowPermission($package_slug, $module_name, FALSE) === FALSE)
                                @continue
                            @endif
                            <li class="{{ !$index++ ? 'active' : '' }}">
                                <a href="#module_{!! $module_name !!}" aria-controls="module_{{ $module_name }}"
                                   role="tab" data-toggle="tab">
                                    {!! \STALKER_CMS\Vendor\Helpers\array_translate($module['title']) !!}
                                </a>
                            </li>
                        @endforeach
                    @endforeach
                </ul>
                <ul class="actions">
                    <li class="dropdown">
                        <a data-toggle="dropdown" href="" aria-expanded="false">
                            <i class="zmdi zmdi-more-vert"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li>
                                <a href="{{ URL::route('core.system.settings.cache.create') }}">
                                    @lang('core_system_lang::settings.cache_settings')
                                </a>
                            </li>
                            <li>
                                <a href="{{ URL::route('core.system.settings.cache.clear') }}">
                                    @lang('core_system_lang::settings.cache_clear')
                                </a>
                            </li>
                            <li>
                                <a target="_blank" href="{{ URL::route('core.system.settings.php') }}">
                                    @lang('core_system_lang::settings.php_info')
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
                @BtnSave
            </div>
            <div class="card-body card-padding p-t-0">
                <div class="row">
                    <div class="tab-content p-0">
                        <?php $index = 0; ?>
                        @foreach($settings_values as $package_slug => $modules)
                            @foreach($modules as $module_name => $module)
                                @if(PermissionsController::allowPermission($package_slug, $module_name, FALSE) === FALSE)
                                    @continue
                                @endif
                                <div role="tabpanel" id="module_{{ $module_name }}"
                                     class="tab-pane {{ !$index++ ? 'active in' : '' }} animated fadeIn">
                                    @foreach($module['options'] as $field_name => $field)
                                        <section class="m-10">
                                            @if(isset($field['group_title']))
                                                <p class="c-black f-500">
                                                    {!! \STALKER_CMS\Vendor\Helpers\array_translate($field['group_title']) !!}
                                                </p>
                                            @elseif(is_array($field))
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        @if($field['type'] == 'checkbox')
                                                            <div class="form-group m-0">
                                                                <div class="checkbox">
                                                                    <label>
                                                                        {!! Form::checkbox($package_slug.'['.$module_name.']['.$field_name.']', TRUE, @$settings_values[$package_slug][$module_name]['options'][$field_name]['value'], ['autocomplete' => 'off']) !!}
                                                                        <i class="input-helper"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate($field['title']) !!}
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            @if(isset($field['note']))
                                                                <small class="help-description">
                                                                    {!! \STALKER_CMS\Vendor\Helpers\array_translate($field['note']) !!}
                                                                </small>
                                                            @endif
                                                        @elseif($field['type'] == 'text')
                                                            <div class="form-group fg-float">
                                                                <div class="fg-line">
                                                                    {!! Form::text($package_slug.'['.$module_name.']['.$field_name.']', @$settings_values[$package_slug][$module_name]['options'][$field_name]['value'], ['autocomplete' => 'off', 'class' => 'form-control']) !!}
                                                                </div>
                                                                <label class="fg-label">
                                                                    {!! \STALKER_CMS\Vendor\Helpers\array_translate($field['title']) !!}
                                                                </label>
                                                                @if(isset($field['note']))
                                                                    <small>
                                                                        {!! \STALKER_CMS\Vendor\Helpers\array_translate($field['note']) !!}
                                                                    </small>
                                                                @endif
                                                            </div>
														@elseif($field['type'] == 'textarea')
                                                            <div class="form-group fg-float">
                                                                <div class="fg-line">
                                                                    {!! Form::textarea($package_slug.'['.$module_name.']['.$field_name.']', @$settings_values[$package_slug][$module_name]['options'][$field_name]['value'], ['autocomplete' => 'off', 'class' => 'form-control']) !!}
                                                                </div>
                                                                <label class="fg-label">
                                                                    {!! \STALKER_CMS\Vendor\Helpers\array_translate($field['title']) !!}
                                                                </label>
                                                                @if(isset($field['note']))
                                                                    <small>
                                                                        {!! \STALKER_CMS\Vendor\Helpers\array_translate($field['note']) !!}
                                                                    </small>
                                                                @endif
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            @endif
                                        </section>
                                    @endforeach
                                </div>
                            @endforeach
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@stop
@section('scripts_before')
@stop
@section('scripts_after')
    <script>
        //$("#settings-form input[type='text']").SelectizeInput();
    </script>
@stop