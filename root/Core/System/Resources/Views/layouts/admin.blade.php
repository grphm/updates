<!DOCTYPE html>
<!--[if IE 9 ]>
<html class="ie9">
<![endif]-->
<head>
    @include('core_system_views::assets.head')
    @yield('head')
</head>
<body class="scroll-overflow @yield('body-class')">
@include('core_system_views::assets.header')
@yield('breadcrumb')
<section id="main">
    <aside id="sidebar">
        <div class="sidebar-inner c-overflow">
            <div class="profile-menu">
                <a href=""
                   @if(File::exists(public_path('theme/images/sidebar-logo.png')))style="background-image: url('{{ asset('theme/images/sidebar-logo.png') }}')"@endif>
                    <div class="profile-pic small-image">
                        @ProfileAvatar(Auth::user()->name, Auth::user()->avatar_thumbnail)
                    </div>
                    <div class="profile-info">
                        {!! Auth::user()->name !!}
                        <i class="zmdi zmdi-arrow-drop-down"></i>
                    </div>
                </a>
                <ul class="main-menu">
                    @if(\PermissionsController::allowPermission('core_system', 'users', FALSE))
                        <li>
                            <a href="{{ route('core.system.users.edit', Auth::user()->id) }}">
                                <i class="zmdi zmdi-account"></i> @lang('core_system_lang::dashboard.edit_profile')
                            </a>
                        </li>
                    @endif
                    @if(\PermissionsController::allowPermission('core_system', 'groups', FALSE))
                        <li>
                            <a href="{{ route('core.system.groups.accesses-index', Auth::user()->group_id) }}">
                                <i class="zmdi zmdi-lock-open"></i> @lang('core_system_lang::dashboard.edit_profile_permissions')
                            </a>
                        </li>
                    @endif
                </ul>
            </div>
            @include('core_system_views::assets.sidebar')
        </div>
    </aside>
    <section id="content">
        <div class="container">
            @yield('content')
            @yield('modal')
        </div>
    </section>
</section>
@include('root_views::assets.old-browser')
@preloader()
@include('core_system_views::assets.footer')
{!! Html::script('core/js/vendor.js') !!}
@yield('scripts_before')
{!! Html::script('core/js/main.js') !!}
@yield('scripts_after')
</body>
</html>