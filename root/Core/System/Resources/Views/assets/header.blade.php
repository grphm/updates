<header id="header">
    <ul class="header-inner">
        <li id="menu-trigger" data-trigger="#sidebar">
            <div class="line-wrap">
                <div class="line top"></div>
                <div class="line center"></div>
                <div class="line bottom"></div>
            </div>
        </li>
        <li class="logo hidden-xs">
            <a href="{{ URL::route('dashboard') }}">{!! config('app.application_name') !!}</a>
        </li>
        <li class="pull-right">
            <ul class="top-menu">
                <li id="toggle-width">
                    <div class="toggle-switch">
                        <input id="tw-switch" type="checkbox" hidden="hidden">
                        <label for="tw-switch" class="ts-helper"></label>
                    </div>
                </li>
                @if(\PermissionsController::allowPermission('core_mailer', 'mailer', FALSE))
                    @set($notifications, \PublicMailer::getNewMessages())
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="tm-message" href="">
                            <i class="tmn-counts">{{ $notifications->count() }}</i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-lg pull-right">
                            <div class="listview{{ $notifications->count() ? '' : ' empty' }}" id="notifications">
                                <div class="lv-header">
                                    @lang('core_system_lang::dashboard.header.notification')
                                </div>
                                <div class="lv-body">
                                    @if($notifications->count())
                                        @foreach($notifications as $index => $notification)
                                            @if($index >= 5)
                                                <a href="{!! route('core.mailer.index') !!}" class="lv-footer">
                                                    @lang('core_system_lang::dashboard.header.notification_view_all')
                                                </a>
                                                @break
                                            @endif
                                            <a class="lv-item" href="{!! route('core.mailer.index') !!}">
                                                <div class="media">
                                                    <div class="profile-pic small-image pull-left">
                                                        @ProfileAvatar($notification->name, $notification->avatar)
                                                    </div>
                                                    <div class="media-body">
                                                        <div class="lv-title">{{ $notification->name }}</div>
                                                        <small class="lv-small d-block">{{ \Illuminate\Support\Str::limit($notification->message, 75) }}</small>
                                                    </div>
                                                </div>
                                            </a>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </li>
                @endif
                <li class="dropdown">
                    <a data-toggle="dropdown" class="tm-settings" href=""></a>
                    <ul class="dropdown-menu dm-icon pull-right">
                        <li class="hidden-xs">
                            <a data-action="fullscreen" href="">
                                <i class="zmdi zmdi-fullscreen-alt"></i> @lang('core_system_lang::dashboard.header.settings.full_screen')
                            </a>
                        </li>
                        <li class="hidden-xs">
                            <a href="{!! route('cms.settings.index') !!}">
                                <i class="zmdi zmdi-settings"></i> @lang('core_system_lang::dashboard.header.settings.edit')
                            </a>
                        </li>
                        <li>
                            <a href="{!! route('auth.login.logout') !!}">
                                <i class="zmdi zmdi-power"></i> @lang('core_system_lang::dashboard.header.settings.logout')
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
        <li class="pull-right m-r-30 m-t-5">
            <ul class="top-menu">
                @if(\STALKER_CMS\Vendor\Helpers\settings(['core_system', 'settings', 'debug_mode']))
                    <li>
                        <span class="badge bgm-orange c-white f-16 p-5">@lang('core_system_lang::dashboard.header.debug_mode')</span>
                    </li>
                @endif
                @if(\STALKER_CMS\Vendor\Helpers\settings(['core_system', 'settings', 'services_mode']))
                    <li>
                        <span class="badge bgm-red c-white f-16 p-5">@lang('core_system_lang::dashboard.header.services_mode')</span>
                    </li>
                @endif
            </ul>
        </li>
    </ul>
</header>