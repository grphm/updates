{!! Html::meta(['charset' => 'utf-8', 'content' => NULL, 'name' => NULL]) !!}
{!! Html::meta(['http-equiv' => 'X-UA-Compatible', 'content' => 'IE=edge', 'name' => NULL]) !!}
{!! Html::meta(['name' => 'viewport', 'content' => 'width=device-width, initial-scale=1']) !!}
<title>@yield('title')</title>
<meta name="description" content="@yield('description')">
{!! Html::meta(['name' => 'csrf-token', 'content' => csrf_token()]) !!}
{!! Html::meta(['name' => 'locale', 'content' => \App::getLocale()]) !!}
{!! Html::meta(['name' => 'fallback_locale', 'content' => config('app.fallback_locale')]) !!}
@if(Session::has('status'))
    {!! Html::meta(['name' => 'flash-message', 'content' => trans('root_lang::codes.'.Session::get('status'))]) !!}
@endif
@if(\Request::has('status'))
    {!! Html::meta(['name' => 'flash-message', 'content' => trans('root_lang::codes.'.\Request::input('status'))]) !!}
@endif
{!! Html::style('core/css/vendor.css') !!}
{!! Html::style('core/css/main.css') !!}
@if(File::exists(public_path('theme/favicon.ico')))
    {!! Html::favicon('theme/favicon.ico') !!}
@else
    {!! Html::favicon('favicon.ico') !!}
@endif