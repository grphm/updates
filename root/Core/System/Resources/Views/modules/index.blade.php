@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb" style="margin-bottom: 5px;">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('core_system::menu.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_system::menu.title')) !!}
        </li>
        <li class="active">
            <i class="{{ config('core_system::menu.menu_child.modules.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_system::menu.menu_child.modules.title')) !!}
        </li>
    </ol>
@stop
@section('content')
    {!! Form::open(['url' => URL::route('core.system.modules.update'), 'class' => 'form-validate', 'id' => 'modules-form']) !!}
    <div class="block-header p-r-0">
        <h2>
            <i class="{{ config('core_system::menu.menu_child.modules.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_system::menu.menu_child.modules.title')) !!}
            @if(\File::exists(base_path('bootstrap/cache/config.php')))
                <span role="alert" class="alert alert-danger f-10 pull-right">
                    <i class="zmdi zmdi-alert-circle-o"></i>
                    @lang('core_system_lang::modules.clear_cache_notification_part1')
                    <a href="{!! route('core.system.settings.index') !!}">@lang('core_system_lang::modules.clear_cache_notification_part2')</a>
                </span>
            @endif
        </h2>
    </div>
    <div class="card">
        <div class="card-header ch-alt p-40">
            <ul class="lv-actions actions">
                <li class="dropdown">
                    <a aria-expanded="false" data-toggle="dropdown" href="">
                        <i class="zmdi zmdi-more-vert"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li>
                            <a href="javascript:void(0);" class="js-check-all">
                                <i class="zmdi zmdi-check-all zmdi-hc-fw"></i> @lang('core_system_lang::modules.select_all')
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="{!! route('core.system.modules.updates') !!}">
                                <i class="zmdi zmdi-refresh-sync"></i> @lang('core_system_lang::modules.check_updates')
                            </a>
                        </li>
                        @if(\STALKER_CMS\Vendor\Models\Packages::getNestedPackages()->count() > 1)
                            <li>
                                <a href="{{ route('core.system.modules.boot') }}">
                                    <i class="zmdi zmdi-time-countdown zmdi-hc-fw"></i> @lang('core_system_lang::modules.boot')
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>
            </ul>
            @BtnSave
        </div>
        <div class="card-body card-padding p-t-30">
            <div class="listview lv-bordered lv-lg">
                <?php $elements = 0; ?>
                @foreach($packages as $index => $package)
                    @if($package->required && !$package->enabled)
                        @continue
                    @endif
                    @if($elements % 3 == 0)
                        {!! '<div class="row">' !!}
                    @endif
                    <div class="col-sm-4">
                        <div class="card">
                            <div class="card-header {{ $package->enabled ? 'bgm-green' : 'bgm-bluegray' }}">
                                <h2 class="f-14">
                                    <i class="{{ config($package->slug.'::config.package_icon' ) }}"></i> {{ $package->title }}
                                </h2>
                                <ul class="actions">
                                    <li class="{{ !$package->enabled ? '' : 'hidden' }}">
                                        <div class="checkbox">
                                            <label>
                                                {!! Form::checkbox($package->slug, TRUE, $package->enabled, ['autocomplete' => 'off']) !!}
                                                <i class="input-helper b-white"></i>
                                            </label>
                                        </div>
                                    </li>
                                    @if($package->enabled)
                                        <li class="dropdown">
                                            <a href="" data-toggle="dropdown" aria-expanded="true">
                                                <i class="zmdi zmdi-more-vert c-white"></i>
                                            </a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li>
                                                    <a href="javascript:void(0);" class="module-information"
                                                       data-version="{!! number_format(config($package->slug.'::config.version.ver'), 1) !!}"
                                                       data-date="{!! config($package->slug.'::config.version.date') !!}">
                                                        @lang('core_system_lang::modules.information')
                                                    </a>
                                                </li>
                                                @if(!$package->required)
                                                    <li class="divider"></li>
                                                    <li>
                                                        <button data-action="{{ route('core.system.modules.disable') }}"
                                                                class="confirm-warning btn-link pull-right c-red p-r-15"
                                                                autocomplete="off"
                                                                data-method="delete"
                                                                data-params="package={{ $package->id }}"
                                                                data-question="@lang('core_system_lang::modules.disabled.question') &laquo;{{ $package->title }}&raquo;?"
                                                                data-confirmbuttontext="@lang('core_system_lang::modules.disabled.confirmbuttontext')"
                                                                data-cancelbuttontext="@lang('core_system_lang::modules.disabled.cancelbuttontext')">
                                                            @lang('core_system_lang::modules.disabled.submit')
                                                        </button>
                                                    </li>
                                                @endif
                                            </ul>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                            <div class="card-body card-padding m-h-100 p-t-5">
                                @if(!empty($package->description))
                                    <small>{{ $package->description }}</small>
                                @endif
                            </div>
                        </div>
                    </div>
                    <?php $elements++; ?>
                    @if($elements % 3 == 0)
                        {!! '</div>' !!}
                    @endif
                @endforeach
            </div>
        </div>
    </div>
    </div>
    {!! Form::close() !!}
@stop
@section('scripts_before')
@stop
@section('scripts_after')
    <script>
        $(".js-check-all").click(function () {
            $("#modules-form input[type='checkbox']").prop('checked', true);
            $(this).parents('li.dropdown').removeClass('open');
        });
        $(".module-information").click(function () {
            var version = $(this).data('version');
            var date = $(this).data('date');
            swal({
                title: 'Version: ' + version + ' by ' + date,
                type: "info",
                showCancelButton: false
            });
        });
    </script>
@stop