@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb" style="margin-bottom: 5px;">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('core_system::menu.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_system::menu.title')) !!}
        </li>
        <li class="active">
            <i class="{{ config('core_system::menu.menu_child.modules_solutions.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_system::menu.menu_child.modules_solutions.title')) !!}
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="{{ config('core_system::menu.menu_child.modules_solutions.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_system::menu.menu_child.modules_solutions.title')) !!}
        </h2>
    </div>
    <div class="card">
        <div role="tabpanel" class="tab">
            <div class="card-header ch-alt m-b-20">
                <ul class="tab-nav" role="tablist" data-tab-color="teal">
                    <?php $index = 0; ?>
                    @foreach($solutions_packages as $symbolic_code => $lists)
                        <li class="{{ !$index++ ? 'active' : '' }}">
                            <a href="#module_{!! $symbolic_code !!}" aria-controls="module_{!! $symbolic_code !!}"
                               role="tab" data-toggle="tab">
                                {!! \STALKER_CMS\Vendor\Helpers\array_translate($lists['title']) !!}
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="card-body card-padding p-t-0">
                <div class="row">
                    <div class="tab-content p-0">
                        <?php $index = 0; ?>
                        @foreach($solutions_packages as $symbolic_code => $lists)
                            <div role="tabpanel" id="module_{!! $symbolic_code !!}"
                                 class="tab-pane {{ !$index++ ? 'active in' : '' }} animated fadeIn">
                                @if(!empty($lists['solutions']))
                                    @foreach($lists['solutions'] as $package_name => $package)
                                        <div class="col-sm-4">
                                            <div class="card">
                                                <div class="card-header bgm-bluegray">
                                                    <h2 class="f-14">
                                                        {{ \STALKER_CMS\Vendor\Helpers\array_translate($package['package_title']) }}
                                                    </h2>
                                                    <div class="actions">
                                                        <a title="@lang('core_system_lang::solutions.install')"
                                                           data-action="{{ route('core.system.modules.solutions.enable') }}"
                                                           class="confirm-warning solution-install"
                                                           autocomplete="off"
                                                           data-method="post"
                                                           data-params="section={!! $symbolic_code !!}&solution={!! $package['package_name'] !!}"
                                                           data-question="@lang('core_system_lang::solutions.enable.question') &laquo;{{ \STALKER_CMS\Vendor\Helpers\array_translate($package['package_title']) }}&raquo;?"
                                                           data-confirmbuttontext="@lang('core_system_lang::solutions.enable.confirmbuttontext')"
                                                           data-cancelbuttontext="@lang('core_system_lang::solutions.enable.cancelbuttontext')">
                                                            <i class="zmdi zmdi-download c-white"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="card-body card-padding m-h-100 p-t-5">
                                                    <small>{{ \STALKER_CMS\Vendor\Helpers\array_translate($package['package_description']) }}</small>
                                                    <br><br>
                                                    {{ \STALKER_CMS\Vendor\Helpers\array_translate(['ru' => 'Версия', 'en'=>'Version', 'es'=>'Versión']) }}:
                                                    {!! number_format($package['version']['ver'], 1) !!}
                                                    {{ \STALKER_CMS\Vendor\Helpers\array_translate(['ru' => 'от', 'en'=>'from', 'es'=>'de']) }}:
                                                    {!! $package['version']['date'] !!}
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @else
                                    <h2 class="p-l-30 f-16 c-gray">@lang('core_system_lang::solutions.empty')</h2>
                                @endif
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop