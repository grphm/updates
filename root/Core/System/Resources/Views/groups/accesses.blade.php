@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb" style="margin-bottom: 5px;">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('core_system::menu.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_system::menu.title')) !!}
        </li>
        <li>
            <a href="{{ route('core.system.groups.index') }}">
                <i class="{{ config('core_system::menu.menu_child.groups.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_system::menu.menu_child.groups.title')) !!}
            </a>
        </li>
        <li class="active">
            <i class="zmdi zmdi-lock-open"></i> @lang('core_system_lang::groups.accesses.breadcrumb')
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2><i class="zmdi zmdi-lock-open"></i> @lang('core_system_lang::groups.accesses.breadcrumb')</h2>
    </div>
    {!! Form::open(['route' => ['core.system.groups.accesses-update', $group->id], 'class' => 'form-validate', 'id' => 'group-accesses-form', 'method' => 'PUT']) !!}
    <div class="card">

        <div class="card-header ch-alt p-30">
            <h2>@lang('core_system_lang::groups.accesses.access_group') &laquo;{{ $group->title }}&raquo; @lang('core_system_lang::groups.accesses.to_modules')</h2>
            <ul class="actions">
                <li>
                    <a href="" data-checked="0" class="toggle_all_checkboxes"><i class="zmdi zmdi-check-all"></i></a>
                </li>
                <li class="dropdown">
                    <a data-toggle="dropdown" href="" aria-expanded="false">
                        <i class="zmdi zmdi-more-vert"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        @foreach(\STALKER_CMS\Core\System\Models\Group::lists('title', 'id') as $id => $description)
                            <li>
                                <a href="{{ route('core.system.groups.accesses-index', $id) }}">
                                    {{ $description }}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </li>
            </ul>
            @BtnSave
        </div>
        <div class="card-body card-padding">
            <div class="panel-group" data-collapse-color="green" role="tablist"
                 aria-multiselectable="true">
                @foreach($actions as $package_slug => $package)
                    <div class="panel panel-collapse">
                        <div class="panel-heading" role="tab">
                            <h4 class="panel-title">
                                <div class="checkbox m-l-20 pull-left">
                                    <label>
                                        <?php $checked = TRUE; ?>
                                        @foreach($package['actions'] as $module_name => $module)
                                            @if($module['enabled'] == FALSE)
                                                <?php $checked = FALSE; ?>
                                                @break
                                            @endif
                                        @endforeach
                                        {!! Form::checkbox(NULL, TRUE, $checked, ['class' => 'toggle_package_checkboxes', 'data-package' => $package_slug, 'autocomplete' => 'off']) !!}
                                        <i class="input-helper"></i>
                                    </label>
                                </div>
                                <a data-toggle="collapse" data-parent="#accordionRed" href="#{{ $package_slug }}"
                                   aria-expanded="true">
                                    @if(!empty($package['info']['icon']))
                                        <i class="{{ $package['info']['icon'] }} p-r-5"></i>
                                    @endif
                                    {!! \STALKER_CMS\Vendor\Helpers\array_translate($package['info']['title']) !!}
                                </a>
                            </h4>
                        </div>
                        <div id="{{ $package_slug }}" class="collapse" role="tabpanel">
                            <div class="panel-body">
                                @foreach($package['actions'] as $module_name => $module)
                                    <div class="col-sm-4">
                                        <div class="checkbox">
                                            <label>
                                                {!! Form::checkbox($package_slug.'['.$module_name.']', TRUE, $module['enabled'], ['autocomplete' => 'off', 'data-package' => $package_slug]) !!}
                                                <i class="input-helper"></i>
                                            </label>
                                            @if(!empty($module['icon']))
                                                <i class="{{ $module['icon'] }}"></i>
                                            @endif
                                            {!! \STALKER_CMS\Vendor\Helpers\array_translate($module['title']) !!}
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@stop
@section('scripts_before')
@stop
@section('scripts_after')
    <script>
        $(function () {
            if ($(".card-body :checkbox:checked").length == $(".card-body :checkbox").length) {
                $(".toggle_all_checkboxes").attr('data-checked', 1);
            }
            $(".toggle_all_checkboxes").click(function (event) {
                event.preventDefault();
                if ($(this).attr('data-checked') == 0) {
                    $("#group-accesses-form input:checkbox").prop('checked', true);
                    $(this).attr('data-checked', 1);
                } else {
                    $("#group-accesses-form input:checkbox").prop('checked', false);
                    $(this).attr('data-checked', 0);
                }
            });
            $(".toggle_package_checkboxes").click(function () {
                var package = $(this).data('package');
                if ($(this).prop('checked')) {
                    $("#group-accesses-form input:checkbox[data-package='" + package + "']").prop('checked', true);
                } else {
                    $("#group-accesses-form input:checkbox[data-package='" + package + "']").prop('checked', false);
                }
            });
            $("#group-accesses-form input:checkbox").click(function () {
                var package = $(this).data('package');
                if ($(this).prop('checked')) {
                    if ($("#group-accesses-form input:checkbox[data-package='" + package + "']:checked").length == $("#group-accesses-form input:checkbox[data-package='" + package + "']").length - 1) {
                        $("#group-accesses-form .toggle_package_checkboxes[data-package='" + package + "']").prop('checked', true);
                    }
                } else {
                    $("#group-accesses-form .toggle_package_checkboxes[data-package='" + package + "']").prop('checked', false);
                }
            });
        });
    </script>
@stop