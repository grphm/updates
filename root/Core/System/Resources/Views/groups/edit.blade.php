@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb" style="margin-bottom: 5px;">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('core_system::menu.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_system::menu.title')) !!}
        </li>
        <li>
            <a href="{{ route('core.system.groups.index') }}">
                <i class="{{ config('core_system::menu.menu_child.groups.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_system::menu.menu_child.groups.title')) !!}
            </a>
        </li>
        <li class="active">
            <i class="zmdi zmdi-edit"></i> @lang('core_system_lang::groups.replace.breadcrumb')
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="zmdi zmdi-edit"></i> @lang('core_system_lang::groups.replace.title')
        </h2>
    </div>
    <div class="card">
        <div class="card-body card-padding">
            <div class="row">
                <div class="col-sm-6">
                    {!! Form::model($group, ['route' => ['core.system.groups.update', $group->id], 'class' => 'form-validate', 'id' => 'edit-group-form', 'method' => 'PUT']) !!}
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('title', NULL, ['class'=>'input-sm form-control fg-input']) !!}
                        </div>
                        <label class="fg-label">@lang('core_system_lang::groups.replace.form.title')</label>
                        <small class="help-description">
                            @lang('core_system_lang::groups.replace.form.title_help_description')
                        </small>
                    </div>
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('slug', NULL, ['class'=>'input-sm form-control fg-input']) !!}
                        </div>
                        <label class="fg-label">@lang('core_system_lang::groups.replace.form.slug')</label>
                        <small class="help-description">
                            @lang('core_system_lang::groups.replace.form.slug_help_description')
                        </small>
                    </div>
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('dashboard', NULL, ['class'=>'input-sm form-control fg-input']) !!}
                        </div>
                        <label class="fg-label">@lang('core_system_lang::groups.replace.form.dashboard')</label>
                        <small class="help-description">
                            @lang('core_system_lang::groups.replace.form.dashboard_help_description')
                        </small>
                    </div>
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('start_url', NULL, ['class'=>'input-sm form-control fg-input']) !!}
                        </div>
                        <label class="fg-label">@lang('core_system_lang::groups.replace.form.start_url')</label>
                        <small class="help-description">
                            @lang('core_system_lang::groups.replace.form.start_url_help_description')
                        </small>
                    </div>
                    <button type="submit" autocomplete="off" class="btn btn-primary btn-sm m-t-10 waves-effect">
                        <i class="fa fa-save"></i>
                        <span class="btn-text">@lang('core_system_lang::groups.replace.form.submit')</span>
                    </button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
@section('scripts_before')
@stop
@section('scripts_after')
@stop