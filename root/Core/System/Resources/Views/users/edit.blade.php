@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb" style="margin-bottom: 5px;">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('core_system::menu.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_system::menu.title')) !!}
        </li>
        <li>
            <a href="{{ route('core.system.users.index') }}">
                <i class="{{ config('core_system::menu.menu_child.users.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_system::menu.menu_child.users.title')) !!}
            </a>
        </li>
        <li class="active">
            <i class="zmdi zmdi-edit"></i> @lang('core_system_lang::users.replace.breadcrumb')
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="zmdi zmdi-edit"></i> @lang('core_system_lang::users.replace.title')
        </h2>
    </div>
    <div class="card">
        <div class="card-body card-padding">
            <div class="row">
                {!! Form::model($user, ['route' => ['core.system.users.update', $user->id], 'class' => 'form-validate', 'id' => 'edit-user-form', 'method' => 'PUT']) !!}
                <div class="col-sm-6">
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('name', NULL, ['class'=>'input-sm form-control fg-input']) !!}
                        </div>
                        <label class="fg-label">@lang('core_system_lang::users.replace.form.name')</label>
                    </div>
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::email('email', NULL, ['class'=>'input-sm form-control fg-input']) !!}
                        </div>
                        <label class="fg-label">@lang('core_system_lang::users.replace.form.email')</label>
                    </div>
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('login', NULL, ['class'=>'input-sm form-control fg-input']) !!}
                        </div>
                        <label class="fg-label">@lang('core_system_lang::users.replace.form.login')</label>
                    </div>
                    <div class="form-group">
                        {!! Form::select('group_id', $groups, NULL, ['class' => 'tag-select']) !!}
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group input-group fg-float input-group-help-block">
                                <div class="fg-line p-0 l-0 w-full">
                                    {!! Form::text('password', FALSE, ['class'=>'input-sm form-control fg-input', 'autocomplete' => 'off']) !!}
                                </div>
                                <label class="fg-label">@lang('core_system_lang::users.replace.form.password')</label>
                                <small class="help-description">@lang('core_system_lang::users.replace.form.password_help_description')</small>
                                <span class="input-group-addon last v-a-0">
                                    <button type="button" id="js-generate-pass"
                                            class="btn btn-primary btn-icon waves-effect waves-circle waves-float">
                                        <i class="zmdi zmdi-refresh f-16"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                                {!! Form::checkbox('active', TRUE, TRUE, ['autocomplete' => 'off']) !!}
                                <i class="input-helper"></i> @lang('core_system_lang::users.replace.form.access')
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 m-t-3">
                    <div class="form-group">
                        {!! Form::select('country', \STALKER_CMS\Vendor\Models\Countries::whereLocale(\App::getLocale())->pluck('title', 'slug'), NULL, ['class' => 'tag-select']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::select('city', \STALKER_CMS\Vendor\Models\Cities::whereLocale(\App::getLocale())->pluck('title', 'slug'), NULL, ['class' => 'tag-select']) !!}
                    </div>
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('address', NULL, ['class'=>'input-sm form-control fg-input']) !!}
                        </div>
                        <label class="fg-label">@lang('core_system_lang::users.replace.form.address')</label>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group fg-float">
                                <div class="fg-line">
                                    {!! Form::text('birth_date', \Carbon\Carbon::parse($user->birth_date)->format('d.m.Y'), ['class' => 'input-sm form-control fg-input date-picker date-mask text-center']) !!}
                                </div>
                                <label class="fg-label">@lang('core_system_lang::users.replace.form.birth_date')</label>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group fg-float">
                                <div class="fg-line">
                                    {!! Form::text('age', $user->age ? $user->age : '' , ['class' => 'input-sm form-control fg-input']) !!}
                                </div>
                                <label class="fg-label">@lang('core_system_lang::users.replace.form.age')</label>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group fg-float">
                                {!! Form::select('sex', \STALKER_CMS\Core\System\Models\User::getSex(), NULL, ['class' => 'selectpicker', 'autocomplete' => 'off']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group fg-float">
                                <div class="fg-line">
                                    {!! Form::text('phone', NULL, ['class' => 'input-sm form-control phone-mask fg-input']) !!}
                                </div>
                                <label class="fg-label">@lang('core_system_lang::users.replace.form.phone')</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group fg-float">
                                <div class="fg-line">
                                    {!! Form::text('skype', NULL, ['class' => 'input-sm form-control fg-input']) !!}
                                </div>
                                <label class="fg-label">@lang('core_system_lang::users.replace.form.skype')</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group fg-float">
                                <div class="fg-line">
                                    {!! Form::text('social[vk]', @$social['vk'], ['class' => 'input-sm form-control fg-input']) !!}
                                </div>
                                <label class="fg-label">@lang('core_system_lang::users.replace.form.vk')</label>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group fg-float">
                                <div class="fg-line">
                                    {!! Form::text('social[facebook]', @$social['facebook'], ['class' => 'input-sm form-control fg-input']) !!}
                                </div>
                                <label class="fg-label">@lang('core_system_lang::users.replace.form.facebook')</label>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group fg-float">
                                <div class="fg-line">
                                    {!! Form::text('social[instagram]', @$social['instagram'], ['class' => 'input-sm form-control fg-input']) !!}
                                </div>
                                <label class="fg-label">@lang('core_system_lang::users.replace.form.instagram')</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            @cropImage($user->avatar_thumbnail)
                        </div>
                        <div class="col-sm-8">
                            <div class="form-group fg-float">
                                <div class="fg-line">
                                    {!! Form::textarea('comment', NULL, ['class' => 'form-control auto-size fg-input', 'data-autosize-on' => 'true', 'rows' => 1]) !!}
                                </div>
                                <label class="fg-label">@lang('core_system_lang::users.replace.form.comment')</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <button type="submit" autocomplete="off" class="btn btn-primary btn-sm m-t-10 waves-effect">
                        <i class="fa fa-save"></i>
                        <span class="btn-text">@lang('core_system_lang::users.replace.form.submit')</span>
                    </button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop
@section('scripts_before')
@stop
@section('scripts_after')
    <script>
        $(function () {
            $("#js-generate-pass").click(function () {
                $("input[name='password']").str_random();
                $("input[name='password']").parent().addClass('fg-toggled');
                $("input[name='password']").parents('.input-group-help-block').removeClass('has-error').addClass('has-success').find('.help-block').remove();
            });
        });
    </script>
@stop