<?php

return [
    'users_count' => 'usuario|usuarios|usuarios',
    'permissions' => 'Permisos',
    'edit' => 'Editar',
    'symbolic_code' => 'Simbólico código',
    'delete' => [
        'question' => 'Eliminar grupo',
        'confirmbuttontext' => 'Sí, eliminar',
        'cancelbuttontext' => 'He cambiado de idea',
        'submit' => 'Eliminar'
    ],
    'insert' => [
        'breadcrumb' => 'Añadir',
        'title' => 'Adición de grupos de usuarios',
        'form' => [
            'title' => 'Título',
            'title_help_description' => 'Por ejemplo: administradores',
            'slug' => 'Simbólico código',
            'slug_help_description' => 'Sólo latino personajes, guiones, guiones, al menos 5 caracteres. <br> Ejemplo: admin',
            'new_interface' => 'Crear una interfaz independiente',
            'new_interface_help_description' => 'Crear una interfaz independiente para la cuenta personal del nuevo grupo',
            'submit' => 'Guardar'
        ],
        'javascript' => [
            'new_interface_path' => 'La ruta de acceso a la interfaz',
            'new_interface_error' => 'Debe especificar un grupo de códigos de caracteres'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Editar',
        'title' => 'Editar grupos de usuarios',
        'form' => [
            'title' => 'Título',
            'title_help_description' => 'Por ejemplo: administradores',
            'slug' => 'Simbólico código',
            'slug_help_description' => 'Sólo latino personajes, guiones, guiones, al menos 5 caracteres. <br> Ejemplo: admin',
            'dashboard' => 'Enlace a la interfaz de tablero',
            'dashboard_help_description' => 'Sólo latino personajes, guiones, guiones, al menos 5 caracteres. <br> Ejemplo: admin',
            'start_url' => 'Enlace a la interfaz página de inicio',
            'start_url_help_description' => 'Sólo latino personajes, guiones, guiones, al menos 5 caracteres. <br> Ejemplo: admin',
            'submit' => 'Guardar'
        ]
    ],
    'accesses' => [
        'breadcrumb' => 'Permisos',
        'title' => 'Permisos',
        'access_group' => 'Acceso grupo',
        'to_modules' => 'a los módulos',
    ]
];