<?php

return [
    'cache_settings' => 'Cache settings',
    'cache_clear' => 'Clear cache settings',
    'php_info' => 'PHP info',
    'main' => [
        'title' => 'Settings',
        'interface_language' => 'Interface language',
        'app_name' => 'Application name',
        'clear_cache_part1' => 'After changing the name of the application, clear your cache',
        'clear_cache_part2' => 'settings',
        'logo' => 'Application logo',
        'logo_help_description' => 'Supported only png format <br> Recommended dimensions 412x214 px',
        'logo_select' => 'Select',
        'logo_change' => 'Change',
        'logo_delete' => 'Delete',
        'favicon' => 'Application favicon',
        'favicon_help_description' => 'Supported only ico format. <a href="http://convertico.com/" target="_blank">Converter png to ico</a>',
        'favicon_select' => 'Select',
        'favicon_change' => 'Change',
        'submit' => 'Save'
    ]
];