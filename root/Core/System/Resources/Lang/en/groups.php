<?php

return [
    'users_count' => 'user|users|users',
    'permissions' => 'Permissions',
    'edit' => 'Edit',
    'symbolic_code' => 'Symbolic code',
    'delete' => [
        'question' => 'Delete group',
        'confirmbuttontext' => 'Yes, delete',
        'cancelbuttontext' => 'No, I change my mind',
        'submit' => 'Delete'
    ],
    'insert' => [
        'breadcrumb' => 'Add',
        'title' => 'Adding user groups',
        'form' => [
            'title' => 'Title',
            'title_help_description' => 'For example: Administrators',
            'slug' => 'Symbolic code',
            'slug_help_description' => 'Only latin characters, underscores, dashes, at least 5 characters. <br> Example: admin',
            'new_interface' => 'Create a separate interface',
            'new_interface_help_description' => 'Create a separate interface for the personal account of the new group',
            'submit' => 'Save'
        ],
        'javascript' => [
            'new_interface_path' => 'The path to the interface',
            'new_interface_error' => 'You must specify a character code group'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Edit',
        'title' => 'Edit user groups',
        'form' => [
            'title' => 'Title',
            'title_help_description' => 'For example: Administrators',
            'slug' => 'Symbolic code',
            'slug_help_description' => 'Only latin characters, underscores, dashes, at least 5 characters. <br> Example: admin',
            'dashboard' => 'Link to dashboard interface',
            'dashboard_help_description' => 'Only latin characters, underscores, dashes, at least 5 characters. <br> Example: admin',
            'start_url' => 'Link to the start page interface',
            'start_url_help_description' => 'Only latin characters, underscores, dashes, at least 5 characters. <br> Example: admin',
            'submit' => 'Save'
        ]
    ],
    'accesses' => [
        'breadcrumb' => 'Permissions',
        'title' => 'Permissions',
        'access_group' => 'Access group',
        'to_modules' => 'to modules',
    ]
];