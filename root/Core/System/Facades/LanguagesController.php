<?php
namespace STALKER_CMS\Core\System\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Фасад контроллера языков
 * Class LanguagesController
 * @package STALKER_CMS\Core\System\Facades
 */
class LanguagesController extends Facade {

    protected static function getFacadeAccessor() {

        return 'LanguagesController';
    }
}