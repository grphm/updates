<?php
namespace STALKER_CMS\Core\System\Models;

use STALKER_CMS\Vendor\Models\BaseModel;
use STALKER_CMS\Vendor\Traits\ModelTrait;

/**
 * Модель Настройки
 * Class Setting
 * @package STALKER_CMS\Core\System\Models
 */
class Setting extends BaseModel {

    /**
     * @var string
     */
    protected $table = 'settings';
    /**
     * @var array
     */
    protected $fillable = ['package', 'module', 'name', 'value', 'user_id'];
    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var array
     */
    public static $rules = [
        'package' => 'required', 'module' => 'required', 'name' => 'required', 'user_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user() {

        return $this->hasOne('\STALKER_CMS\Core\System\Models\User', 'id', 'user_id');
    }
}