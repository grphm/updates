<?php
namespace STALKER_CMS\Core\System\Models;

use STALKER_CMS\Vendor\Interfaces\ModelInterface;
use STALKER_CMS\Vendor\Models\BaseModel;
use STALKER_CMS\Vendor\Traits\ModelTrait;

/**
 * Модуль Доступы
 * Class Permission
 * @package STALKER_CMS\Core\System\Models
 */
class Permission extends BaseModel implements ModelInterface {

    use ModelTrait;

    /**
     * @var string
     */
    protected $table = 'permissions';
    /**
     * @var array
     */
    protected $fillable = ['group_id', 'module', 'action', 'status'];
    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var bool
     */
    public $timestamps = FALSE;

    /**
     * @param $request
     * @return $this
     */
    public function insert($request) {

        $this->group_id = $request['group_id'];
        $this->module = $request['module'];
        $this->action = $request['action'];
        $this->status = $request['status'];
        $this->save();
        return $this;
    }

    /**
     * @param $id
     * @param $request
     * @return mixed
     */
    public function replace($id, $request) {

        $model = $this::where($id)->first();
        $model->status = $request['status'];
        $model->save();
        return $model;
    }

    /**
     * @param array|int $id
     */
    public function remove($id) {
        // TODO: Implement remove() method.
    }

    /**
     * @param array $attributes
     */
    public function filter(array $attributes) {
        // TODO: Implement filter() method.
    }

    /**
     * @param array $attributes
     */
    public function sort(array $attributes) {
        // TODO: Implement sort() method.
    }

    /**
     * @param array $attributes
     */
    public function search(array $attributes) {
        // TODO: Implement search() method.
    }
}