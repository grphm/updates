<?php
namespace STALKER_CMS\Core\System\Models;

use Carbon\Carbon;
use Illuminate\Foundation\Auth\User as Authenticatable;
use STALKER_CMS\Vendor\Interfaces\ModelInterface;
use STALKER_CMS\Vendor\Traits\ModelTrait;

/**
 * Модель Пользователь
 * Class User
 * @package STALKER_CMS\Core\System\Models
 */
class User extends Authenticatable implements ModelInterface {

    use ModelTrait;
    /**
     * @var string
     */
    protected $table = 'users';
    /**
     * @var array
     */
    protected $fillable = ['group_id', 'locale', 'name', 'login', 'password', 'active'];
    /**
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];
    /**
     * @var array
     */
    protected $guarded = [];
    /**
     * @var array
     */
    protected $dates = ['birth_date', 'created_at', 'updated_at', 'disabled_at', 'last_login'];

    /**
     * @param $request
     * @return $this
     */
    public function insert($request) {

        $this->group_id = $request::input('group_id');
        $this->locale = \App::getLocale();
        $this->name = $request::input('name');
        $this->email = $request::input('email');
        $this->login = $request::input('login');
        $this->password = bcrypt($request::input('password'));
        $this->active = $request::has('active') ? TRUE : FALSE;
        $this->approve = $request::has('active') ? TRUE : FALSE;
        $this->country = $request::input('country');
        $this->city = $request::input('city');
        $this->address = $request::input('address');
        $this->birth_date = $this->getBirthDate($request::input('birth_date'));
        $this->age = $request::input('age');
        $this->sex = $request::input('sex');
        $this->phone = $request::input('phone');
        $this->skype = $request::input('skype');
        $this->social = json_encode($request::input('social'));
        $this->photo = $request::has('photo') ? $request::input('photo') : NULL;
        $this->thumbnail = $request::has('thumbnail') ? $request::input('thumbnail') : NULL;
        $this->comment = $request::input('comment');
        $this->save();
        return $this;
    }

    /**
     * @param $id
     * @param $request
     * @return mixed
     */
    public function replace($id, $request) {

        $model = $this::whereFind(['id' => $id]);
        $model->group_id = $request::input('group_id');
        $model->name = $request::input('name');
        $model->email = $request::input('email');
        $model->login = $request::input('login');
        if($request::input('password') != ''):
            $model->password = bcrypt($request::input('password'));
        endif;
        $model->active = $request::has('active') ? TRUE : FALSE;
        $model->country = $request::input('country');
        $model->city = $request::input('city');
        $model->address = $request::input('address');
        $model->birth_date = $this->getBirthDate($request::input('birth_date'));
        $model->age = $request::input('age');
        $model->sex = $request::input('sex');
        $model->phone = $request::input('phone');
        $model->skype = $request::input('skype');
        $model->social = json_encode($request::input('social'));
        $model->photo = $request::input('photo');
        $model->thumbnail = $request::input('thumbnail');
        $model->comment = $request::input('comment');
        $model->save();
        $model->touch();
        return $model;
    }

    /**
     * @param array|int $id
     */
    public function remove($id) {
        // TODO: Implement remove() method.
    }

    /**
     * @param array $attributes
     */
    public function search(array $attributes) {
        // TODO: Implement search() method.
    }

    /**
     * @param array $attributes
     */
    public function filter(array $attributes) {
        // TODO: Implement filter() method.
    }

    /**
     * @param array $attributes
     */
    public function sort(array $attributes) {
        // TODO: Implement sort() method.
    }

    /**
     * Группа
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function group() {

        return $this->hasOne('\STALKER_CMS\Core\System\Models\Group', 'id', 'group_id');
    }

    /**
     * Язык
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function language() {

        return $this->hasOne('\STALKER_CMS\Vendor\Models\Languages', 'id', 'language_id');
    }

    /**
     * Сессия
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function session() {

        return $this->hasOne('\STALKER_CMS\Core\System\Models\Sessions', 'user_id', 'id');
    }

    /**
     * Дата последнего входа
     * @return string
     */
    public function getLastLoginAttribute() {

        if($this->attributes['last_login']):
            \Carbon\Carbon::setLocale(\App::getLocale());
            return \Carbon\Carbon::parse($this->attributes['last_login'])->diffForHumans();
        else:
            return '<span class="c-red">'.\Lang::get('core_system_lang::users.not_logged').'</span >';
        endif;
    }

    /**
     * Статус аккаунта
     * @return string
     */
    public function getActiveStatusAttribute() {

        if($this->attributes['active']):
            return '<span class="c-green">'.\Lang::get('core_system_lang::users.active').'</span>';
        else:
            return '<span class="c-red">'.\Lang::get('core_system_lang::users.blocked_by').' '.\Carbon\Carbon::parse($this->attributes['disabled_at'])->format('d.m.Y').'</span>';
        endif;
    }

    /**
     * Проверка online
     * @return string
     */
    public function getIsOnlineAttribute() {

        if(is_object($this->session)):
            return (time() - 600) > $this->session->last_activity
                ? '<span class="c-red">'.\Lang::get('core_system_lang::users.offline').'</span>'
                : '<span class="c-green">'.\Lang::get('core_system_lang::users.online').'</span>';
        else:
            return '<span class="c-red">'.\Lang::get('core_system_lang::users.offline').'</span>';
        endif;
    }

    /**
     * Аватар пользователя
     * @return mixed
     */
    public function getAvatarAttribute() {

        if(!empty($this->attributes['photo'])):
            return \STALKER_CMS\Vendor\Helpers\double_slash('uploads/'.$this->attributes['photo']);
        endif;
    }

    /**
     * Миниатюра аватара пользователя
     * @return mixed
     */
    public function getAvatarThumbnailAttribute() {

        if(!empty($this->attributes['thumbnail'])):
            return \STALKER_CMS\Vendor\Helpers\double_slash('uploads/'.$this->attributes['thumbnail']);
        endif;
    }

    /***************************************************************************************************************/
    /**
     * @return array
     */
    public static function getAuthRules() {

        return ['login' => 'required|email', 'password' => 'required'];
    }

    /**
     * @return array
     */
    public static function getStoreRules() {

        return ['group_id' => 'required', 'name' => 'required', 'email' => 'email', 'login' => 'required', 'password' => 'required'];
    }

    /**
     * @return array
     */
    public static function getUpdateRules() {

        return ['group_id' => 'required', 'name' => 'required', 'email' => 'email', 'login' => 'required'];
    }

    /**
     * Пол
     * @return mixed
     */
    public static function getSex() {

        $sex = [
            'ru' => ['Женский', 'Мужской'],
            'en' => ['Female', 'Male'],
            'es' => ['Hembra', 'Masculino']
        ];
        return $sex[\App::getLocale()];
    }

    /**
     * Получени даты рождения
     * @param null $birth_date
     * @return null|string
     */
    private function getBirthDate($birth_date = NULL) {

        try {
            if(!empty($birth_date)):
                return Carbon::parse($birth_date)->format('Y-m-d 00:00:00');
            endif;
        } catch(\Exception $e) {
        }
        return NULL;
    }
}
