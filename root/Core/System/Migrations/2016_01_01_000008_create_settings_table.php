<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration {

    public function up() {

        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('package', 32)->nullable()->index();
            $table->string('module', 32)->nullable()->index();
            $table->string('name', 32)->nullable()->index();
            $table->text('value')->nullable();
            $table->integer('user_id', FALSE, TRUE)->default(0)->nullable();
            $table->timestamps();
        });
    }

    public function down() {

        Schema::dropIfExists('settings');
    }

}