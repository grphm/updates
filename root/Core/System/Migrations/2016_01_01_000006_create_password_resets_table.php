<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePasswordResetsTable extends Migration {

    public function up() {

        Schema::create('password_resets', function (Blueprint $table) {
            $table->string('email')->nullable()->index();
            $table->string('token')->nullable()->index();
            $table->timestamp('created_at');
        });
    }

    public function down() {

        Schema::drop('password_resets');
    }
}
