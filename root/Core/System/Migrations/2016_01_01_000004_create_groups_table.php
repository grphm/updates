<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupsTable extends Migration {

    public function up() {

        Schema::create('groups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug', 20)->unique()->nullable();
            $table->string('title', 255)->nullable();
            $table->string('dashboard', 50)->nullable();
            $table->string('start_url', 25)->nullable();
            $table->boolean('required', FALSE, TRUE)->default(0)->nullable();
            $table->timestamps();
        });
    }

    public function down() {

        Schema::dropIfExists('groups');
    }

}
