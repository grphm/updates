<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitiesTable extends Migration {

    public function up() {

        Schema::create('cities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug', 50)->nullable()->index();
            $table->string('locale', 10)->nullable()->index();
            $table->string('title', 100)->nullable();
        });
    }

    public function down() {

        Schema::dropIfExists('cities');
    }
}