<?php
###########################################################################################
#Роуты для модуля Настройки
###########################################################################################
\Route::group(['prefix' => 'admin/system/settings', 'middleware' => 'secure'], function () {
    \Route::get('/', ['as' => 'core.system.settings.index', 'uses' => 'SettingsController@index']);
    \Route::post('update', ['as' => 'core.system.settings.update', 'uses' => 'SettingsController@update']);
    \Route::get('/rebuild', ['as' => 'core.system.settings.rebuild', 'uses' => 'SettingsController@rebuild']);
    \Route::get('/cache', ['as' => 'core.system.settings.cache.create', 'uses' => 'SettingsController@createConfigCash']);
    \Route::get('/clear', ['as' => 'core.system.settings.cache.clear', 'uses' => 'SettingsController@clearConfigCash']);
    \Route::get('/phpInformation', ['as' => 'core.system.settings.php', 'uses' => 'SettingsController@phpInformation']);
});
\Route::group(['prefix' => 'admin', 'middleware' => 'secure'], function () {
    \Route::get('settings', ['as' => 'cms.settings.index', 'uses' => 'SettingsController@settings']);
    \Route::post('settings', ['as' => 'cms.settings.update', 'uses' => 'SettingsController@settingsStore']);
});
###########################################################################################
#Роуты для модуля Модули
###########################################################################################
\Route::group(['prefix' => 'admin/system/modules', 'middleware' => 'secure'], function () {
    \Route::get('/', ['as' => 'core.system.modules.index', 'uses' => 'ModulesController@index']);
    \Route::get('/update', ['as' => 'core.system.modules.updates', 'uses' => 'ModulesController@getUpdates']);
    \Route::get('/rebuild', ['as' => 'core.system.modules.rebuild', 'uses' => 'ModulesController@rebuild']);
    \Route::post('/enable', ['as' => 'core.system.modules.update', 'uses' => 'ModulesController@update']);
    \Route::delete('/disable', ['as' => 'core.system.modules.disable', 'uses' => 'ModulesController@destroy']);
    \Route::get('/boot', ['as' => 'core.system.modules.boot', 'uses' => 'ModulesController@boot']);
    \Route::post('/boot', ['as' => 'core.system.modules.boot.update', 'uses' => 'ModulesController@bootUpdate']);
});
###########################################################################################
#Роуты для подмодуля Решения
###########################################################################################
\Route::group(['prefix' => 'admin' . '/system/modules/solutions', 'middleware' => 'secure'], function () {
    \Route::get('/', ['as' => 'core.system.modules.solutions.index', 'uses' => 'ModulesSolutionsController@index']);
    \Route::post('enable', ['as' => 'core.system.modules.solutions.enable', 'uses' => 'ModulesSolutionsController@enable']);
    \Route::delete('disable', ['as' => 'core.system.modules.solutions.disable', 'uses' => 'ModulesSolutionsController@disable']);
});
###########################################################################################
#Роуты для модуля Группы
###########################################################################################
\Route::group(['prefix' => 'admin/system', 'middleware' => 'secure'], function () {
    \Route::get('groups/{group_id}/accesses', ['as' => 'core.system.groups.accesses-index', 'uses' => 'PermissionsController@index'])->where(['id' => '[0-9]+']);
    \Route::put('groups/{group_id}/accesses', ['as' => 'core.system.groups.accesses-update', 'uses' => 'PermissionsController@update'])->where(['id' => '[0-9]+']);
    \Route::resource('groups', 'GroupsController',
        [
            'except' => ['show'],
            'names' => [
                'index' => 'core.system.groups.index',
                'create' => 'core.system.groups.create',
                'store' => 'core.system.groups.store',
                'edit' => 'core.system.groups.edit',
                'update' => 'core.system.groups.update',
                'destroy' => 'core.system.groups.destroy',
            ]
        ]
    );
});
###########################################################################################
#Роуты для модуля Пользователи
###########################################################################################
\Route::group(['prefix' => 'admin/system', 'middleware' => 'secure'], function () {
    \Route::resource('users', 'UsersController',
        [
            'except' => ['show'],
            'names' => [
                'index' => 'core.system.users.index',
                'create' => 'core.system.users.create',
                'store' => 'core.system.users.store',
                'edit' => 'core.system.users.edit',
                'update' => 'core.system.users.update',
                'destroy' => 'core.system.users.destroy',
            ]
        ]
    );
});