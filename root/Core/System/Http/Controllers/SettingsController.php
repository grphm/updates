<?php
namespace STALKER_CMS\Core\System\Http\Controllers;

use STALKER_CMS\Vendor\Helpers;
use STALKER_CMS\Core\System\Models\Setting;
use STALKER_CMS\Vendor\Models\Languages;

/**
 * Контроллер Настройки пакетов
 * Class SettingsController
 * @package STALKER_CMS\Core\System\Http\Controllers
 */
class SettingsController extends ModuleController {

    /**
     * SettingsController constructor.
     */
    public function __construct() {

        $this->middleware('auth');
    }

    /**
     * Страница настроек установленных модулей
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {

        \PermissionsController::allowPermission('core_system', 'settings');
        $settings_values = \App::make('Settings');
        return view('core_system_views::settings.index', compact('settings_values'));
    }

    /**
     * Сохранение настроек
     * Сбрасываем все чекбоксы
     * Устанавливаем текущие
     * Анализируем состояние
     * @return \Illuminate\Http\JsonResponse
     */
    public function update() {

        \PermissionsController::allowPermission('core_system', 'settings');
        $request = \RequestController::isAJAX()->init();
        $this->resetCheckboxSettings();
        foreach(\PermissionsController::packagesEnabled() as $package_name => $package_title):
            if($request::has($package_name)):
                foreach($request::input($package_name) as $module_name => $settings):
                    foreach($settings as $name => $value):
                        Setting::updateOrCreate(
                            ['package' => $package_name, 'module' => $module_name, 'name' => $name],
                            ['value' => $value, 'user_id' => \Auth::user()->id]
                        );
                    endforeach;
                endforeach;
            endif;
        endforeach;
        $this->analysisSystemSettings();
        $this->analysisMailsSettings();
        return \ResponseController::success(202)->redirect(route('core.system.settings.rebuild'))->json();
    }

    /**
     * Кэширование настроек
     * @return \Illuminate\Http\RedirectResponse
     */
    public function rebuild() {

        \PermissionsController::allowPermission('core_system', 'settings');
        \Artisan::call('config:cache', ['--no-ansi' => TRUE, '--quiet' => TRUE]);
        return redirect()->to(route('core.system.settings.index').'?status=200');
    }

    /**
     * Основные настройки CMS
     */
    public function settings() {

        $languages = Languages::whereActive(TRUE)->lists('title', 'slug');
        return view('core_system_views::settings.main', compact('languages'));
    }

    /**
     * Сохранения настроек CMS
     */
    public function settingsStore() {

        $request = \RequestController::isAJAX()->init();
        if(\ValidatorController::passes($request, ['APP_LOCALE' => 'required', 'APPLICATION_NAME' => 'required'])):
            \Auth::user()->locale = $request::input('APP_LOCALE');
            \Auth::user()->save();
            Helpers\update_config_file('APPLICATION_NAME="'.config('app.application_name').'"', 'APPLICATION_NAME="'.$request::input('APPLICATION_NAME').'"');
            if($request::hasFile('logo') && $request::file('logo')->isValid()):
                if($request::file('logo')->getMimeType() == 'image/png'):
                    $fileName = public_path('theme/images/sidebar-logo.png');
                    if(file_exists($fileName)):
                        unlink($fileName);
                    endif;
                    $request::file('logo')->move(public_path('theme/images'), 'sidebar-logo.png');
                endif;
            endif;
            if($request::hasFile('favicon') && $request::file('favicon')->isValid()):
                if($request::file('favicon')->getMimeType() == 'image/x-icon'):
                    try {
                        $fileName = public_path('theme/favicon.ico');
                        if(file_exists($fileName)):
                            unlink($fileName);
                        endif;
                        $fileName = public_path('favicon.ico');
                        if(file_exists($fileName)):
                            unlink($fileName);
                        endif;
                        $request::file('favicon')->move(public_path('theme'), 'favicon.ico');
                        copy(public_path('theme/favicon.ico'), public_path('favicon.ico'));
                    } catch(\Exception $exception) {
                        return \ResponseController::error($exception->getCode())->json();
                    }
                endif;
            endif;
            $this->rebuild();
            return \ResponseController::success(202)->redirect(route('cms.settings.index'))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /************************************************************************************/
    /**
     * Получить все настройки включенных модулей
     * @return array
     */
    public static function getSettings() {

        $_settings = [];
        foreach(\PermissionsController::packagesEnabled() as $package_name => $package_title):
            if(config($package_name.'::settings')):
                $_settings[$package_name] = config($package_name.'::settings');
            endif;
        endforeach;
        foreach(Setting::all() as $db_setting):
            if(isset($_settings[$db_setting->package][$db_setting->module]['options'][$db_setting->name]['value'])):
                $_settings[$db_setting->package][$db_setting->module]['options'][$db_setting->name]['value'] = $db_setting->value;
                $_settings[$db_setting->package][$db_setting->module]['options'][$db_setting->name]['user'] = $db_setting->user_id ? \DB::table('users')->value('name') : 'Значение по умолчанию';
                $_settings[$db_setting->package][$db_setting->module]['options'][$db_setting->name]['updated_at'] = $db_setting->user_id ? $db_setting->updated_at->format("d.m.Y в H:i") : 'Значение по умолчанию';
            endif;
        endforeach;
        return $_settings;
    }

    /************************************************************************************/
    /**
     * Создает файл конфигрурации в кэше
     * bootstrap/cache/config.php
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createConfigCash() {

        return $this->rebuild();
    }

    /**
     * Удаляет файл конфигрурации из кэша
     * bootstrap/cache/config.php
     * @return \Illuminate\Http\RedirectResponse
     */
    public function clearConfigCash() {

        \PermissionsController::allowPermission('core_system', 'settings');
        \Artisan::call('config:clear', ['--no-ansi' => TRUE, '--quiet' => TRUE]);
        return redirect()->route('core.system.settings.index')->with('status', 200);
    }

    /**
     * Показывает странцу информации о PHP
     * @return bool
     */
    public function phpInformation() {

        phpinfo();
    }

    /**
     *  Сбрасываем все значения чекбоксов
     */
    private function resetCheckboxSettings() {

        $settings_values = \App::make('Settings');
        foreach($settings_values as $package_name => $modules):
            foreach($modules as $module_name => $settings):
                foreach($settings['options'] as $name => $value):
                    if(isset($value['type']) && $value['type'] == 'checkbox'):
                        Setting::wherePackage($package_name)->whereModule($module_name)->whereName($name)->update(['value' => 0]);
                    endif;
                endforeach;
            endforeach;
        endforeach;
    }

    /**
     * Анализ значений чекбоксов
     */
    private function analysisSystemSettings() {

        if(\PermissionsController::isPackageEnabled('core_system')):
            $settings_values = self::getSettings();
            $settings_options = $settings_values['core_system']['settings']['options'];
            if($settings_options['services_mode']['value']):
                // Включен режим обслуживания
                # Обработка режима
            endif;
            if($settings_options['debug_mode']['value'] == TRUE):
                Helpers\update_config_file('APP_DEBUG=false', 'APP_DEBUG=true');
            elseif($settings_options['debug_mode']['value'] == FALSE):
                Helpers\update_config_file('APP_DEBUG=true', 'APP_DEBUG=false');
            endif;
            \Artisan::call('config:clear', ['--no-ansi' => TRUE, '--quiet' => TRUE]);
        endif;
    }

    /**
     * Анализ и изменения настроек SMTP сервера
     */
    private function analysisMailsSettings() {

        if(\PermissionsController::isPackageEnabled('core_mailer')):
            $settings_values = self::getSettings();
            $settings_options = $settings_values['core_mailer']['mailer']['options'];
            $config_file = file(base_path('.env'));
            if(is_array($config_file)):
                foreach($config_file as $key => $value):
                    $params = explode('=', $value);
                    if(in_array('MAIL_HOST', $params)):
                        $config_file[$key] = 'MAIL_HOST='.$settings_options['smtp_host']['value']."\n";
                    elseif(in_array('MAIL_PORT', $params)):
                        $config_file[$key] = 'MAIL_PORT='.$settings_options['smtp_port']['value']."\n";
                    elseif(in_array('MAIL_ENCRYPTION', $params)):
                        $config_file[$key] = 'MAIL_ENCRYPTION='.$settings_options['smtp_encryption']['value']."\n";
                    elseif(in_array('MAIL_USERNAME', $params)):
                        $config_file[$key] = 'MAIL_USERNAME='.$settings_options['smtp_username']['value']."\n";
                    elseif(in_array('MAIL_PASSWORD', $params)):
                        $config_file[$key] = 'MAIL_PASSWORD='.$settings_options['smtp_password']['value']."\n";
                    endif;
                endforeach;
            endif;
            $fp = fopen(base_path('.env'), "w+");
            fwrite($fp, implode("", $config_file));
            fclose($fp);
            \Artisan::call('config:clear', ['--no-ansi' => TRUE, '--quiet' => TRUE]);
        endif;
    }
}