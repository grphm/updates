<?php
namespace STALKER_CMS\Core\System\Http\Controllers;

use Illuminate\Support\Collection;
use PhpSpec\Exception\Exception;
use STALKER_CMS\Core\System\Models\Group;
use STALKER_CMS\Core\System\Models\User;
use STALKER_CMS\Vendor\Interfaces\CrudInterface;
use STALKER_CMS\Vendor\Helpers;

/**
 * Контроллер Пользователи
 * Class UsersController
 * @package STALKER_CMS\Core\System\Http\Controllers
 */
class UsersController extends ModuleController implements CrudInterface {

    /**
     * Модель
     * @var STALKER_CMS\Core\System\Models\User
     */
    protected $model;

    /**
     * UsersController constructor.
     * @param \STALKER_CMS\Core\System\Models\User $users
     */
    public function __construct(User $users) {

        $this->model = $users;
        $this->middleware('auth');
    }

    /**
     * Список пользователей
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {

        \PermissionsController::allowPermission('core_system', 'users');
        $request = \RequestController::init();
        $users = $this->model->with('group', 'session')->where('id', '>', 1);
        if ($request::has('group')):
            $users = $users->whereGroupId($request::input('group'));
        endif;
        if ($request::has('sort_field') && $request::has('sort_direction')):
            foreach (explode(',', $request::get('sort_field')) as $index):
                $users = $users->orderBy($index, $request::get('sort_direction'));
            endforeach;
        endif;
        if ($request::has('search')):
            $search = $request::get('search');
            $users = $users->where(function ($query) use ($search) {
                $query->where('name', 'like', '%' . $search . '%');
                $query->orWhere('email', 'like', '%' . $search . '%');
            });
        endif;
        $total_users = $users->count();
        $users = $users->paginate(25);
        $groups = new Collection();
        foreach (Group::where('id', '>', 1)->get() as $group):
            $groups[$group->id] = $group->title;
        endforeach;
        return view('core_system_views::users.index', compact('users', 'groups', 'total_users'));
    }

    /**
     * Добавление пользователя
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {

        \PermissionsController::allowPermission('core_system', 'users');
        return view('core_system_views::users.create', ['groups' => $this->getGroups()]);
    }

    /**
     * Добавление пользователя
     * @return mixed
     */
    public function store() {

        \PermissionsController::allowPermission('core_system', 'users');
        $request = \RequestController::isAJAX()->init();
        if (\ValidatorController::passes($request, $this->model->getStoreRules())):
            if (User::whereExist(['login' => $request::input('email')])):
                return \ResponseController::error(2011)->json();
            endif;
            if ($avatar = $this->uploadAvatar($request)):
                $request::merge($avatar);
            endif;
            $this->model->insert($request);
            $this->sendNotify($request);
            return \ResponseController::success(201)->redirect(route('core.system.users.index'))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /**
     * Редактирование пользователя
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id) {

        \PermissionsController::allowPermission('core_system', 'users');
        $user = $this->model->findOrFail($id);
        $groups = $this->getGroups();
        $social = json_decode($user->social, TRUE);
        return view('core_system_views::users.edit', compact('user', 'social', 'groups'));
    }

    /**
     * Обновление пользователя
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update($id) {

        \PermissionsController::allowPermission('core_system', 'groups');
        $request = \RequestController::isAJAX()->init();
        if (\ValidatorController::passes($request, $this->model->getUpdateRules())):
            $user = $this->model->findOrFail($id);
            if ($user->login != $request::input('email') && User::whereExist(['login' => $request::input('email')])):
                return \ResponseController::error(2011)->json();
            endif;
            if ($avatar = $this->uploadAvatar($request)):
                $this->deleteAvatars($user);
                $request::merge($avatar);
            else:
                $request::merge(['photo' => $user->photo, 'thumbnail' => $user->thumbnail]);
            endif;
            $this->model->replace($id, $request);
            return \ResponseController::success(200)->redirect(route('core.system.users.index'))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /**
     * Удаление пользователя
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id) {

        \PermissionsController::allowPermission('core_system', 'users');
        $request = \RequestController::isAJAX()->init();
        $user = $this->model->findOrFail($id);
        $this->deleteAvatars($user);
        $user->delete();
        return \ResponseController::success(1203)->redirect(route('core.system.users.index'))->json();
    }

    /*****************************************************************************/

    /**
     * Получить группы пользователей
     * @return mixed
     */
    private function getGroups() {

        if (\Auth::user()->group_id == 1):
            return Group::lists('title', 'id');
        else:
            return Group::where('id', '>', 1)->lists('title', 'id');
        endif;
    }

    /**
     * Загрузка аватара
     * @param \Request $request
     * @return null
     * @throws \Exception
     */
    private function uploadAvatar(\Request $request) {

        if ($photo = Helpers\trim_base64_image($request::input('photo'))):
            $thumbnail = Helpers\trim_base64_image($request::input('thumbnail'));
            $upload_directory = Helpers\setDirectory(Helpers\settings(['core_system', 'users', 'avatar_dir']));
            $upload_directory_thumbnail = Helpers\setDirectory(Helpers\settings(['core_system', 'users', 'avatar_dir']) . '/thumbnail');
            $fileName = time() . "_" . rand(1000, 1999) . '.jpg';
            $photoPath = Helpers\double_slash($upload_directory . '/' . $fileName);
            $thumbnailPath = Helpers\double_slash($upload_directory_thumbnail . '/' . $fileName);
            \Storage::put($photoPath, base64_decode($photo));
            \Storage::put($thumbnailPath, base64_decode($thumbnail));
            $extend['photo'] = Helpers\add_first_slash($photoPath);
            $extend['thumbnail'] = Helpers\add_first_slash($thumbnailPath);
            return $extend;
        else:
            return NULL;
        endif;
    }

    /**
     * Удаление аватара
     * @param User $user
     */
    private function deleteAvatars(User $user) {

        if (!empty($user->photo) && \Storage::exists($user->photo)):
            \Storage::delete($user->photo);
            $user->photo = NULL;
        endif;
        if (!empty($user->thumbnail) && \Storage::exists($user->thumbnail)):
            \Storage::delete($user->thumbnail);
            $user->thumbnail = NULL;
        endif;
        $user->save();
    }

    /**
     * Отправка уведомления о регистрации
     * @param \Request $request
     */
    private function sendNotify(\Request $request) {

        if (\PermissionsController::isPackageEnabled('core_mailer') && $request::has('send_notification')):
            \PublicMailer::register($request);
        endif;
    }
}