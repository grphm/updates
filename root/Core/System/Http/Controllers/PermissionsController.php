<?php
namespace STALKER_CMS\Core\System\Http\Controllers;

use League\Flysystem\Exception;
use STALKER_CMS\Core\System\Models\Group;
use STALKER_CMS\Core\System\Models\Permission;
use STALKER_CMS\Vendor\Models\Packages;

/**
 * Контроллер Управление доступом
 * Class PermissionsController
 * @package STALKER_CMS\Core\System\Http\Controllers
 */
class PermissionsController extends ModuleController {

    /**
     * Установка прав доступа для группы
     * @param $group_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($group_id) {

        $this->allowPermission('core_system', 'groups');
        $group = Group::whereFind(['id' => $group_id]);
        $actions = [];
        foreach (self::packagesEnabled() as $package_slug => $package_title):
            if ($package_actions = config($package_slug . '::actions')):
                $actions[$package_slug]['info'] = [
                    'title' => config($package_slug . '::config.package_title'),
                    'icon' => config($package_slug . '::config.package_icon')
                ];
                $actions[$package_slug]['actions'] = $package_actions;
                foreach (self::permissionsEnabled($package_slug, $group_id) as $permission):
                    foreach ($actions[$package_slug]['actions'] as $module_name => $action):
                        if ($permission == $module_name):
                            $actions[$package_slug]['actions'][$module_name]['enabled'] = TRUE;
                        endif;
                    endforeach;
                endforeach;
            endif;
        endforeach;
        return view('core_system_views::groups.accesses', compact('group', 'actions'));
    }

    /**
     * Сохранение прав доступа для группы
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id) {

        $this->allowPermission('core_system', 'groups');
        $request = \RequestController::isAJAX()->init();
        $this->resetCheckboxPermissions($id);
        foreach ($this->packagesEnabled() as $package_slug => $package_title):
            if ($request::has($package_slug)):
                foreach ($request::input($package_slug) as $module_name => $permission):
                    $model = new Permission();
                    if ($model->whereExist(['group_id' => $id, 'module' => $package_slug, 'action' => $module_name])):
                        $model->replace(['group_id' => $id, 'module' => $package_slug, 'action' => $module_name], ['status' => $permission]);
                    else:
                        $model->insert(['group_id' => $id, 'module' => $package_slug, 'action' => $module_name, 'status' => $permission]);
                    endif;
                endforeach;
            endif;
        endforeach;
        return \ResponseController::success(202)->redirect(route('core.system.groups.accesses-index', $id))->json();
    }

    /**
     * Сброс всех прав доступа
     * @param $group_id
     * @return mixed
     */
    private function resetCheckboxPermissions($group_id) {

        return Permission::whereGroupId($group_id)->update(['status' => 0]);
    }

    /***********************************************************************************************/

    /**
     * Возвращает права доступа для активных модулей
     * @return array
     */
    public function getPermissions() {

        $permissions = [];
        foreach ($this->packagesEnabled() as $package_slug => $package_title):
            $permissions[$package_slug] = $this->permissionsEnabled($package_slug);
        endforeach;
        return $permissions;
    }

    /**
     * Проверяет на активность модуля
     * @param $package_slug
     * @return mixed
     */
    public function isPackageEnabled($package_slug) {

        $PackagesEnabled = \App::make('PackagesEnabled');
        if (!empty($PackagesEnabled)):
            return isset($PackagesEnabled[$package_slug]) ? TRUE : FALSE;
        else:
            return Packages::whereSlug($package_slug)->whereEnabled(TRUE)->exists();
        endif;
    }

    /**
     * Проверяет на установку модуля
     * @param $package_slug
     * @return mixed
     */
    public function isPackageInstalled($package_slug) {

        $PackagesInstalled = \App::make('PackagesInstalled');
        if (!empty($PackagesInstalled)):
            return isset($PackagesInstalled[$package_slug]) ? TRUE : FALSE;
        else:
            return Packages::whereSlug($package_slug)->exists();
        endif;
    }

    /**
     * Возвращает список активных модулей
     * @return mixed
     */
    public function packagesEnabled() {

        if ($packages = Packages::orderBy('order')->whereEnabled(TRUE)->lists('title', 'slug')):
            return $packages->toArray();
        endif;
    }

    /**
     * Возвращает список установленных модулей
     * @return mixed
     */
    public function packagesInstalled() {

        if ($packages = Packages::orderBy('order')->lists('title', 'slug')):
            return $packages->toArray();
        endif;
    }

    /**
     * Возвращает список прав доступа для группы
     * @param $package_slug
     * @param null $group_id
     * @return mixed
     */
    public function permissionsEnabled($package_slug, $group_id = NULL) {

        if (is_null($group_id)):
            $group_id = \Auth::user()->group_id;
        endif;

        if ($permissions = Permission::whereGroupId($group_id)->whereModule($package_slug)->whereStatus(TRUE)->lists('action')):
            return $permissions->toArray();
        endif;
    }

    /**
     * Проверяет разрещения права доступа к событию модуля
     * @param $package_slug
     * @param $package_action
     * @param bool|TRUE $exception
     * @return bool
     * @throws Exception
     */
    public function allowPermission($package_slug, $package_action, $exception = TRUE) {

        $permissions = \App::make('Permissions');
        $packages = \App::make('PackagesEnabled');
        if (isset($permissions[$package_slug]) && in_array($package_action, $permissions[$package_slug]) && isset($packages[$package_slug])):
            return TRUE;
        else:
            if ($exception):
                throw new Exception(\Lang::get('root_lang::codes.403'), 403);
            else:
                return FALSE;
            endif;
        endif;
    }
}