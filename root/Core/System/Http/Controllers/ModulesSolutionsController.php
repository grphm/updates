<?php
namespace STALKER_CMS\Core\System\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use STALKER_CMS\Vendor\Helpers;
use STALKER_CMS\Vendor\Models\Packages;

/**
 * Контроллер Доступных решений
 * Class ModulesSolutionsController
 * @package STALKER_CMS\Core\System\Http\Controllers
 */
class ModulesSolutionsController extends ModuleController {

    /**
     * ModulesSolutionsController constructor.
     */
    public function __construct() {

        $this->middleware('auth');
    }

    /**
     * Список доступных решений на базе установленных модулей
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {

        \PermissionsController::allowPermission('core_system', 'modules_solutions');
        $solutions_packages = include_once(realpath(__DIR__ . '/../../Config/solutions.php'));
        foreach ($solutions_packages as $section => $solutions_lists):
            foreach ($solutions_lists['solutions'] as $package_name => $package):
                if (\PermissionsController::isPackageInstalled($package['package_name'])):
                    unset($solutions_packages[$section]['solutions'][$package_name]);
                endif;
            endforeach;
        endforeach;
        return view('core_system_views::solutions.index', compact('solutions_packages'));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function enable() {

        $request = \RequestController::isAJAX()->init();
        if (\ValidatorController::passes($request, ['solution' => 'required'])):
            $solutions_packages = include_once(realpath(__DIR__ . '/../../Config/solutions.php'));
            if (isset($solutions_packages[$request::input('section')]['solutions'][$request::input('solution')])):
                $composer = Helpers\settings(['core_system', 'settings', 'composer']);
                $repository = $solutions_packages[$request::input('section')]['solutions'][$request::input('solution')]['composer'];
                $directory = $solutions_packages[$request::input('section')]['solutions'][$request::input('solution')]['directory'];
                $repository_group = $solutions_packages[$request::input('section')]['solutions'][$request::input('solution')]['repository_group'];
                $repository_name = $solutions_packages[$request::input('section')]['solutions'][$request::input('solution')]['repository_name'];
                $php = Helpers\settings(['core_system', 'settings', 'php']);
                if (\File::exists($composer)):
                    try {
                        if (!\File::exists(storage_path('app/updates'))):
                            \File::makeDirectory(storage_path('app/updates'), 0754);
                        else:
                            \File::deleteDirectory(storage_path('app/updates'));
                            \File::makeDirectory(storage_path('app/updates'), 0754);
                        endif;
                        chdir(storage_path('app/updates'));
                        if (substr(php_uname(), 0, 7) == "Windows"):
                            shell_exec(trim($php . ' ' . $composer . ' self-update'));
                            shell_exec(trim($php . ' ' . $composer . ' --prefer-dist --no-progress --no-ansi require ' . $repository));
                        else:
                            shell_exec('COMPOSER_HOME="' . storage_path('app/updates') . '" ' . trim($php . ' ' . $composer) . ' self-update');
                            shell_exec('COMPOSER_HOME="' . storage_path('app/updates') . '" ' . trim($php . ' ' . $composer) . ' --prefer-dist --no-progress --no-ansi require ' . $repository);
                        endif;
                        if (\File::exists(storage_path('app/updates/vendor/' . $repository_group . '/' . $repository_name . '/' . $directory))):
                            \File::copyDirectory(storage_path('app/updates/vendor/' . $repository_group . '/' . $repository_name . '/' . $directory), base_path('root/Solutions/' . $directory));
                        endif;
                        \File::deleteDirectory(storage_path('app/updates'));
                        if (\File::exists(base_path('root/Solutions/' . $directory))):
                            $package = new Packages();
                            $package->slug = $solutions_packages[$request::input('section')]['solutions'][$request::input('solution')]['package_name'];
                            $package->title = json_encode($solutions_packages[$request::input('section')]['solutions'][$request::input('solution')]['package_title']);
                            $package->description = json_encode($solutions_packages[$request::input('section')]['solutions'][$request::input('solution')]['package_description']);
                            $package->relations = implode('|', $solutions_packages[$request::input('section')]['solutions'][$request::input('solution')]['relations']);
                            $package->composer_config = $repository;
                            $package->order = Packages::where('order', '<', 1000)->max('order') + 1;
                            $package->save();
                            return \ResponseController::success(200)->redirect(route('core.system.modules.rebuild', ['redirect_route' => 'core.system.modules.solutions.index']))->json();
                        else:
                            return \ResponseController::error(2004)->redirect(route('core.system.modules.solutions.index'))->json();
                        endif;
                    } catch (\Exception $e) {
                        if (\File::exists(base_path('root/Solutions/' . $directory))):
                            \File::deleteDirectory(base_path('root/Solutions/' . $directory));
                        endif;
                        Packages::whereSlug($request::input('solution'))->delete();
                        return \ResponseController::success(500)->redirect(route('core.system.modules.solutions.index'))->json();
                    }
                else:
                    return \ResponseController::error(2404)->redirect(route('core.system.modules.solutions.index'))->json();
                endif;
            endif;
            return \ResponseController::success(1204)->redirect(route('core.system.modules.solutions.index'))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /**
     * Вернуть список установленных решений из каталога root/Solutions
     * @return Collection
     */
    private function getAvailableSolutions() {

        $available_solutions_packages = new Collection();
        $directory_solutions = base_path('root/Solutions');
        foreach (\File::directories($directory_solutions) as $solution_directory):
            $package_config = realpath($solution_directory . '/Config/' . strtolower(basename($solution_directory) . '.php'));
            if (\File::exists($package_config)):
                $show_package = FALSE;
                $solution_package = include($package_config);
                if (!empty($solution_package['relations']) && is_array($solution_package['relations'])):
                    foreach ($solution_package['relations'] as $relation_package_name):
                        if (\PermissionsController::isPackageEnabled($relation_package_name)):
                            $show_package = TRUE;
                            break;
                        endif;
                    endforeach;
                else:
                    $show_package = TRUE;
                endif;
                if ($show_package):
                    $solution_package['enabled'] = FALSE;
                    $solution_package['composer_config'] = NULL;
                    $solution_package['install_path'] = realpath($solution_directory);
                    if (\PermissionsController::isPackageInstalled($solution_package['package_name']) === FALSE):
                        $available_solutions_packages[$solution_package['package_name']] = $solution_package;
                    endif;
                endif;
            endif;
        endforeach;
        return $available_solutions_packages;
    }
}